define(['phoenix/Helper', 'phoenix/RequestBuilder', 'phoenix/EventBus'],
function configReaderLoader(Helper, RequestBuilder, EventBus) {
    var buildConfig = {}, route = {}, locale = {},
        configCounter = 0, isLoaded = false;

    function assertLoaded() {
        if (isLoaded === false) {
            throw new Error('ConfigReader not loaded.');
        }
    }

    function makeRequest(url, successCallback, version) {
        var rB = new RequestBuilder(), request;

        rB.setUrl(url + '?t=' + version);
        rB.setMethod('GET');
        rB.setSuccessCallback(successCallback);
        request = rB.build();
        request.send(request);
    }

    function reduceCounter(step) {
        configCounter -= step;
        if (configCounter === 0) {
            isLoaded = true;
            buildConfig = Helper.deepFreeze(buildConfig);
            route = Helper.deepFreeze(route);
            locale = Helper.deepFreeze(locale);
            EventBus.publish('Configuration:Loaded');
        }
    }

    function updateConfig(res) {
        var response = JSON.parse(res);

        Object.assign(buildConfig, response);
        reduceCounter(1);
    }

    function updateRoutes(res) {
        var response = JSON.parse(res);

        Object.assign(route, response);
        reduceCounter(1);
    }

    function updateLocale(res) {
        var response = JSON.parse(res);

        Object.assign(locale, response);
        reduceCounter(1);
    }

    function ConfigReader(win) {
        this.win = win;
        Object.assign(buildConfig, this.win.Configuration);
    }

    function addCounter(step) {
        configCounter += step;
    }

    function getVersion(config) {
        if (config.Version) {
            return config.Version;
        }

        return (new Date()).getTime();
    }

    ConfigReader.prototype.load = function load() {
        var plugins = this.win.Configuration.Plugins,
            version = getVersion(this.win.Configuration),
            key, config, routes, pluginLocale, enable;

        for (key in plugins) {
            if (plugins.hasOwnProperty(key) === true) {
                enable = plugins[key].enable;
                config = plugins[key].Config;
                routes = plugins[key].Routes;
                pluginLocale = plugins[key].Locale;

                if (enable !== true) {
                    continue;
                }
                if (Helper.isUndefined(config) === false) {
                    addCounter(1);
                    makeRequest(config, updateConfig, version);
                }
                if (Helper.isUndefined(routes) === false) {
                    addCounter(1);
                    makeRequest(routes, updateRoutes, version);
                }
                if (Helper.isUndefined(pluginLocale) === false) {
                    addCounter(1);
                    makeRequest(pluginLocale, updateLocale, version);
                }
            }
        }
    };

    ConfigReader.getAll = function getAll() {
        assertLoaded();

        return buildConfig;
    };

    ConfigReader.getRoute = function getRoute(key) {
        assertLoaded();
        if (Helper.objectHasKey(route, key) === false) {
            throw new Error('error.route.key.missing ' + key);
        }

        return route[key];
    };

    ConfigReader.getLocale = function getLocale() {
        assertLoaded();

        return locale;
    };

    ConfigReader.getLocaleByKey = function getLocaleByKey(key) {
        assertLoaded();
        if (Helper.objectHasKey(locale, key) === true) {
            return locale[key];
        }

        return null;
    };

    ConfigReader.get = function get(key) {
        assertLoaded();
        if (Helper.objectHasKey(buildConfig, key) === false) {
            throw new Error('error.configuration.key.missing');
        }

        return buildConfig[key];
    };

    return ConfigReader;
});
