define([], function helperLoader() {
    var isFunction = function isFunction(fn) {
            return typeof fn === 'function';
        },
        isNull = function isNull(str) {
            return str === null;
        },
        isString = function isString(str) {
            return typeof str === 'string';
        },
        isObject = function isObject(obj) {
            return obj !== null && typeof obj === 'object';
        },
        isNumber = function isNumber(num) {
            return typeof num === 'number';
        },
        isUndefined = function isUndefined(data) {
            return typeof data === 'undefined';
        },
        isEmptyString = function isEmptyString(str) {
            if (isUndefined(str) === true || isString(str) === false) {
                return false;
            }

            return str.length === 0;
        },
        isEmpty = function isEmpty(data) {
            return isNull(data) === true || isEmptyString(data) === true ||
                    isUndefined(data) === true;
        },
        emptyNode = function emptyNodeFn(node) {
            while (node.firstChild !== null) {
                node.removeChild(node.firstChild);
            }
        },
        copyNode = function copyNodeFn(node1, node2) {
            while (node1.firstChild !== null) {
                node2.appendChild(node1.firstChild);
            }
        },
        objectHasKey = function objectHasKey(obj, key) {
            return isObject(obj) && obj.hasOwnProperty(key);
        },
        isArray = function isArray(arr) {
            return Array.isArray(arr);
        },
        getUrlParams = function getUrlParams(key) {
            var query, vars, pair, i;

            query = window.location.search.substring(1);
            vars = query.split('&');
            for (i = 0; i < vars.length; i += 1) {
                pair = vars[i].split('=');
                if (pair[0] === key) {
                    return pair[1];
                }
            }

            return '';
        },
        getUniqueId = function getUniqueId(prefix) {
            var text = '', i = 0,
                possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

            for (i = 0; i < 10; i += 1) {
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            }

            prefix = prefix || '';

            return prefix + text;
        },
        formatBytes = function formatBytes(bytes, precison) {
            var k = 1024, i = 0, size,
                sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

            if (!bytes || isNaN(bytes) === true) {
                bytes = 0;
            }
            if (!precison || isNaN(precison) === true) {
                precison = 0;
            }
            if (bytes === 0) {
                return '0 Byte';
            }
            i = Math.floor(Math.log(bytes) / Math.log(k));
            size = parseFloat((bytes / Math.pow(k, i)).toFixed(precison));

            return size.toString() + ' ' + sizes[i];
        },
        uniqueArray = function uniqueArray(array) {
            return array.filter(function eachItem(elem, pos) {
                return array.indexOf(elem) === pos;
            });
        },
        stringTrim = function stringTrim(str) {
            return str.replace(/&nbsp;/gi, '').trim();
        },
        removeValFromArray = function removeValFromArrayFn(array, value) {
            var index = array.indexOf(value);

            if (index > -1) {
                array.splice(index, 1);
                removeValFromArray(array, value);
            }

            return array;
        },
        replaceLocaleString = function replaceLocaleString(str, locale) {
            var replacer = function localeReplacer(match) {
                var key = match.substring(2, match.length - 2);

                if (key in locale && typeof locale[key] === 'string') {
                    return locale[key];
                }

                return key;
            };

            return str.replace(/\{\{[a-z.1-9]+\}\}/g, replacer);
        },
        addRulesToStyleSheet = function addRulesToStyleSheet(doc, styleSheet, rules) {
            var rule, selector, propStr, cssStr = [];

            for (selector in rules) {
                if (rules.hasOwnProperty(selector) === true) {
                    propStr = '';
                    for (rule in rules[selector]) {
                        if (rules[selector].hasOwnProperty(rule) === true) {
                            propStr += '    ' + rule + ': ' + rules[selector][rule] + ';\n';
                        }
                    }

                    if (propStr !== '') {
                        cssStr.push(selector + ' {\n' + propStr + '}');
                    }
                }
            }

            if (cssStr.length > 0) {
                styleSheet.appendChild(doc.createTextNode(cssStr.join('\n')));
            }
        },
        deepFreeze = function deepFreezeFn(obj) {
            var prop, propNames,
                freeze = function freezeFn(object) {
                    propNames = Object.getOwnPropertyNames(object);
                    propNames.forEach(function eachName(name) {
                        prop = object[name];

                        if (typeof prop === 'object' && prop !== null) {
                            freeze(prop);
                        }
                    });

                    return Object.freeze(object);
                };

            return freeze(obj);
        },
        addMixins = function addMixinsFn(destination, source) {
            var prop;

            for (prop in source) {
                if (source.hasOwnProperty(prop)) {
                    destination[prop] = source[prop];
                }
            }
        },
        encode = function encodeFn(win, content) {
            if (isFunction(win.btoa) === false) {
                return null;
            }

            return win.btoa(unescape(encodeURIComponent(content)));
        },
        decode = function decodeFn(win, content) {
            if (isFunction(win.atob) === false) {
                return null;
            }

            return decodeURIComponent(escape(win.atob(content)));
        },
        fireCustomEvent = function fireCustomEventFn(name, eventDetail, element) {
            var event = new CustomEvent(name, {
                'detail': eventDetail,
                'bubbles': true,
                'cancelable': true
            });

            element.dispatchEvent(event);
        },
        defineProperty = function defineProp(
            instance, prop, value, writable, enumerable, configurable
        ) {
            Object.defineProperty(instance, prop, {
                'value': value,
                'enumerable': Boolean(enumerable),
                'configurable': Boolean(configurable),
                'writable': Boolean(writable)
            });
        },
        arrayEqual = function arrayEqual(arrayA, arrayB) {
            var i = 0, len = arrayA.length;

            if (len !== arrayB.length) {
                return false;
            }

            for (; i < len; i += 1) {
                if (arrayB.indexOf(arrayA[i]) === -1) {
                    return false;
                }
            }

            return true;
        },
        isTextNode = function isTextNodeFn(node, win) {
            return node.nodeType === win.Node.TEXT_NODE;
        },
        isElementNode = function isElementNodeFn(node, win) {
            return node.nodeType === win.Node.ELEMENT_NODE;
        },
        isDomFragment = function isDomFragmentFn(dom, win) {
            return dom.nodeType === win.Node.DOCUMENT_FRAGMENT_NODE;
        },
        extend = function extend(klass, objects) {
            var base, propDescriptors = {}, props, temp, propNames;

            function Base() {}
            base = new Base();

            objects.forEach(function eachProto(proto) {
                temp = Object.getPrototypeOf(proto);

                Object.keys(temp).forEach(function eachProp(prop) {
                    base[prop] = temp[prop];
                });

                if (typeof Object.getOwnPropertyDescriptors === 'function') {
                    propDescriptors = Object.getOwnPropertyDescriptors(proto);
                }
                else {
                    propNames = Object.getOwnPropertyNames(proto);

                    propNames.forEach(function eachProp(prop) {
                        propDescriptors[prop] = Object.getOwnPropertyDescriptor(proto, prop);
                    });
                }

                props = Object.keys(propDescriptors);
                props.forEach(function eachProp(prop) {
                    delete base[prop];
                    Object.defineProperty(base, prop, propDescriptors[prop]);
                });
            });

            base.constructor = klass;

            return base;
        },
        isTypeOf = function isTypeOf(klass, type) {
            var prop;

            for (prop in type) {
                if (typeof type[prop] !== typeof klass[prop]) {
                    return false;
                }
            }

            return true;
        },
        camelize = function camelize(str) {
            var parts = str.split('-'), i, len = parts.length;

            if (len > 1) {
                for (i = len - 1; i >= 1; i -= 1) {
                    parts[i] = parts[i].substr(0, 1).toUpperCase() + parts[i].substr(1);
                }
            }

            return parts.join('');
        },
        executeExtraAttributes = function executeExtraAttributes(attributes, attributesNeeded) {
            var attribute, executedAttributes = {};

            if (attributesNeeded === false) {
                return executedAttributes;
            }

            for (attribute in attributes) {
                if (objectHasKey(attributes, attribute) === true) {
                    if (isFunction(attributes[attribute]) === true) {
                        executedAttributes[attribute] = attributes[attribute]();
                    }
                    else {
                        executedAttributes[attribute] = attributes[attribute];
                    }
                }
            }

            return executedAttributes;
        },
        Helper = {
            'isFunction': isFunction,
            'isString': isString,
            'isNull': isNull,
            'isObject': isObject,
            'isNumber': isNumber,
            'isUndefined': isUndefined,
            'isEmptyString': isEmptyString,
            'isEmpty': isEmpty,
            'emptyNode': emptyNode,
            'copyNode': copyNode,
            'objectHasKey': objectHasKey,
            'isArray': isArray,
            'getUrlParams': getUrlParams,
            'getUniqueId': getUniqueId,
            'formatBytes': formatBytes,
            'uniqueArray': uniqueArray,
            'stringTrim': stringTrim,
            'removeValFromArray': removeValFromArray,
            'replaceLocaleString': replaceLocaleString,
            'addRulesToStyleSheet': addRulesToStyleSheet,
            'deepFreeze': deepFreeze,
            'addMixins': addMixins,
            'encode': encode,
            'decode': decode,
            'fireCustomEvent': fireCustomEvent,
            'defineProperty': defineProperty,
            'arrayEqual': arrayEqual,
            'isTextNode': isTextNode,
            'isElementNode': isElementNode,
            'isDomFragment': isDomFragment,
            'extend': extend,
            'isTypeOf': isTypeOf,
            'camelize': camelize,
            'executeExtraAttributes': executeExtraAttributes
        };

    return Helper;
});
