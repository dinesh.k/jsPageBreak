/*global define*/
define([
    'rainbow/util/DomHelpers'
], function defineFn(DomHelper) {
    var proto = null;

    function BoxProcessor(win, doc) {
        this.dHelper = new DomHelper();
        this.win = win;
        this.doc = doc;
    }

    function createFootnoteContainer(action, box, dHelper) {
        var node = null;

        node = dHelper.create('div');
        dHelper.addOrModifyAttribute('class', action.selectors.foonoteClassname, node);
        dHelper.append(node, box);
    }

    function getFootnoteAnchor(action, box, container, dHelper) {
        var i = 0, footnoteAnchor = null, len = 0, attr = null, foonote = null,
            foonoteContainer = null;

        footnoteAnchor = dHelper.findAll(action.selectors.fAnchor, box);
        foonoteContainer = dHelper.find('.' + action.selectors.foonoteClassname, box);
        len = footnoteAnchor.length;
        for (i = 0; i < len; i += 1) {
            attr = footnoteAnchor[i].getAttribute(action.selectors.attributeName);
            foonote = dHelper.find(action.selectors.foonote + '[id=' + attr + ']', container);
            if (foonote === null) {
                foonote = dHelper.find(action.selectors.footnote + '[id=' + attr + ']', container);
            }
            dHelper.append(foonote, foonoteContainer);
        }
    }

    function handleAuhtorGroup(action, box, dHelper) {
        var txtBody = null, auGroup = null;

        txtBody = dHelper.find(action.selectors.auDest, box);
        auGroup = dHelper.find(action.selectors.auSrc, box);
        if (txtBody !== null && auGroup !== null) {
            dHelper.append(auGroup, txtBody);
        }
    }

    function handleBoxCredit(action, box, dHelper) {
        var credit = null, foonoteContainer = null;

        credit = dHelper.find(action.selectors.credit, box);
        foonoteContainer = dHelper.find('.' + action.selectors.foonoteClassname, box);
        if (credit !== null && foonoteContainer !== null) {
            dHelper.append(credit, foonoteContainer);
        }
    }

    proto = BoxProcessor.prototype;

    proto.execute = function execute(action, box, container) {
        var dHelper = this.dHelper;

        createFootnoteContainer(action, box, dHelper);
        getFootnoteAnchor(action, box, container, dHelper);
        handleAuhtorGroup(action, box, dHelper);
        handleBoxCredit(action, box, dHelper);

        return box;
    };

    return BoxProcessor;
});
