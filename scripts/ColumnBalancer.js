/*global define*/
define([], function defineFn() {
    var ColumnBalancer = function ColumnBalancerClass(templateHandler) {
        function getLineCount(pageItem) {
            var i = 0, source = null, lineCount = 0;

            for (; i < pageItem.sources.length; i += 1) {
                source = pageItem.sources[i];
                lineCount += source.lines.length;
            }

            return lineCount;
        }

        function getTemplateOfPageItem(page, pageItem) {
            var position = -1;

            position = page.pageItems.indexOf(pageItem);
            if (position >= 0 && position < page.template.pageItems.length) {
                return page.template.pageItems[position];
            }

            return null;
        }

        function getPageItemOfTemplate(page, pageItem) {
            var position = -1;

            position = page.template.pageItems.indexOf(pageItem);
            if (position >= 0 && position < page.pageItems.length) {
                return page.pageItems[position];
            }

            return null;
        }

        function cloneObject(obj) {
            var newObj = {}, propkey = null, value = null;

            for (propkey in obj) {
                if (obj.hasOwnProperty(propkey) === false) {
                    continue;
                }
                value = obj[propkey];
                newObj[propkey] = value;
            }

            return newObj;
        }

        function getBalacingSources(page, frontPageItem, backPageItem, leftToRight) {
            var lastIndex = 0, source = null,
                sources = {
                    'from': null,
                    'to': null,
                    'skipFlow': {
                        'from': false,
                        'to': false
                    },
                    'holdFlow': {
                        'from': false,
                        'to': false
                    },
                    'lineCount': {
                        'from': 0,
                        'to': 0
                    },
                    'template': {
                        'from': null,
                        'to': null
                    }
                };

            if (frontPageItem === null || backPageItem === null) {
                return null;
            }
            sources.skipFlow.from = frontPageItem.skipFlow;
            sources.skipFlow.to = backPageItem.skipFlow;
            sources.holdFlow.from = frontPageItem.holdFlow;
            sources.holdFlow.to = backPageItem.holdFlow;
            sources.lineCount.from = getLineCount(frontPageItem);
            sources.lineCount.to = getLineCount(backPageItem);
            sources.template.from = getTemplateOfPageItem(page, frontPageItem);
            sources.template.to = getTemplateOfPageItem(page, backPageItem);
            if (frontPageItem.sources.length > 0) {
                lastIndex = frontPageItem.sources.length - 1;
                sources.from = frontPageItem.sources[lastIndex];
            }
            if (backPageItem.sources.length > 0) {
                sources.to = backPageItem.sources[0];
            }
            if (sources.from === null || sources.to === null ||
                sources.from.content !== sources.to.content
            ) {
                if (leftToRight === true && sources.from !== null) {
                    source = cloneObject(sources.from);
                    source.lines = [];
                    backPageItem.sources.splice(0, 0, source);
                    sources.to = source;
                }
                else if (leftToRight === false && sources.to !== null) {
                    source = cloneObject(sources.to);
                    source.lines = [];
                    frontPageItem.sources.push(source);
                    sources.from = source;
                }
                else {
                    return null;
                }
            }

            return sources;
        }

        function getNextBreakableLines(existingLines, lines, fromBottom) {
            var i = 0, line = null, result = [], existFlag = false;

            if (fromBottom === true) {
                i = lines.length - 1;
            }
            while (i < lines.length && i >= 0) {
                line = lines[i];
                result.push(line);
                existFlag = existingLines.indexOf(line) >= 0;
                if (fromBottom === true && i > 0) {
                    line = lines[i - 1];
                }
                if (line.breakable === true && existFlag === false) {
                    break;
                }
                if (fromBottom === true) {
                    i -= 1;
                }
                else {
                    i += 1;
                }
            }

            return result;
        }

        function isFootnoteTypeLinkExist(lines) {
            var i = 0, j = 0, line = null, link = null;

            for (; i < lines.length; i += 1) {
                line = lines[i];
                for (j = 0; j < line.links.length; j += 1) {
                    link = line.links[j];
                    if (link.footnoteType === true) {
                        return true;
                    }
                }
            }

            return false;
        }

        function getPageItemHeight(pageItem) {
            var height = 0, i = 0, source = null, lastIndex = 0,
                totalHeight = 0, firstLine = null, lastLine = null;

            for (; pageItem !== null && i < pageItem.sources.length; i += 1) {
                source = pageItem.sources[i];
                lastIndex = source.lines.length - 1;
                if (lastIndex < 0) {
                    continue;
                }
                firstLine = source.lines[0];
                lastLine = source.lines[lastIndex];
                height = lastLine.bottom;
                height -= firstLine.top;
                height += source.additionalHeight;
                totalHeight += height;
            }

            return totalHeight;
        }

        function getColumnHeight(page, column) {
            var pageItems = null, pageItem = null, i = 0, height = 0;

            pageItems = column.pageItems;
            for (; i < pageItems.length; i += 1) {
                pageItem = pageItems[i];
                pageItem = getPageItemOfTemplate(page, pageItem);
                height += getPageItemHeight(pageItem);
            }

            return height;
        }

        function getBalancingHeight(page, columns) {
            var i = 0, column = null, height = 0;

            for (; i < columns.length; i += 1) {
                column = columns[i];
                height += getColumnHeight(page, column);
            }
            height /= columns.length;
            height = Math.round(height);

            return height;
        }

        function getFirstRunningPageItem(page, column) {
            var pageItems = null, pageItem = null, i = 0;

            pageItems = column.pageItems;
            for (; i < pageItems.length; i += 1) {
                pageItem = pageItems[i];
                if (templateHandler.isRunningItem(pageItem) === true) {
                    return getPageItemOfTemplate(page, pageItem);
                }
            }

            return null;
        }

        function getLastRunningPageItem(page, column) {
            var pageItems = null, pageItem = null, i = 0;

            pageItems = column.pageItems;
            for (i = pageItems.length - 1; i >= 0; i -= 1) {
                pageItem = pageItems[i];
                if (templateHandler.isRunningItem(pageItem) === true) {
                    return getPageItemOfTemplate(page, pageItem);
                }
            }

            return null;
        }

        function balanceTemplate(sources, leftToRight, committedHeight) {
            var existingHeight = 0, height = 0;

            if (committedHeight > 0) {
                if (leftToRight === true) {
                    existingHeight = templateHandler.getPageItemHeight(sources.template.from);
                    height = Math.round(existingHeight - committedHeight);
                    templateHandler.setPageItemHeight(sources.template.from, height);
                    existingHeight = templateHandler.getPageItemHeight(sources.template.to);
                    height = Math.round(existingHeight + committedHeight);
                    templateHandler.setPageItemHeight(sources.template.to, height);
                }
                else {
                    existingHeight = templateHandler.getPageItemHeight(sources.template.from);
                    height = Math.round(existingHeight + committedHeight);
                    templateHandler.setPageItemHeight(sources.template.from, height);
                    existingHeight = templateHandler.getPageItemHeight(sources.template.to);
                    height = Math.round(existingHeight - committedHeight);
                    templateHandler.setPageItemHeight(sources.template.to, height);
                }
            }
        }

        function balancePageItems(
            page, frontPageItem, backPageItem, leftToRight, balancingHeight
        ) {
            var height = 0, line = null, lastIndex = 0, sources = null,
                balFlag = false, lines = [], committedHeight = 0;

            sources = getBalacingSources(page, frontPageItem, backPageItem, leftToRight);
            while (balFlag === false && sources !== null) {
                if (leftToRight === false && sources.holdFlow.from === true) {
                    balFlag = true;
                    continue;
                }
                if (leftToRight === true && sources.to.lines.length === 0 &&
                    sources.skipFlow.to === true
                ) {
                    balFlag = true;
                    continue;
                }
                if (leftToRight === false && sources.from.lines.length === 0 &&
                    sources.skipFlow.from === true
                ) {
                    balFlag = true;
                    continue;
                }
                if (leftToRight === true && sources.from.lines.length > 0 &&
                    sources.skipFlow.from === false
                ) {
                    lines = getNextBreakableLines(lines, sources.from.lines, leftToRight);
                }
                else if (leftToRight === false && sources.to.lines.length > 0 &&
                    sources.skipFlow.to === false
                ) {
                    lines = getNextBreakableLines(
                        lines, sources.to.lines, leftToRight
                    );
                }
                else {
                    balFlag = true;
                    continue;
                }
                if (lines.length === 0 || isFootnoteTypeLinkExist(lines) === true) {
                    balFlag = true;
                    continue;
                }
                lastIndex = lines.length - 1;
                if (
                    (leftToRight === true &&
                    (sources.lineCount.to + lines.length) < backPageItem.lineLimit) ||
                    (leftToRight === false &&
                    (sources.lineCount.from + lines.length) < frontPageItem.lineLimit)
                ) {
                    if (
                        (leftToRight === true && lines.length === sources.from.lines.length) ||
                        (leftToRight === false && lines.length === sources.to.lines.length)
                    ) {
                        balFlag = true;
                    }
                    continue;
                }
                if (leftToRight === true) {
                    height += lines[0].bottom - lines[lastIndex].top;
                }
                else {
                    height += lines[lastIndex].bottom - lines[0].top;
                }
                if (height >= balancingHeight) {
                    balFlag = true;
                    continue;
                }
                committedHeight = height;
                while (lines.length > 0) {
                    if (leftToRight === true) {
                        line = sources.from.lines.pop();
                        sources.to.lines.splice(0, 0, line);
                        sources.lineCount.from -= 1;
                        sources.lineCount.to += 1;
                    }
                    else {
                        line = sources.to.lines[0];
                        sources.to.lines.splice(0, 1);
                        sources.from.lines.push(line);
                        sources.lineCount.from += 1;
                        sources.lineCount.to -= 1;
                    }
                    lines.pop();
                }
            }
            balanceTemplate(sources, leftToRight, committedHeight);
        }

        function balanceColumns(page, columns, withInItem, balancingHeight) {
            var i = 0, columnBalHeight = 0, firstColumn = null,
                secondColumn = null, frontPageItem = null,
                backPageItem = null, leftToRight = false;

            for (; i < columns.length - 1; i += 1) {
                firstColumn = columns[i];
                secondColumn = columns[i + 1];
                columnBalHeight = getColumnHeight(page, firstColumn);
                columnBalHeight = Math.round(
                    balancingHeight - columnBalHeight
                );
                frontPageItem = getLastRunningPageItem(page, firstColumn);
                backPageItem = getFirstRunningPageItem(page, secondColumn);
                if (
                    withInItem.indexOf(frontPageItem) < 0 ||
                    withInItem.indexOf(backPageItem) < 0
                ) {
                    continue;
                }
                if (columnBalHeight < 0) {
                    columnBalHeight *= -1;
                    leftToRight = true;
                }
                else {
                    leftToRight = false;
                }
                balancePageItems(
                    page, frontPageItem, backPageItem,
                    leftToRight, columnBalHeight + 5
                );
            }
        }

        function balancePage(page, withInItem) {
            var i = 0, columns = null, columnGroups = null,
                balancingHeight = 0;

            columnGroups = page.template.columnGroups;
            for (; i < columnGroups.length; i += 1) {
                columns = templateHandler.getRunningColumns(columnGroups[i]);
                if (columns.length < 2) {
                    continue;
                }
                balancingHeight = getBalancingHeight(page, columns);
                balanceColumns(page, columns, withInItem, balancingHeight);
            }
        }

        this.balance = function balanceFn(pageItem) {
            var position = -1, page = null, withInItem = [];

            page = pageItem.parent;
            position = page.pageItems.indexOf(pageItem) + 1;
            withInItem.push.apply(withInItem, page.pageItems);
            withInItem.splice(position, withInItem.length - position);
            balancePage(pageItem.parent, withInItem);
        };
    };

    return ColumnBalancer;
});
