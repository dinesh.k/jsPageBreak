/*global define*/
define([
    'rainbow/PresetReader', 'rainbow-config/ColumnStretchingConfig'
], function defineFn(PresetReader, ColumnStretchingConfig) {
    var ColumnStretching = function ColumnStretchingClass(
        doc, layoutName, eventBus, templateHandler
    ) {
        var currentPages = null, param = {}, columnStretchConf = null;

        function getColumnStretchItems(column, priority) {
            var stretchConf = null, blockItems = null, i = 0, j = 0,
                stretchItems = [], item = null, parent = null,
                nextNode = null, newNode = null;

            for (; i < columnStretchConf.stretchOn.length; i += 1) {
                stretchConf = columnStretchConf.stretchOn[i];
                if (priority !== stretchConf.priority) {
                    continue;
                }
                blockItems = column.htmlElement.querySelectorAll(
                    stretchConf.selectors.join(',')
                );
                for (j = 0; j < blockItems.length; j += 1) {
                    item = blockItems[j];
                    parent = item.parentNode;
                    nextNode = item.nextSibling;
                    if (stretchConf.before === true) {
                        newNode = doc.createElement('div');
                        newNode.classList.add('stretching');
                        newNode.setAttribute('data-limit', stretchConf.limit);
                        parent.insertBefore(newNode, item);
                        stretchItems.push(newNode);
                    }
                    if (stretchConf.after === true) {
                        newNode = doc.createElement('div');
                        newNode.classList.add('stretching');
                        newNode.setAttribute('data-limit', stretchConf.limit);
                        if (nextNode === null) {
                            parent.appendChild(newNode);
                        }
                        else {
                            parent.insertBefore(newNode, nextNode);
                        }
                        stretchItems.push(newNode);
                    }
                }
            }

            return stretchItems;
        }

        function getPageItemHeight(pageItem) {
            var clientRect = null, height = 0, currentNode = null,
                child = null, i = 0;

            currentNode = pageItem.htmlElement;
            clientRect = currentNode.getBoundingClientRect();
            height = clientRect.height;
            while (height === 0) {
                for (i = 0; i < currentNode.childNodes.length; i += 1) {
                    child = currentNode.childNodes[i];
                    clientRect = child.getBoundingClientRect();
                    height += clientRect.height;
                }
                if (currentNode.childNodes.length !== 1) {
                    break;
                }
                currentNode = currentNode.childNodes[0];
            }

            return height;
        }

        function getColumnHeight(page, column) {
            var pageItems = null, pageItem = null, i = 0, height = 0;

            pageItems = column.pageItems;
            for (; i < pageItems.length; i += 1) {
                pageItem = pageItems[i];
                height += getPageItemHeight(pageItem);
            }

            return height;
        }

        function getStretchHeight(page, columns) {
            var i = 0, column = null, height = 0, maxHeight = 0;

            for (; i < columns.length; i += 1) {
                column = columns[i];
                height = getColumnHeight(page, column);
                if (maxHeight < height) {
                    maxHeight = height;
                }
            }

            return maxHeight;
        }

        function stretchMargin(page, column, stretchItems, stretchHeight) {
            var height = 0, i = 0, item = null, limit = 0, stretchLimit = false,
                columnHeight = 0, count = 1, backupHeight = '';

            height = getColumnHeight(page, column);
            height = stretchHeight - height;
            while (
                height > 0 &&
                stretchLimit === false &&
                stretchItems.length > 0
            ) {
                stretchLimit = true;
                for (i = 0; i < stretchItems.length; i += 1) {
                    item = stretchItems[i];
                    limit = parseInt('0' + item.getAttribute('data-limit'), 10);
                    backupHeight = item.style.height;
                    if (limit >= count) {
                        item.style.height = count + 'px';
                        stretchLimit = false;
                    }
                    columnHeight = getColumnHeight(page, column);
                    if (columnHeight > stretchHeight) {
                        item.style.height = backupHeight;
                    }
                    height -= 1;
                }
                count += 1;
            }
        }

        function stretchColumn(page, column, stretchHeight) {
            var height = 0, stretchItems = null, priority = 1;

            height = getColumnHeight(page, column);
            while (height < stretchHeight && priority <= 10) {
                stretchItems = getColumnStretchItems(column, priority);
                priority += 1;
                stretchMargin(page, column, stretchItems, stretchHeight);
                height = getColumnHeight(page, column);
            }
        }

        function stretchPage() {
            var i = 0, j = 0, columns = null, columnGroups = null,
                stretchHeight = 0, page = null;

            if (currentPages.length > 0) {
                page = currentPages[0];
                columnGroups = page.template.columnGroups;
                for (; i < columnGroups.length; i += 1) {
                    columns = templateHandler.getRunningColumns(columnGroups[i]);
                    if (columns.length < 2) {
                        continue;
                    }
                    stretchHeight = getStretchHeight(page, columns);
                    for (j = 0; j < columns.length; j += 1) {
                        stretchColumn(page, columns[j], stretchHeight);
                    }
                }
                currentPages.splice(0, 1);
                setTimeout(function timeoutFn1() {
                    eventBus.publish('paginate:columnStretching:stretchPage');
                }, 0);
            }
            else {
                setTimeout(function timeoutFn2() {
                    eventBus.publish('paginate:columnStretching:deinit');
                }, 0);
            }
        }

        function init(pages) {
            var presetReader = new PresetReader();

            currentPages = [];
            currentPages.push.apply(currentPages, pages);
            columnStretchConf = presetReader.getPreset(
                layoutName, ColumnStretchingConfig
            );
            setTimeout(function timeoutFn() {
                eventBus.publish('paginate:columnStretching:stretchPage');
            }, 0);
        }

        function deinit() {
            unsubscribeColumnStretchingEvents();
            param.callBack();
        }

        function unsubscribeColumnStretchingEvents() {
            eventBus.unsubscribe('paginate:columnStretching:init', init);
            eventBus.unsubscribe('paginate:columnStretching:stretchPage', stretchPage);
            eventBus.unsubscribe('paginate:columnStretching:deinit', deinit);
        }

        function subscribeColumnStretchingEvents() {
            eventBus.subscribe('paginate:columnStretching:deinit', deinit);
            eventBus.subscribe('paginate:columnStretching:stretchPage', stretchPage);
            eventBus.subscribe('paginate:columnStretching:init', init);
        }

        this.stretchContent = function stretchFn(pages, callBack) {
            subscribeColumnStretchingEvents();
            param.callBack = callBack;
            setTimeout(function timeoutFn() {
                eventBus.publish('paginate:columnStretching:init', pages);
            }, 0);
        };
    };

    return ColumnStretching;
});
