/*global define*/
define([
    'phoenix/Helper', 'rainbow/util/DomHelpers'
], function defineFn(Helper, DomHelper) {
    var FigureBreaker = null, proto = null;

    function getContainerWithFixedWidth(self, width) {
        var dHelper = self.dHelper, container = null;

        container = dHelper.create('div');
        container.style.width = width + 'px';
        dHelper.append(self.source, container);
        dHelper.append(container, self.target);

        return container;
    }

    function checkWhetherItemFitInThisDimension(self, dimension) {
        var targetContainer = null, width = 0, parent = null,
            dHelper = self.dHelper;

        targetContainer = getContainerWithFixedWidth(self, dimension.width);
        width = targetContainer.getBoundingClientRect().width;

        if (Math.round(width) >= Math.round(targetContainer.scrollWidth)) {
            return true;
        }

        parent = targetContainer.parentNode;

        if (Helper.isObject(parent) === true) {
            dHelper.remove(targetContainer, parent);
        }

        return false;
    }

    function getFloatItem(self) {
        var selector = self.selector[0], dHelper = self.dHelper, target = null;

        target = dHelper.find('.' + selector, self.target);

        return target;
    }

    function getFloatHeight(self, item) {
        var height = 0;

        height = item.getBoundingClientRect().height;

        return height;
    }

    function isPageHeightExceedsItemHeight(self, pageHeight, floatHeight) {
        if (pageHeight > floatHeight) {
            return true;
        }

        return false;
    }

    function updateNodes(self, item, caption) {
        var dHelper = self.dHelper, clonedCaption = null, clonedItem = null,
            nodes = [], clonedWrapper = null;

        clonedWrapper = dHelper.clone(item, false);
        clonedCaption = dHelper.clone(caption, true);
        dHelper.remove(caption, item);
        dHelper.append(clonedCaption, clonedWrapper);
        clonedItem = dHelper.clone(item, true);
        nodes.push(clonedItem);
        nodes.push(clonedWrapper);

        dHelper.append(clonedItem, self.target);
        dHelper.append(clonedWrapper, self.target);

        return nodes;
    }

    function updateDimension(data, position) {
        var newData = {}, key = '';

        if (Helper.isObject(data) === true) {
            for (key in data) {
                if (data.hasOwnProperty(key) === true) {
                    newData[key] = data[key];
                }
            }

            newData.position = position;
        }

        return newData;
    }

    function getFullPageFigureParts(
        self, caption, imageHeight, captionHeight, item, dim
    ) {
        var pageHeight = dim.height, datas = {};

        datas.dimensions = [];
        datas.nodes = [];

        if (pageHeight <= imageHeight) {
            datas.nodes = updateNodes(self, item, caption);
            datas.dimensions.push(updateDimension(dim, 'top'));
            datas.dimensions.push(updateDimension(dim, 'bottom'));
        }
        else {
            datas.dimensions.push(updateDimension(dim, 'top'));
            datas.nodes.push(item);
        }

        return datas;
    }

    function getHeights(images) {
        var i = 0, length = 0, heights = [], image = null;

        length = images.length;

        for (i = 0; i < length; i += 1) {
            image = images[i];
            heights.push(image.getBoundingClientRect().height);
        }

        return heights;
    }

    function getWrappers(self, item) {
        var dHelper = self.dHelper, wrappers = [], kids = [], i = 0,
            length = 0, kid = null;

        kids = item.childNodes;
        length = kids.length;

        for (i = 0; i < length; i += 1) {
            kid = kids[i];
            if (dHelper.containClass(kid, self.options.image) === true) {
                wrappers.push(kid);
            }
        }

        return wrappers;
    }

    function getParts(self, item) {
        var partImages = [];

        partImages = getWrappers(self, item);

        return partImages;
    }

    function breakIntoParts(self, parts, item, availableSpace, imagesHeight) {
        var i = 0, floatHeight = 0, dHelper = self.dHelper, clonedItem = null,
            clonedPart = null, length = 0, idValue = '', pageHeight = 0,
            idSelector = self.options.idAttribute;

        clonedItem = dHelper.clone(item, true);
        length = parts.length;
        pageHeight = availableSpace;
        availableSpace -= imagesHeight[0];

        for (i = 1; i < length; i += 1) {
            floatHeight = imagesHeight[i];

            if (availableSpace < floatHeight || i === length - 1) {
                dHelper.addIntoClassList(parts[i], 'part-figure');
                idValue = parts[i].getAttribute(idSelector);
                clonedPart = dHelper.find('#' + idValue, clonedItem);
                dHelper.remove(clonedPart, clonedItem);
            }

            if (pageHeight < floatHeight) {
                dHelper.addIntoClassList(parts[i], 'full-page-part-figure');
            }

            availableSpace -= floatHeight;
        }

        return clonedItem;
    }

    function generateContinuedCaption(self, caption) {
        var continuedCaption = null, label = null,
            continuedText = null, dHelper = self.dHelper;

        continuedCaption = dHelper.clone(caption, false);
        label = dHelper.find(self.options.label, caption);
        dHelper.append(dHelper.clone(label, true), continuedCaption);
        continuedText = dHelper.create('span');
        dHelper.addIntoClassList(continuedText, 'x');
        continuedText.innerHTML = '&nbsp';
        dHelper.append(continuedText, continuedCaption);
        continuedText = dHelper.create('i');
        continuedText.innerHTML = 'Continued';
        dHelper.append(continuedText, continuedCaption);

        return continuedCaption;
    }

    function getHeightOfContinuedCaption(self, caption) {
        var dHelper = self.dHelper, wrapper = null;

        wrapper = dHelper.create('div');
        dHelper.addIntoClassList(wrapper, 'continued-caption');
        dHelper.append(caption, wrapper);
        dHelper.append(wrapper, self.target);

        return wrapper.getBoundingClientRect().height;
    }

    /* TO BE OPTIMISED */
    function updateResultData(
        self, item, availableSpace, datas, caption, dim, id
    ) {
        var dHelper = self.dHelper, partImages = [], i = 0, length = 0,
            continuedCaption = null, imagesHeight = [], newPartImages = [],
            continuedCaptionHeight = 0, clonedWrapper = null, part = null,
            allowedHeight = availableSpace, isFullPagePartImage = false,
            fullPagePartCloneWrapper = null;

        partImages = dHelper.findAll('.part-figure', item);
        length = partImages.length;

        continuedCaption = generateContinuedCaption(self, caption);
        continuedCaptionHeight = getHeightOfContinuedCaption(
            self, continuedCaption);
        allowedHeight -= continuedCaptionHeight;
        imagesHeight = getHeights(partImages);
        clonedWrapper = dHelper.clone(item, false);

        for (i = 0; i < length; i += 1) {
            part = partImages[i];

            if (
                availableSpace < imagesHeight[i] &&
                allowedHeight < imagesHeight[i]
            ) {
                isFullPagePartImage = true;
                dHelper.removeFromClassList(part, 'part-figure');
                dHelper.append(dHelper.clone(part, true), clonedWrapper);
                break;
            }

            if (
                allowedHeight < imagesHeight[i] &&
                availableSpace > imagesHeight[i]
            ) {
                break;
            }
            else {
                allowedHeight -= imagesHeight[i];
                dHelper.removeFromClassList(part, 'part-figure');
                dHelper.append(dHelper.clone(part, true), clonedWrapper);
            }
        }

        if (isFullPagePartImage === true) {
            datas.nodes.push(clonedWrapper);
            datas.dimensions.push(updateDimension(dim, 'top'));
            fullPagePartCloneWrapper = dHelper.clone(item, false);
            dHelper.append(continuedCaption, fullPagePartCloneWrapper);
            datas.nodes.push(fullPagePartCloneWrapper);
            datas.dimensions.push(updateDimension(dim, 'bottom'));
        }

        else {
            dHelper.addOrModifyAttribute('data-is-part-figure', 'true', clonedWrapper);
            dHelper.append(continuedCaption, clonedWrapper);
            datas.nodes.push(clonedWrapper);
            datas.dimensions.push(updateDimension(dim, 'top'));
        }

        newPartImages = Array.prototype.slice.call(partImages);
        newPartImages = newPartImages.splice(i, 1);
        length = newPartImages.length;

        if (length === 0) {
            return datas;
        }

        return updateResultData.call(
            this, self, item, availableSpace, datas, caption, dim, id
        );
    }

    function getDatasOfFirstPart(
        self, caption, captionHeight, firstPart, pageHeight, dim
    ) {
        var wrappers = [], length = 0, i = 0, wrapper = null, heights = 0,
            datas = {
                'dimensions': [],
                'nodes': []
            }, items = null, dHelper = self.dHelper, captionWrapper = null;

        wrappers = getWrappers(self, firstPart);
        length = wrappers.length;
        dHelper.append(firstPart, self.target);

        if (length > 0) {
            for (i = 0; i < length; i += 1) {
                wrapper = wrappers[i];
                heights += wrapper.getBoundingClientRect().height;
            }

            if (heights > pageHeight) {
                items = dHelper.clone(firstPart, false);
                for (i = 0; i < length; i += 1) {
                    wrapper = wrappers[i];
                    dHelper.append(dHelper.clone(wrapper, true), items);
                }

                datas.nodes.push(items);
                datas.dimensions.push(updateDimension(dim, 'top'));
                captionWrapper = dHelper.clone(firstPart, false);
                dHelper.append(dHelper.clone(caption, true), captionWrapper);
                datas.nodes.push(captionWrapper);
                datas.dimensions.push(updateDimension(dim, 'bottom'));
                dHelper.remove(firstPart, self.target);

                return datas;
            }
        }

        datas.dimensions.push(updateDimension(dim, 'top'));
        datas.nodes.push(firstPart);
        dHelper.remove(firstPart, self.target);

        return datas;
    }

    function getPartImages(
        self, pageHeight, images, caption, captionHeight, item, dim
    ) {
        var parts = [], partItems = [], imagesHeight = [], idValue = '',
            idAttribute = self.options.idAttribute,
            datas = {
                'dimensions': [],
                'nodes': []
            };

        parts = getParts(self, item);
        imagesHeight = getHeights(images);
        idValue = item.getAttribute(idAttribute);

        if (parts.length > 0) {
            partItems = breakIntoParts(
                self, parts, item, pageHeight, imagesHeight
            );
            datas = getDatasOfFirstPart(
                self, caption, captionHeight, partItems, pageHeight, dim
            );

            return updateResultData(
                self, item, pageHeight, datas, caption, dim, idValue
            );
        }

        return datas;
    }

    function splitIntoParts(self, pageHeight, item, dim) {
        var imageHeight = 0, captionHeight = 0, caption = null,
            dHelper = self.dHelper, captionSelector = self.options.caption,
            images = [], length = 0, parts = {};

        images = getWrappers(self, item);

        length = images.length;

        caption = dHelper.find(captionSelector, item);
        captionHeight = caption.getBoundingClientRect().height;

        if (length === 1) {
            imageHeight = images[0].getBoundingClientRect().height;
            parts = getFullPageFigureParts(
                self, caption, imageHeight, captionHeight, item, dim
            );

            return parts;
        }

        if (length > 1) {
            parts = getPartImages(
                self, pageHeight, images, caption, captionHeight, item, dim
            );

            return parts;
        }

        return parts;
    }

    function generateDimesionData(data, dimension, node) {
        data.dimensions = [];
        data.dimensions.push(dimension);
        data.nodes = [];
        data.nodes.push(node);

        return data;
    }

    function collectFloatParts(self, height, item, dim) {
        var pageHeight = dim.height, hasFittableHeight = false, parts = {};

        hasFittableHeight = isPageHeightExceedsItemHeight(
            self, pageHeight, height
        );

        if (hasFittableHeight === true) {
            parts = generateDimesionData(parts, dim, item);

            return parts;
        }

        if (hasFittableHeight === false) {
            parts = splitIntoParts(self, pageHeight, item, dim);

            return parts;
        }

        return parts;
    }

    function execute(self) {
        var dimensions = self.dimensions, length = 0, i = 0, dim = null,
            isFittableWidth = false, floatHeight = 0, parts = {}, item = null,
            dHelper = self.dHelper;

        length = dimensions.length;
        parts.dimensions = [];
        parts.nodes = [];

        if (length > 0) {
            for (i = 0; i < length; i += 1) {
                dim = dimensions[i];
                isFittableWidth = checkWhetherItemFitInThisDimension(self, dim);
                if (isFittableWidth === true) {
                    item = getFloatItem(self);
                    floatHeight = getFloatHeight(self, item);
                    parts = collectFloatParts(self, floatHeight, item, dim);
                    dHelper.remove(item, item.parentNode);

                    return parts;
                }
            }
        }

        return parts;
    }

    FigureBreaker = function FigureBreakerFn(target) {
        this.target = target;
        this.dHelper = new DomHelper();
    };

    proto = FigureBreaker.prototype;

    proto.figureSplitter = function figureSplitterFn(dimensions, node, options) {
        var splittedParts = {
            'dimensions': [],
            'nodes': []
        };

        if (Helper.isObject(dimensions) === false) {
            throw new Error('Dimensions needed to break the figure');
        }

        if (Helper.isObject(options) === false) {
            throw new Error('options needed to break the figure');
        }

        if (Helper.isObject(node) === false) {
            throw new Error('Node cannot be empty');
        }

        this.dimensions = dimensions;
        this.options = options;
        this.source = node;
        this.selector = node.classList;

        splittedParts = execute(this);

        this.dHelper.empty(this.target);

        return splittedParts;
    };

    return FigureBreaker;
});
