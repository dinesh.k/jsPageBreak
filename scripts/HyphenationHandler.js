/*global define*/
define([
    'rainbow/PresetReader', 'rainbow-config/HyphenationConfig',
    'rainbow-library/hypher'
], function defineFn(PresetReader, HyphenationConfig) {
    var HyphenationHandler = function HyphenationHandlerClass(win, layoutName) {
        var config = null;

        function init() {
            var presetReader = new PresetReader();

            config = presetReader.getPreset(layoutName, HyphenationConfig);
        }

        function removeUnwantedZeroWidthSpace(text) {
            var splits = text.split('/\u200B');

            text = splits.join('/');

            return text;
        }

        function hyphenateText(text) {
            var languages = win.Hypher.languages, hypher = null;

            if (
                languages === null ||
                typeof languages === 'undefined'
            ) {
                return text;
            }
            hypher = languages[config.lang];
            if (
                hypher === null ||
                typeof hypher === 'undefined'
            ) {
                return text;
            }
            text = hypher.hyphenateText(text);

            return text;
        }

        function identifyTextNode(node, excludeElements) {
            var i = 0, length = 0, text = null;

            if (excludeElements.indexOf(node) >= 0) {
                return;
            }
            length = node.childNodes.length;
            for (; i < length; i += 1) {
                identifyTextNode(node.childNodes[i], excludeElements);
            }
            if (node.nodeType === 3) {
                text = node.nodeValue;
                text = hyphenateText(text);
                text = removeUnwantedZeroWidthSpace(text);
                node.nodeValue = text;
            }
        }

        this.hyphenate = function hyphenateFn(container) {
            var elements = null, excludeElements = [], i = 0;

            if (config.excludeSelectors.length > 0) {
                elements = container.querySelectorAll(
                    config.excludeSelectors.join(',')
                );
                excludeElements.push.apply(excludeElements, elements);
            }
            if (config.selectors.length === 0) {
                return;
            }
            elements = container.querySelectorAll(
                config.selectors.join(',')
            );
            for (; i < elements.length; i += 1) {
                identifyTextNode(elements[i], excludeElements);
            }
        };

        init();
    };

    return HyphenationHandler;
});
