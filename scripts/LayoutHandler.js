/*global define*/
define([
    'rainbow/PresetReader', 'rainbow/config/common/GlobalConfig',
    'rainbow-config/LayoutConfig'
], function defineFn(PresetReader, GlobalConfig, LayoutConfig) {
    var LayoutHandler = function LayoutHandlerClass(doc, templateName) {
        var config = null, holders = null;

        function init() {
            var presetReader = new PresetReader();

            config = presetReader.getLayoutPreset(templateName, LayoutConfig);
        }

        function splitIds(ids) {
            if (ids !== null && typeof ids !== 'undefined' && ids.length > 0) {
                return ids.split(',');
            }

            return [];
        }

        function getClassNameOtherThan(node, className) {
            var i = 0, clsName = null;

            for (; i < node.classList.length; i += 1) {
                clsName = node.classList[i];
                if (className !== clsName) {
                    return clsName;
                }
            }

            return null;
        }

        function initItem(updateObj, lyt, className) {
            var items = null, i = 0, item = null;

            if (lyt !== null) {
                items = lyt.querySelectorAll('.' + className);
            }
            for (; items !== null && i < items.length; i += 1) {
                item = getClassNameOtherThan(items[i], className);
                if (item === null) {
                    continue;
                }
                if (typeof updateObj[item] === 'undefined') {
                    updateObj[item] = [];
                }
                updateObj[item].push(items[i]);
            }
        }

        function validateContainer(target) {
            var i = 0, length = 0, nodes = null, node = null, exist = false;

            nodes = doc.querySelectorAll(GlobalConfig.outputSelector);
            length = nodes.length;
            for (; i < length; i += 1) {
                node = nodes[i];
                if (node === target) {
                    exist = true;
                    break;
                }
            }
            if (exist === false) {
                throw new Error(
                    'Pagination output element given as a parameter should be "' +
                    GlobalConfig.outputSelector + '" element.');
            }
        }

        this.initializeContainer = function initializeContainerFn(target) {
            validateContainer(target);
            target.innerHTML = GlobalConfig.skeleton.template.join('');
            holders = {};
            holders.root = target;
            holders.paginationRoot = target.querySelector(
                GlobalConfig.skeleton.rootSelector
            );
            holders.source = target.querySelector(
                GlobalConfig.skeleton.sourceSelector
            );
            holders.destination = target.querySelector(
                GlobalConfig.skeleton.destinationSelector
            );
            holders.layout = target.querySelector(
                GlobalConfig.skeleton.layoutSelector
            );
            holders.template = target.querySelector(
                GlobalConfig.skeleton.templateSelector
            );
            holders.paginationRoot.classList.add(config.name);
        };

        this.getLayoutName = function getLayoutNameFn() {
            return config.name;
        };

        this.getLayoutData = function getLayoutDataFn() {
            return config.data;
        };

        this.getPlaceholders = function getPlaceholdersFn() {
            if (holders === null) {
                throw new Error('Pagination containers not initialized.');
            }
            return holders;
        };

        this.isReferenceStylingEnable = function isReferenceStylingEnableFn() {
            return config.referenceStyling;
        };

        this.getPages = function getPagesFn(layout) {
            var items = [];

            if (layout !== null) {
                items.push.apply(
                    items, layout.querySelectorAll('.' + GlobalConfig.page.className)
                );
            }

            return items;
        };

        this.getCategorisedPages = function getCategorisedPagesFn(layout) {
            var pages = {};

            if (layout !== null) {
                initItem(pages, layout, GlobalConfig.page.className);
            }

            return pages;
        };

        this.flowPage = function flowPageFn(page, destination) {
            if (page === null || typeof page === 'undefined') {
                return null;
            }

            return destination.appendChild(page.cloneNode(true));
        };

        this.getPageTextarea = function getPageTextareaFn(page) {
            if (page !== null) {
                return page.querySelector(GlobalConfig.page.textareaSelector);
            }

            return null;
        };

        this.getPageColumnGroup = function getPageColumnGroupFn(page) {
            var items = [];

            if (page !== null) {
                items.push.apply(
                    items, page.querySelectorAll(GlobalConfig.page.columnGroupSelector)
                );
            }

            return items;
        };

        this.getPageColumns = function getPageColumnsFn(page) {
            var items = [];

            if (page !== null) {
                items.push.apply(
                    items, page.querySelectorAll(GlobalConfig.page.columnSelector)
                );
            }

            return items;
        };

        this.getPageItems = function getPageItemsFn(page) {
            var items = [];

            if (page !== null) {
                items.push.apply(
                    items, page.querySelectorAll('.' + GlobalConfig.pageItem.className)
                );
            }

            return items;
        };

        this.getCategorisedPageItems = function getCategorisedPageItemsFn(page) {
            var pageItems = {};

            if (page !== null) {
                initItem(pageItems, page, GlobalConfig.pageItem.className);
            }

            return pageItems;
        };

        this.getPageItemLine = function getPageItemLineFn(pageItem) {
            var lineType = '';

            lineType = pageItem.getAttribute(GlobalConfig.pageItem.lineAttribute);
            if (lineType === null || typeof lineType === 'undefined') {
                lineType = '';
            }

            return lineType;
        };

        this.getPageItemLineMinLimit = function getPageItemLineMinLimitFn(pageItem) {
            var linelimit = '';

            linelimit = pageItem.getAttribute(GlobalConfig.pageItem.lineMinLimitAttribute);
            if (linelimit === null || typeof linelimit === 'undefined') {
                linelimit = '';
            }
            linelimit = parseInt('0' + linelimit, 10);

            return linelimit;
        };

        this.isPageItemContentStart = function isPageItemContentStartFn(pageItem) {
            return (!pageItem.hasAttribute(GlobalConfig.pageItem.contentNeverStartAttribute));
        };

        this.getPageItemLink = function getPageItemLinkFn(pageItem) {
            var linkIds = pageItem.getAttribute(GlobalConfig.pageItem.linkAttribute);

            return splitIds(linkIds);
        };

        init();
    };

    return LayoutHandler;
});
