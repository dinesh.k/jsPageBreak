/*global define*/
define([
    'rainbow/PresetReader', 'rainbow-config/LineTypeConfig'
], function defineFn(PresetReader, LineTypeConfig) {
    var LineTypeHandler = function LineTypeHandlerClass(layoutName) {
        var lineTypeNodes = null, lineTypeConf = null;

        function init() {
            var presetReader = new PresetReader();

            lineTypeConf = presetReader.getPreset(layoutName, LineTypeConfig);
        }

        function cloneObject(obj) {
            var propkey = null, propval = null, result = {};

            for (propkey in obj) {
                if (obj.hasOwnProperty(propkey) === false) {
                    continue;
                }
                propval = obj[propkey];
                if (typeof propval === typeof {}) {
                    result[propkey] = cloneObject(propval);
                }
                else {
                    result[propkey] = propval;
                }
            }

            return result;
        }

        function cloneLineType(lineType) {
            var result = null;

            result = cloneObject(lineType.current);
            lineType.previous.push(result);
        }

        function initialLineType(obj) {
            var i = 0, lneType = null;

            if (typeof obj.current === 'undefined') {
                obj.current = {};
                for (; i < lineTypeConf.lineTypes.length; i += 1) {
                    lneType = lineTypeConf.lineTypes[i];
                    obj.current[lneType.typeName] = {
                        'count': 0,
                        'start': false,
                        'end': false
                    };
                }
            }
            if (typeof obj.previous === 'undefined') {
                obj.previous = [];
            }
        }

        function isStartAndEndInSingleLine(config, lineTypes) {
            var i = 0, lineType = null, obj = null, flag = false,
                result = false;

            for (; i < lineTypes.length; i += 1) {
                lineType = lineTypes[i];
                obj = lineType[config.typeName];
                if (obj.start === true) {
                    flag = true;
                    result = false;
                }
                if (flag === true && obj.end === true) {
                    result = true;
                }
            }

            return result;
        }

        function getCurrentLineBreakable(config, lineTypes, result) {
            var i = 0, lineType = null, obj = null;

            for (; i < lineTypes.length; i += 1) {
                lineType = lineTypes[i];
                obj = lineType[config.typeName];
                if (
                    config.breakable === false &&
                    (obj.count > 0 || obj.end === true)
                ) {
                    result.current = 1;
                }
                if (
                    config.breakable === true &&
                    obj.end === true
                ) {
                    result.current = -1;
                }
                if (config.widows > 0 && obj.start === true) {
                    if (
                        isStartAndEndInSingleLine(config, lineTypes) === false
                    ) {
                        result.current = 1;
                    }
                }
            }
        }

        function getPreviousLineBreakable(config, lineTypes, result) {
            var i = 0, lineType = null, obj = null;

            for (; i < lineTypes.length; i += 1) {
                lineType = lineTypes[i];
                obj = lineType[config.typeName];
                if (config.keepBefore > 0 && obj.start === true) {
                    result.previous = config.keepBefore;
                }
                if (config.orphans > 1 && obj.end === true) {
                    if (
                        isStartAndEndInSingleLine(config, lineTypes) === false
                    ) {
                        result.previous = config.orphans - 1;
                    }
                }
            }
        }

        function getNextLineBreakable(config, lineTypes, result) {
            var i = 0, lineType = null, obj = null;

            for (; i < lineTypes.length; i += 1) {
                lineType = lineTypes[i];
                obj = lineType[config.typeName];
                if (config.keepAfter > 1 && obj.end === true) {
                    result.next = config.keepAfter - 1;
                }
                if (config.widows > 2 && obj.start === true) {
                    if (
                        isStartAndEndInSingleLine(config, lineTypes) === false
                    ) {
                        result.next = config.widows - 2;
                    }
                }
            }
        }

        this.initLineTypes = function initLineTypesFn(container) {
            var i = 0, lineType = null, nodes = null;

            lineTypeNodes = {};
            for (; i < lineTypeConf.lineTypes.length; i += 1) {
                lineType = lineTypeConf.lineTypes[i];
                nodes = [];
                lineTypeNodes[lineType.typeName] = nodes;
                nodes.push.apply(
                    nodes, container.querySelectorAll(lineType.selectors.join(','))
                );
            }
        };

        this.getLineTypeSelector = function getLineTypeSelectorFn(lineTypeName) {
            var i = 0, lineType = null;

            for (; i < lineTypeConf.lineTypes.length; i += 1) {
                lineType = lineTypeConf.lineTypes[i];
                if (lineTypeName === lineType.typeName) {
                    return lineType.selectors.join(',');
                }
            }

            return null;
        };

        this.getLineTypeContinueAfter = function getLineTypeContinueAfterFn(lineTypeName) {
            var i = 0, lineType = null, result = [];

            for (; i < lineTypeConf.lineTypes.length; i += 1) {
                lineType = lineTypeConf.lineTypes[i];
                if (lineTypeName === lineType.typeName) {
                    if (
                        lineType.continueAfter !== null &&
                        typeof lineType.continueAfter !== 'undefined'
                    ) {
                        result.push.apply(result, lineType.continueAfter);
                    }
                    break;
                }
            }

            return result;
        };

        this.getStartingTypes = function getStartingTypesFn(lineTypes) {
            var i = 0, lineType = null, result = [],
                propkey = null, propval = null;

            for (; i < lineTypes.length; i += 1) {
                lineType = lineTypes[i];
                for (propkey in lineType) {
                    if (lineType.hasOwnProperty(propkey) === false) {
                        continue;
                    }
                    propval = lineType[propkey];
                    if (propval.start === true && propval.count === 1) {
                        result.push(propkey);
                    }
                }
            }

            return result;
        };

        this.getEndingTypes = function getEndingTypesFn(lineTypes) {
            var i = 0, lineType = null, result = [],
                propkey = null, propval = null;

            for (; i < lineTypes.length; i += 1) {
                lineType = lineTypes[i];
                for (propkey in lineType) {
                    if (lineType.hasOwnProperty(propkey) === false) {
                        continue;
                    }
                    propval = lineType[propkey];
                    if (propval.end === true && propval.count === 0) {
                        result.push(propkey);
                    }
                }
            }

            return result;
        };

        this.existingLineType = function existingLineTypeFn(lineType) {
            cloneLineType(lineType);
        };

        this.updateLineType = function updateLineTypeFn(node, lineType, onStart) {
            var i = 0, lneType = null, obj = null, updateFlag = false;

            initialLineType(lineType);
            for (; i < lineTypeConf.lineTypes.length; i += 1) {
                lneType = lineTypeConf.lineTypes[i];
                obj = lineType.current[lneType.typeName];
                if (lineTypeNodes[lneType.typeName].indexOf(node) >= 0) {
                    if (onStart === true) {
                        obj.count += 1;
                        obj.start = true;
                        obj.end = false;
                    }
                    else {
                        obj.count -= 1;
                        obj.start = false;
                        obj.end = true;
                    }
                    updateFlag = true;
                }
                else if (obj.start === true || obj.end === true) {
                    obj.start = false;
                    obj.end = false;
                    updateFlag = true;
                }
            }
            if (updateFlag === true) {
                cloneLineType(lineType);
            }
        };

        this.clearPreviousLineType = function clearPreviousLineTypeFn(lineType) {
            var result = [];

            result.push.apply(result, lineType.previous);
            lineType.previous.splice(0, lineType.previous.length);

            return result;
        };

        this.getLineBreakable = function getLineBreakableFn(lineTypes) {
            var i = 0, lineType = null,
                result = {'previous': 0, 'current': 0, 'next': 0};

            for (; i < lineTypeConf.lineTypes.length; i += 1) {
                lineType = lineTypeConf.lineTypes[i];
                getCurrentLineBreakable(lineType, lineTypes, result);
                getPreviousLineBreakable(lineType, lineTypes, result);
                getNextLineBreakable(lineType, lineTypes, result);
            }

            return result;
        };

        init();
    };

    return LineTypeHandler;
});
