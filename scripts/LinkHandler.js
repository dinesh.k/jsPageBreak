/*global define*/
define([
    'rainbow/PresetReader', 'rainbow-config/LinkConfig'
], function defineFn(PresetReader, LinkConfig) {
    var LinkHandler = function LinkHandlerClass(doc, layoutName, templateHandler) {
        var availablelinks = null, canIPlaceCommitedIDs = null, linkConf = null;

        function init() {
            var presetReader = new PresetReader();

            linkConf = presetReader.getPreset(layoutName, LinkConfig);
        }

        function getMappingGroup(groupId) {
            var i = 0, group = null;

            if (availablelinks === null) {
                return null;
            }
            for (; i < availablelinks.length; i += 1) {
                group = availablelinks[i];
                if (group.id === groupId) {
                    return group;
                }
            }
            group = {};
            group.id = groupId;
            group.links = [];
            availablelinks.push(group);

            return group;
        }

        function getLinkMappings(linkIds) {
            var i = 0, links = [], link = null;

            if (linkIds !== null && linkIds.length > 0) {
                for (; i < linkConf.linkMappings.length; i += 1) {
                    link = linkConf.linkMappings[i];
                    if (linkIds.indexOf(link.id) >= 0) {
                        links.push(link);
                    }
                }
            }

            return links;
        }

        function sortLinks(link) {
            var linkMappings = null, group = null, index = -1, i = 0;

            linkMappings = getLinkMappings([link.id]);
            if (linkMappings.length === 0) {
                return;
            }
            for (; i < linkMappings.length; i += 1) {
                group = getMappingGroup(linkMappings[i].groupid);
                if (group === null) {
                    continue;
                }
                index = group.links.indexOf(link);
                if (index < 0) {
                    continue;
                }
                group.links.splice(index, 1);
                group.links.push(link);
            }
        }

        function getNextLinkOfMapping(linkMapping, placable) {
            var i = 0, link = null, group = null;

            group = getMappingGroup(linkMapping.groupid);
            for (; i < group.links.length; i += 1) {
                link = group.links[i];
                if (link.lines.length === 0) {
                    continue;
                }
                if (placable === false || link.placable === placable) {
                    if (linkMapping.id === link.id) {
                        return link;
                    }
                    break;
                }
            }

            return null;
        }

        function getNextLinkOfID(linkIds, placable) {
            var linkMappings = null, link = null, i = 0;

            linkMappings = getLinkMappings(linkIds);
            if (linkMappings.length === 0) {
                return null;
            }
            for (; i < linkMappings.length; i += 1) {
                link = getNextLinkOfMapping(
                    linkMappings[i], placable
                );
                if (link !== null) {
                    return link;
                }
            }

            return null;
        }

        function isFootnoteTypeLinkExist() {
            var i = 0, links = [], link = null;

            for (; i < linkConf.linkMappings.length; i += 1) {
                link = linkConf.linkMappings[i];
                link = getNextLinkOfID(link.id, true);
                if (link !== null) {
                    links.push(link);
                }
            }
            for (i = 0; i < links.length; i += 1) {
                link = links[i];
                if (link.footnoteType === true) {
                    return true;
                }
            }

            return false;
        }

        function getLinkAllowedWidths(linkId) {
            var items = [], widths = [], i = 0,
                item = null, ids = null, width = null;

            items.push.apply(items, templateHandler.getAllPageDynamicItems());
            for (i = 0; i < items.length; i += 1) {
                item = items[i];
                ids = item.links;
                if (ids.indexOf(linkId) >= 0) {
                    width = Math.round(templateHandler.getPageItemWidth(item));
                    if (widths.indexOf(width) < 0) {
                        widths.push(width);
                    }
                }
            }
            widths.sort(function sortFn(itema, itemb) {
                return itema - itemb;
            });

            return widths;
        }

        function removeDuplicateDimensions(dimensions) {
            var dimension = null, i = 0, count = 0;

            for (i = 0; i < dimensions.length; i += 1) {
                dimension = dimensions[i];
                count = i + 1;
                while (count < dimensions.length) {
                    if (
                        dimension.width === dimensions[count].width &&
                        dimension.height === dimensions[count].height
                    ) {
                        dimensions.splice(count, 1);
                    }
                    else {
                        count += 1;
                    }
                }
            }
        }

        function getLinkAllowedDimensions(linkId) {
            var items = [], dimensions = [], i = 0,
                item = null, ids = null, width = null, height = null;

            items.push.apply(items, templateHandler.getAllPageDynamicItems());
            for (i = 0; i < items.length; i += 1) {
                item = items[i];
                ids = item.links;
                if (ids.indexOf(linkId) >= 0) {
                    width = Math.round(templateHandler.getPageItemWidth(item));
                    height = Math.round(templateHandler.getParentTextAreaHeight(item));
                    dimensions.push({
                        'width': width,
                        'height': height
                    });
                }
            }
            removeDuplicateDimensions(dimensions);
            dimensions.sort(function sortFn(itema, itemb) {
                return itema.width - itemb.width;
            });

            return dimensions;
        }

        function getAllowedWidth(allowedWidths, width) {
            var i = 0, alloWidth = 0;

            for (; i < allowedWidths.length; i += 1) {
                alloWidth = allowedWidths[i];
                if (Math.round(width) === Math.round(alloWidth)) {
                    break;
                }
                if (Math.round(width) < Math.round(alloWidth)) {
                    break;
                }
            }

            return alloWidth;
        }

        function getFittableWidth(allowedWidths, target) {
            var i = 0, alloWidth = 0, backup = '', clientRect = null;

            backup = target.getAttribute('style');
            for (; i < allowedWidths.length; i += 1) {
                alloWidth = allowedWidths[i];
                target.setAttribute('style', 'width:' + alloWidth + 'px');
                clientRect = target.getBoundingClientRect();
                if (Math.round(clientRect.width) < Math.round(target.scrollWidth)) {
                    continue;
                }
                target.setAttribute('style', backup);

                return alloWidth;
            }
            target.setAttribute('style', backup);
            if (allowedWidths.length > 0) {
                return allowedWidths[allowedWidths.length - 1];
            }

            return 0;
        }

        function getLinkDestination(
            container, source, destinations,
            linkSetting, allowedWidths, linksInfo
        ) {
            var i = 0, srcKey = '', destKey = '', dests = [], newNode = null,
                destination = null, fitWidth = 0, continued = false, width = 0,
                position = null, top = false, bottom = false;

            if (source !== null) {
                srcKey = source.getAttribute(linkSetting.source.attribute);
            }
            for (; i < destinations.length; i += 1) {
                destination = destinations[i];
                destKey = destination.getAttribute(linkSetting.destination.attribute);
                if (srcKey === destKey || source === null) {
                    newNode = doc.createElement('div');
                    newNode.appendChild(destination);
                    newNode = container.appendChild(newNode);
                    fitWidth = getFittableWidth(allowedWidths, destination);
                    width = fitWidth;
                    if (destination.hasAttribute('data-width') === true) {
                        width = parseFloat(
                            destination.getAttribute('data-width')
                        );
                        width = getAllowedWidth(allowedWidths, width);
                    }
                    newNode.setAttribute('style', 'width:' + width + 'px');
                    top = false;
                    bottom = false;
                    position = destination.getAttribute('data-position');
                    if (position === 'top') {
                        top = true;
                    }
                    if (position === 'bottom') {
                        bottom = true;
                    }
                    dests.push(destination);
                    if (width === 0) {
                        continue;
                    }
                    if (
                        dests.length > 1 &&
                        destination.hasAttribute('data-continued') === true
                    ) {
                        continued = true;
                    }
                    linksInfo.push({
                        'id': linkSetting.id,
                        'source': source,
                        'destination': newNode,
                        'lines': [],
                        'prepend': {
                            'templateString': linkSetting.template.prepend.join(''),
                            'template': null,
                            'height': 0
                        },
                        'append': {
                            'templateString': linkSetting.template.append.join(''),
                            'template': null,
                            'height': 0
                        },
                        'footnoteType': linkSetting.footnoteType,
                        'completeBefore': linkSetting.completeBefore,
                        'width': width,
                        'underPlacement': false,
                        'placable': false,
                        'top': top,
                        'bottom': bottom,
                        'continued': continued
                    });
                }
            }
            while (dests.length > 0) {
                i = destinations.indexOf(dests[0]);
                destinations.splice(i, 1);
                dests.splice(0, 1);
            }
        }

        function getLinkSourceAndDestination(
            container, sources, destinations,
            linkSetting, allowedWidths, linksInfo
        ) {
            var i = 0, dests = [];

            dests.push.apply(dests, destinations);
            for (; i < sources.length; i += 1) {
                getLinkDestination(
                    container, sources[i], dests,
                    linkSetting, allowedWidths, linksInfo
                );
            }
            getLinkDestination(
                container, null, dests, linkSetting,
                allowedWidths, linksInfo
            );
        }

        this.initLinks = function initLinksFn(source) {
            var i = 0, sources = null, destinations = null,
                allowedWidths = null, linkMapping = null, group = null;

            availablelinks = [];
            canIPlaceCommitedIDs = [];
            for (i = 0; i < linkConf.linkMappings.length; i += 1) {
                linkMapping = linkConf.linkMappings[i];
                group = getMappingGroup(linkMapping.groupid);
                allowedWidths = getLinkAllowedWidths(linkMapping.id);
                sources = source.querySelectorAll(linkMapping.source.selector);
                destinations = source.querySelectorAll(linkMapping.destination.selector);
                getLinkSourceAndDestination(
                    source, sources, destinations,
                    linkMapping, allowedWidths, group.links
                );
            }
        };

        this.getAllLinkDimensions = function getAllLinkDimensionsFn() {
            var i = 0, linkMapping = null, result = {};

            for (i = 0; i < linkConf.linkMappings.length; i += 1) {
                linkMapping = linkConf.linkMappings[i];
                result[linkMapping.id] = getLinkAllowedDimensions(linkMapping.id);
            }

            return result;
        };

        this.isLinkExist = function isLinkExistFn() {
            var i = 0, j = 0, group = null, link = null;

            for (;
                availablelinks !== null && i < availablelinks.length; i += 1
            ) {
                group = availablelinks[i];
                for (j = 0; j < group.links.length; j += 1) {
                    link = group.links[j];
                    if (link.lines.length > 0) {
                        return true;
                    }
                }
            }

            return false;
        };

        this.isContinuingBottomLinkExist = function isContinuingBottomLinkExistFn() {
            var i = 0, j = 0, group = null, link = null;

            for (;
                availablelinks !== null && i < availablelinks.length; i += 1
            ) {
                group = availablelinks[i];
                for (j = 0; j < group.links.length; j += 1) {
                    link = group.links[j];
                    if (
                        link.lines.length === 0 ||
                        link.placable === false
                    ) {
                        continue;
                    }
                    if (link.footnoteType === false && link.continued === true &&
                        link.bottom === true
                    ) {
                        return true;
                    }
                    break;
                }
            }

            return false;
        };

        this.isContinuingLinkExist = function isContinuingLinkExistFn() {
            var i = 0, j = 0, group = null, link = null;

            for (;
                availablelinks !== null && i < availablelinks.length; i += 1
            ) {
                group = availablelinks[i];
                for (j = 0; j < group.links.length; j += 1) {
                    link = group.links[j];
                    if (
                        link.lines.length === 0 ||
                        link.placable === false
                    ) {
                        continue;
                    }
                    if (link.footnoteType === false && link.continued === true) {
                        return true;
                    }
                    break;
                }
            }

            return false;
        };

        this.getMatchingLinks = function getMatchingLinksFn(node) {
            var i = 0, j = 0, group = null, link = null, links = [];

            for (;
                availablelinks !== null && i < availablelinks.length; i += 1
            ) {
                group = availablelinks[i];
                for (j = 0; j < group.links.length; j += 1) {
                    link = group.links[j];
                    if (link.source === node) {
                        links.push(link);
                    }
                }
            }

            return links;
        };

        this.getNextLinkWidth = function getNextLinkWidthFn() {
            var i = 0, links = [], link = null, width = 0;

            for (; i < linkConf.linkMappings.length; i += 1) {
                link = linkConf.linkMappings[i];
                link = getNextLinkOfID(link.id, true);
                if (link !== null) {
                    links.push(link);
                }
            }
            for (i = 0; i < links.length; i += 1) {
                link = links[i];
                if (width < link.width) {
                    width = link.width;
                }
            }

            return width;
        };

        this.getAllLinks = function getAllLinksFn() {
            var i = 0, group = null, links = [];

            for (;
                availablelinks !== null && i < availablelinks.length; i += 1
            ) {
                group = availablelinks[i];
                links.push.apply(links, group.links);
            }

            return links;
        };

        this.setLinksPlacable = function setLinksPlacableFn(lnks, value) {
            var j = 0, lnk = null;

            for (; j < lnks.length; j += 1) {
                lnk = lnks[j];
                if (canIPlaceCommitedIDs.indexOf(lnk.id) >= 0) {
                    continue;
                }
                if (value === true && lnk.placable === false) {
                    sortLinks(lnk);
                }
                lnk.placable = value;
            }
        };

        this.canIPlace = function canIPlaceFn(lineTypes) {
            var i = 0, j = 0, group = null, link = null, flag = true;

            for (;
                availablelinks !== null && i < availablelinks.length; i += 1
            ) {
                group = availablelinks[i];
                for (j = 0; j < group.links.length; j += 1) {
                    link = group.links[j];
                    if (
                        (link.lines.length > 0 ||
                        link.underPlacement === true) &&
                        lineTypes.indexOf(link.completeBefore) >= 0
                    ) {
                        flag = false;
                        link.placable = true;
                        canIPlaceCommitedIDs.push(link.id);
                    }
                }
            }

            return flag;
        };

        this.getNextLink = function getNextLinkFn(linkIds, width) {
            var link = null;

            link = getNextLinkOfID(linkIds, true);
            if (
                link === null ||
                Math.round(width) !== Math.round(link.width)
            ) {
                return null;
            }
            if (
                link.footnoteType === false &&
                isFootnoteTypeLinkExist() === true
            ) {
                return null;
            }

            return link;
        };

        this.getRemainingLink = function getRemainingLinkFn(linkIds, width) {
            var link = null;

            link = getNextLinkOfID(linkIds, false);
            if (
                link === null ||
                Math.round(width) !== Math.round(link.width)
            ) {
                return null;
            }
            if (
                link.footnoteType === false &&
                isFootnoteTypeLinkExist() === true
            ) {
                return null;
            }
            link.placable = true;

            return link;
        };

        init();
    };

    return LinkHandler;
});
