/*global define*/
define([
    'rainbow/PresetReader', 'rainbow-config/NonHyphenBreakConfig'
], function defineFn(PresetReader, NonHyphenBreakConfig) {
    var NonHyphenBreakHandler = function NonHyphenBreakHandlerClass(layoutName) {
        var config = null;

        function init() {
            var presetReader = new PresetReader();

            config = presetReader.getPreset(layoutName, NonHyphenBreakConfig);
        }

        function insertBreakerText(text, breaker, insertText) {
            var modifiedText = '', preIndex = 0, index = 0;

            index = text.indexOf(breaker, preIndex);
            while (index >= 0) {
                modifiedText += text.slice(preIndex, index);
                preIndex = index + breaker.length;
                index = text.indexOf(breaker, preIndex);
                if (index === preIndex) {
                    modifiedText += breaker;
                }
                else {
                    modifiedText += insertText;
                }
                if (index === -1 && text.length > preIndex) {
                    modifiedText += text.slice(preIndex);
                }
            }
            if (modifiedText === '') {
                return text;
            }

            return modifiedText;
        }

        function breakText(text, breakCharacters, before) {
            var i = 0, breakCharacter = null, insertText = null;

            for (; i < breakCharacters.length; i += 1) {
                breakCharacter = breakCharacters[i];
                if (before === true) {
                    insertText = '\u200B' + breakCharacter;
                }
                else {
                    insertText = breakCharacter + '\u200B';
                }
                text = insertBreakerText(
                    text, breakCharacter, insertText
                );
            }

            return text;
        }

        function identifyTextNode(node, breakBefore, breakAfter) {
            var i = 0, length = 0, text = null;

            length = node.childNodes.length;
            for (; i < length; i += 1) {
                identifyTextNode(
                    node.childNodes[i], breakBefore, breakAfter
                );
            }
            if (node.nodeType === 3) {
                text = node.nodeValue;
                text = breakText(text, breakBefore, true);
                text = breakText(text, breakAfter, false);
                node.nodeValue = text;
            }
        }

        this.doBreak = function doBreakFn(container) {
            var elements = null, i = 0, j = 0, breaker = null;

            if (config === null || typeof config === 'undefined') {
                return;
            }
            for (;
                typeof config.breakers !== 'undefined' &&
                i < config.breakers.length;
                i += 1
            ) {
                breaker = config.breakers[i];
                if (breaker.selectors.length === 0) {
                    continue;
                }
                elements = container.querySelectorAll(
                    breaker.selectors.join(',')
                );
                for (j = 0; j < elements.length; j += 1) {
                    identifyTextNode(
                        elements[j], breaker.before, breaker.after
                    );
                }
            }
        };

        init();
    };

    return NonHyphenBreakHandler;
});
