/*global T2W*/
define([
    'rainbow-library/numbersToWords'
], function defineFn() {
    var numberToWord = null;

    numberToWord = new T2W('EN_US');

    return numberToWord;
});
