/*global define, console*/
define(['phoenix/Logger', 'css!rainbow-common/proofOverlay'], function OverlayLoader(Logger) {
    var overlayTemplate = [
        '<div class="proof-overlay">',
            '<div class="header"></div>',
            '<div class="message"></div>',
            '<div class="loader"> </div>',
            '<span class ="proof-pages"> &nbsp;</span>',
            '<div class="progress-bar">',
                '<span class="first-level"></span>',
                '<span class="second-level"></span>',
                '<span class="third-level"></span>',
                '<span class="fourth-level"></span>',
                '<span class="end-level"></span>',
            '</div>',
            '<div class="footer"></div>',
        '</div>'
    ];

    function initializeVariables(instance, src) {
        instance.doc = null;
        instance.eventBus = null;
        instance.overlay = null;
        instance.destroyed = false;
        Logger.info('Set instance variables for overlay from ' + src, true);
    }

    function proceedProgressUpdation(self, elementInfo) {
        var element = null;

        element = self.doc.querySelector(elementInfo);
        element.classList.add('progress-bar-color');
    }

    function updateProgressBar(progressInfo) {
        var elementInfo = null, key = null,
            progressBarLevels = {
                'first': '.first-level',
                'second': '.second-level',
                'third': '.third-level',
                'fourth': '.fourth-level',
                'end': '.end-level'
            };

        for (key in progressBarLevels) {
            if (progressBarLevels.hasOwnProperty(key) === true &&
                progressInfo.indexOf(key) > -1
            ) {
                elementInfo = progressBarLevels[key];
            }
        }
        proceedProgressUpdation(this, elementInfo);
    }

    function updateProofPagesCount(pageInfo) {
        var pageElement = null;

        pageElement = this.doc.querySelector('.proof-pages');
        pageElement.innerHTML = pageInfo;
    }

    function subscribeOverlayEvents(self) {
        self.eventBus.subscribe(
            'overlay:pageCountUpdater', updateProofPagesCount, self
        );
        self.eventBus.subscribe(
            'overlay:progressInvoker', updateProgressBar, self
        );
    }

    function unsubscribeOverlayEvents(self) {
        self.eventBus.unsubscribe(
            'overlay:progressInvoker', updateProgressBar, self
        );
        self.eventBus.unsubscribe(
            'overlay:pageCountUpdater', updateProofPagesCount, self
        );
    }

    function Overlay(doc, eBus) {
        initializeVariables(this, 'constructor');
        this.eventBus = eBus;
        this.doc = doc;
        this.overlay = this.doc.createElement('div');
        subscribeOverlayEvents(this);
    }

    Overlay.prototype.render = function render() {
        this.overlay.classList.add('page-overlay');
        this.overlay.innerHTML = overlayTemplate.join('');
        this.doc.body.appendChild(this.overlay);
        this.doc.body.classList.add('noscroll');
    };

    function errorCallBack(error, message) {
        var newError = new Error();

        ProofError.prototype = newError;

        function ProofError() {
            var errPrefix = 'Proof.Error.';

            this.name = 'Proof-Error';
            this.message = errPrefix + 'proof_render_issue';
            if (error === null || typeof error === 'undefined') {
                return;
            }
            if (message !== null && typeof message !== 'undefined') {
                this.message = errPrefix + message + '; ' + error.message;
            }
            else {
                this.message = errPrefix + error.message;
            }
            this.stack = error.stack;
            this.parentClass = error.parentClass;
        }

        throw new ProofError();
    }

    Overlay.prototype.destroy = function destroy() {
        var errorData = {};

        try {
            if (this.destroyed === true) {
                Logger.info('Overlay destroyed already!', true);
                return;
            }
            this.doc.body.removeChild(this.overlay);
            this.doc.body.classList.remove('noscroll');
            unsubscribeOverlayEvents(this);
            initializeVariables(this, 'destroy');
            this.destroyed = true;
            Logger.info('Overlay destroyed successfully', true);
        }
        catch (e) {
            errorData = e;
            errorData.parentClass = '';

            if (this === null || typeof (this) === 'undefined') {
                errorData.parentClass = 'null';
            }
            else {
                errorData.parentClass = this.constructor.name;
            }
            errorCallBack(errorData, e.message);
        }
    };

    return Overlay;
});
