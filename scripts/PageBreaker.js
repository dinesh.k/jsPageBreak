/*global define, performance*/
define([
    'rainbow/LinkHandler', 'rainbow/LineTypeHandler', 'rainbow-obfuscated/ContentHandler',
    'rainbow-obfuscated/LineBreaker', 'rainbow-obfuscated/ColumnBreaker',
    'rainbow-obfuscated/FlowTemplate', 'rainbow/ColumnBalancer', 'rainbow/ColumnStretching',
    'rainbow/RuleEngine', 'rainbow/RuleManager', 'rainbow-config/PreRuleConfig',
    'rainbow-config/PostRuleConfig', 'rainbow/config/common/CommonPreRuleConfig',
    'rainbow/config/common/CommonPostRuleConfig',
    'rainbow/config/common/PDFRuleConfig', 'phoenix/Logger'
], function defineFn(
    LinkHandler, LineTypeHandler, ContentHandler, LineBreaker, ColumnBreaker,
    FlowTemplate, ColumnBalancer, ColumnStretching, RuleEngine, RuleManager,
    PreRules, PostRules, CommonPreRules, CommonPostRules, PDFRules, Logger
) {
    var PageBreaker = function PageBreakerClass(
        win, doc, eventBus, templateHandler
    ) {
        var contentHandler = null, linkHandler = null, ruleManager = null,
            lineTypeHandler = null, columnInfo = null, param = {};

        function errorHandlingCallBack(error, message) {
            templateHandler.destroy();
            unsubscribePageBreakerEvents();
            param.placeHolders.paginationRoot.innerHTML = '';
            param.errorCallBack(error, message);
        }

        function init() {
            param.placeHolders.source.style.display = '';
            linkHandler = new LinkHandler(
                doc, param.data.layoutName, templateHandler
            );
            lineTypeHandler = new LineTypeHandler(param.data.layoutName);
            contentHandler = new ContentHandler(
                doc, lineTypeHandler, templateHandler
            );
            ruleManager = new RuleManager(param.data.layoutName);
            param.data.dimensions = linkHandler.getAllLinkDimensions();
            setTimeout(function timeoutFn() {
                eventBus.publish('paginate:pageBreaker:commonPreRule');
            }, 0);
        }

        function commonPreRule() {
            var beforePreRule = null, eachProcess = null;

            eachProcess = performance.now();
            beforePreRule = new RuleEngine(
                win, doc, 'commonPreRule', eventBus, CommonPreRules
            );
            beforePreRule.start(
                param.placeHolders.root, param.data, function callBack() {
                    Logger.debug('Common Pre-Pagination Rules Applyed At ' +
                        (performance.now() - eachProcess) + ' milliseconds.'
                    );
                    setTimeout(function timeoutFn() {
                        eventBus.publish('paginate:pageBreaker:preRule');
                    }, 0);
                }, errorHandlingCallBack
            );
        }

        function preRule() {
            var beforePaging = null, eachProcess = null,
                requiredPreRules = [];

            eachProcess = performance.now();
            requiredPreRules = ruleManager.getRules(PreRules);
            beforePaging = new RuleEngine(
                win, doc, 'preRule', eventBus, requiredPreRules
            );
            beforePaging.start(
                param.placeHolders.root, param.data, function callBack() {
                    Logger.debug('Pre-Pagination Rules Applyed At ' +
                        (performance.now() - eachProcess) + ' milliseconds.'
                    );
                    eventBus.publish(
                        'overlay:progressInvoker', 'first'
                    );
                    setTimeout(function timeoutFn() {
                        eventBus.publish('paginate:pageBreaker:breakWord');
                    }, 0);
                }, errorHandlingCallBack
            );
        }

        function breakWord() {
            var source = null, eachProcess = null;

            eachProcess = performance.now();
            source = param.placeHolders.source;
            contentHandler.initContainers(source);
            linkHandler.initLinks(source);
            Logger.debug('Wrapping word spaces and text end at ' +
                (performance.now() - eachProcess) + ' milliseconds'
            );
            setTimeout(function timeoutFn() {
                eventBus.publish('paginate:pageBreaker:breakLine');
            }, 0);
        }

        function breakLine() {
            var source = null, lineBreaker = null, eachProcess = null;

            eachProcess = performance.now();
            source = param.placeHolders.source;
            lineTypeHandler.initLineTypes(source);
            lineBreaker = new LineBreaker(
                win, doc, eventBus, linkHandler,
                lineTypeHandler, contentHandler
            );
            lineBreaker.start(
                function callBack() {
                    Logger.debug('Line Information Extraction At ' +
                        (performance.now() - eachProcess) + ' milliseconds.'
                    );
                    eventBus.publish(
                        'overlay:progressInvoker', 'second'
                    );
                    setTimeout(function timeoutFn() {
                        eventBus.publish('paginate:pageBreaker:breakColumn');
                    }, 0);
                }, errorHandlingCallBack
            );
        }

        function breakColumn() {
            var columnBalancer = null, columnBreaker = null, eachProcess = null;

            eachProcess = performance.now();
            columnBalancer = new ColumnBalancer(templateHandler);
            columnBreaker = new ColumnBreaker(
                eventBus, linkHandler, contentHandler, templateHandler,
                columnBalancer
            );
            columnBreaker.start(
                function callBack() {
                    Logger.debug('Column Breaking and Column Balancing ' +
                        'Process At ' + (performance.now() - eachProcess) +
                        ' milliseconds.'
                    );
                    columnInfo = columnBreaker.getPaginationInfo();
                    setTimeout(function timeoutFn() {
                        eventBus.publish('paginate:pageBreaker:flowPage');
                    }, 0);
                }, errorHandlingCallBack
            );
        }

        function flowPage() {
            var destination = null, flowTemplate = null, eachProcess = null;

            eachProcess = performance.now();
            destination = param.placeHolders.destination;
            flowTemplate = new FlowTemplate(eventBus, templateHandler);
            flowTemplate.start(columnInfo, destination, function callBack() {
                Logger.debug('Flowing of Pages At ' +
                    (performance.now() - eachProcess) + ' milliseconds.'
                );
                eventBus.publish(
                    'overlay:pageCountUpdater', '&nbsp;'
                );
                eventBus.publish(
                    'overlay:progressInvoker', 'third'
                );
                setTimeout(function timeoutFn() {
                    eventBus.publish('paginate:pageBreaker:commonPostRule');
                }, 0);
            });
        }

        function commonPostRule() {
            var afterPaging = null, eachProcess = null;

            eachProcess = performance.now();
            param.data.startPageNumber = 1;
            param.data.endPageNumber = columnInfo.length;
            if (columnInfo.length === 0) {
                param.data.endPageNumber = 1;
            }
            afterPaging = new RuleEngine(
                win, doc, 'commonPostRule', eventBus, CommonPostRules
            );
            afterPaging.start(
                param.placeHolders.root, param.data, function callBack() {
                    Logger.debug('Common Post-Pagination Rules Applyed At ' +
                        (performance.now() - eachProcess) + ' milliseconds.'
                    );
                    setTimeout(function timeoutFn() {
                        eventBus.publish('paginate:pageBreaker:postRule');
                    }, 0);
                }, errorHandlingCallBack
            );
        }

        function postRule() {
            var afterCommonPostrule = null, eachProcess = null,
                requiredPostRules = [];

            eachProcess = performance.now();
            requiredPostRules = ruleManager.getRules(PostRules);
            afterCommonPostrule = new RuleEngine(
                win, doc, 'postRule', eventBus, requiredPostRules
            );
            afterCommonPostrule.start(
                param.placeHolders.root, param.data, function callBack() {
                    Logger.debug('Post-Pagination Rules Applyed At ' +
                        (performance.now() - eachProcess) + ' milliseconds.'
                    );
                    eventBus.publish(
                        'overlay:progressInvoker', 'fourth'
                    );
                    setTimeout(function timeoutFn() {
                        eventBus.publish('paginate:pageBreaker:stretchColumn');
                    }, 0);
                }, errorHandlingCallBack
            );
        }

        function stretchColumn() {
            var columnStretching = null, eachProcess = null;

            eachProcess = performance.now();
            columnStretching = new ColumnStretching(
                doc, param.data.layoutName, eventBus, templateHandler
            );
            columnStretching.stretchContent(columnInfo, function callBack() {
                Logger.debug('Column Stretching Process At ' +
                    (performance.now() - eachProcess) + ' milliseconds.'
                );
                setTimeout(function timeoutFn() {
                    eventBus.publish('paginate:pageBreaker:pdfRule');
                }, 0);
            });
        }

        function pdfRule() {
            var pdfRuleEng = null, eachProcess = null;

            eachProcess = performance.now();
            pdfRuleEng = new RuleEngine(
                win, doc, 'pdfRule', eventBus, PDFRules
            );
            pdfRuleEng.start(
                param.placeHolders.root, param.data, function callBack() {
                    Logger.debug('PDF Rules Applyed At ' +
                        (performance.now() - eachProcess) + ' milliseconds.'
                    );
                    eventBus.publish(
                        'overlay:progressInvoker', 'end'
                    );
                    setTimeout(function timeoutFn() {
                        eventBus.publish('paginate:pageBreaker:deinit');
                    }, 0);
                }, errorHandlingCallBack
            );
        }

        function deinit() {
            templateHandler.destroy();
            unsubscribePageBreakerEvents();
            param.placeHolders.paginationRoot.style.visibility = '';
            param.placeHolders.source.style.display = 'none';
            param.success();
        }

        function unsubscribePageBreakerEvents() {
            eventBus.unsubscribe('paginate:pageBreaker:init', init);
            eventBus.unsubscribe('paginate:pageBreaker:commonPreRule', commonPreRule);
            eventBus.unsubscribe('paginate:pageBreaker:preRule', preRule);
            eventBus.unsubscribe('paginate:pageBreaker:breakWord', breakWord);
            eventBus.unsubscribe('paginate:pageBreaker:breakLine', breakLine);
            eventBus.unsubscribe('paginate:pageBreaker:breakColumn', breakColumn);
            eventBus.unsubscribe('paginate:pageBreaker:flowPage', flowPage);
            eventBus.unsubscribe('paginate:pageBreaker:commonPostRule', commonPostRule);
            eventBus.unsubscribe('paginate:pageBreaker:postRule', postRule);
            eventBus.unsubscribe('paginate:pageBreaker:stretchColumn', stretchColumn);
            eventBus.unsubscribe('paginate:pageBreaker:pdfRule', pdfRule);
            eventBus.unsubscribe('paginate:pageBreaker:deinit', deinit);
        }

        function subscribePageBreakerEvents() {
            eventBus.subscribe('paginate:pageBreaker:deinit', deinit);
            eventBus.subscribe('paginate:pageBreaker:pdfRule', pdfRule);
            eventBus.subscribe('paginate:pageBreaker:stretchColumn', stretchColumn);
            eventBus.subscribe('paginate:pageBreaker:postRule', postRule);
            eventBus.subscribe('paginate:pageBreaker:commonPostRule', commonPostRule);
            eventBus.subscribe('paginate:pageBreaker:flowPage', flowPage);
            eventBus.subscribe('paginate:pageBreaker:breakColumn', breakColumn);
            eventBus.subscribe('paginate:pageBreaker:breakLine', breakLine);
            eventBus.subscribe('paginate:pageBreaker:breakWord', breakWord);
            eventBus.subscribe('paginate:pageBreaker:preRule', preRule);
            eventBus.subscribe('paginate:pageBreaker:commonPreRule', commonPreRule);
            eventBus.subscribe('paginate:pageBreaker:init', init);
        }

        this.start = function startFn(
            placeHolders, data, successCallBack, errorCallBack
        ) {
            param.placeHolders = placeHolders;
            param.data = data;
            param.success = successCallBack;
            param.errorCallBack = errorCallBack;
            subscribePageBreakerEvents();
            setTimeout(function timeoutFn() {
                eventBus.publish('paginate:pageBreaker:init');
            }, 0);
        };
    };

    return PageBreaker;
});
