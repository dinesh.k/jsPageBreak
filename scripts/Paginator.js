/*global define, performance*/
define([
    'rainbow/LayoutHandler', 'rainbow/TemplateHandler', 'rainbow/HyphenationHandler',
    'rainbow/NonHyphenBreakHandler', 'rainbow/TOCHandler', 'rainbow/PdfDownloader',
    'rainbow/Overlay', 'rainbow/ReferenceStylerToAll', 'rainbow/PageBreaker', 'phoenix/Logger',
    'phoenix/ConfigReader', 'css!rainbow-common/proof-common'
], function defineFn(
    Layout, TemplateHandler, Hyphenator, NonHyphenBreaker, TOCHandler,
    PdfDownloader, Overlay, ReferenceStyler, PageBreaker, Logger, Config
) {
    var Paginator = function PaginatorClass(win, doc, templateName) {
        var layoutHandler = null, placeHolders = null, data = {}, param = {},
            startTime = null, tocHandler = null, overlay = null;

        function updateRequirePath(layoutName) {
            var themePath = require.toUrl('rainbow-theme'), version = null;

            if (Config.get('Environment') === 'prod') {
                version = Config.get('sourceFileVersion');
                themePath += '/' + version;
            }
            themePath += '/' + layoutName;
            require.config({
                'paths': {'rainbow-theme': themePath}
            });
        }

        function init() {
            var debugMode = false, logger = null;

            if (Config.get('Environment') !== 'prod') {
                logger = new Logger(win, doc);
                logger.configure('debug', true, Config.getRoute('paginationLogEndPoint'));
                debugMode = true;
            }
            layoutHandler = new Layout(doc, templateName);
            data = layoutHandler.getLayoutData();
            data.debugMode = debugMode;
            data.layoutName = layoutHandler.getLayoutName();
            updateRequirePath(data.layoutName);
            require([
                'css!rainbow-theme/layout',
                'css!rainbow-theme/specification',
                'css!rainbow-theme/common-spec'
            ], function onCssLoaded() {
                Logger.info('Pagination Module Loaded.');
            });
        }

        function successCallBack(workDone) {
            setTimeout(function timeoutFn() {
                param.eventBus.publish(
                    'paginate:onComplete', data.pageCount, workDone
                );
            }, 0);
            overlay.destroy();
        }

        function errorCallBack(error, message) {
            var newError = new Error();

            ProofError.prototype = newError;

            function ProofError() {
                var errPrefix = 'Proof.Error.';

                this.name = 'Proof-Error';
                this.message = errPrefix + 'proof_render_issue';
                if (error === null || typeof error === 'undefined') {
                    return;
                }
                if (message !== null && typeof message !== 'undefined') {
                    this.message = errPrefix + message + '; ' + error.message;
                }
                else {
                    this.message = errPrefix + error.message;
                }
                this.stack = error.stack;
            }

            unsubscribePaginationEvents();
            throw new ProofError();
        }

        function removeAllNodes(target, selectors) {
            var i = 0, length = 0, nodes = null, node = null, parent = null;

            nodes = target.querySelectorAll(selectors.join(','));
            length = nodes.length;
            for (; i < length; i += 1) {
                node = nodes[i];
                parent = node.parentNode;
                if (parent !== null) {
                    parent.removeChild(node);
                }
            }
        }

        function inputHtmlCleanup() {
            var htmlNode = doc.createElement('div'), eachProcess = null;

            eachProcess = performance.now();
            htmlNode.innerHTML = param.htmlContent;
            removeAllNodes(htmlNode, Config.get('inputHtmlCleanupSelectors'));
            placeHolders.source.appendChild(htmlNode.firstElementChild);
            Logger.debug('Input Html Cleanup Done At ' +
                (performance.now() - eachProcess) + ' milliseconds.'
            );
            setTimeout(function timeoutFn() {
                param.eventBus.publish('paginate:hyphenate');
            }, 0);
        }

        function hyphenate() {
            var hyphenator = null, eachProcess = null;

            eachProcess = performance.now();
            hyphenator = new Hyphenator(win, data.layoutName);
            hyphenator.hyphenate(placeHolders.source);
            Logger.debug('Hyphenation Applyed At ' +
                (performance.now() - eachProcess) + ' milliseconds.'
            );
            setTimeout(function timeoutFn() {
                param.eventBus.publish('paginate:nonHyphenBreak');
            }, 0);
        }

        function nonHyphenBreak() {
            var nonHyphenBreaker = null, eachProcess = null;

            eachProcess = performance.now();
            nonHyphenBreaker = new NonHyphenBreaker(data.layoutName);
            nonHyphenBreaker.doBreak(placeHolders.source);
            Logger.debug('Non Hyphenation Breaking Applyed At ' +
                (performance.now() - eachProcess) + ' milliseconds.'
            );
            setTimeout(function timeoutFn() {
                param.eventBus.publish('paginate:tocGeneration');
            }, 0);
        }

        function tocGeneration() {
            var eachProcess = null;

            eachProcess = performance.now();
            tocHandler = new TOCHandler(doc, data.layoutName);
            tocHandler.generate(placeHolders.source);
            Logger.debug('Table Of Content Generated At ' +
                (performance.now() - eachProcess) + ' milliseconds.'
            );
            setTimeout(function timeoutFn() {
                param.eventBus.publish('paginate:referenceStyling');
            }, 0);
        }

        function referenceStyling() {
            if (layoutHandler.isReferenceStylingEnable() === true) {
                var refStyler = null, eachProcess = null;

                eachProcess = performance.now();
                refStyler = new ReferenceStyler(data.debugMode);
                refStyler.doStylingToAll(placeHolders.source);
                Logger.debug('Reference Styling Process Completed At ' +
                    (performance.now() - eachProcess) + ' milliseconds.'
                );
            }
            setTimeout(function timeoutFn() {
                param.eventBus.publish('paginate:paginateContent');
            }, 0);
        }

        function paginateContent() {
            var pager = null, templateHandler = null;

            templateHandler = new TemplateHandler(
                doc, placeHolders.layout, placeHolders.template, layoutHandler
            );
            pager = new PageBreaker(
                win, doc, param.eventBus, templateHandler
            );
            pager.start(placeHolders, data, function callBack() {
                setTimeout(function timeoutFn() {
                    param.eventBus.publish('paginate:locateTocTerms');
                }, 0);
            }, errorCallBack);
        }

        function locateTocTerms() {
            var eachProcess = null;

            eachProcess = performance.now();
            tocHandler.locateTerms(placeHolders.destination);
            Logger.debug('Table Of Content Locating Page Numbers At ' +
                (performance.now() - eachProcess) + ' milliseconds.'
            );
            successCallBack(true);
            setTimeout(function timeoutFn() {
                param.eventBus.publish('paginate:processComplete');
            }, 0);
        }

        function processComplete() {
            Logger.debug('Overall Pagination Execution Time: ' +
                (performance.now() - startTime) + ' milliseconds.'
            );
            unsubscribePaginationEvents();
        }

        function unsubscribePaginationEvents() {
            param.eventBus.unsubscribe('paginate:inputHtmlCleanup', inputHtmlCleanup);
            param.eventBus.unsubscribe('paginate:hyphenate', hyphenate);
            param.eventBus.unsubscribe('paginate:nonHyphenBreak', nonHyphenBreak);
            param.eventBus.unsubscribe('paginate:tocGeneration', tocGeneration);
            param.eventBus.unsubscribe('paginate:referenceStyling', referenceStyling);
            param.eventBus.unsubscribe('paginate:paginateContent', paginateContent);
            param.eventBus.unsubscribe('paginate:locateTocTerms', locateTocTerms);
            param.eventBus.unsubscribe('paginate:processComplete', processComplete);
        }

        function subscribePaginationEvents() {
            param.eventBus.subscribe('paginate:processComplete', processComplete);
            param.eventBus.subscribe('paginate:locateTocTerms', locateTocTerms);
            param.eventBus.subscribe('paginate:paginateContent', paginateContent);
            param.eventBus.subscribe('paginate:referenceStyling', referenceStyling);
            param.eventBus.subscribe('paginate:tocGeneration', tocGeneration);
            param.eventBus.subscribe('paginate:nonHyphenBreak', nonHyphenBreak);
            param.eventBus.subscribe('paginate:hyphenate', hyphenate);
            param.eventBus.subscribe('paginate:inputHtmlCleanup', inputHtmlCleanup);
        }

        function isRevisedContent(revisionId) {
            if (data === null || typeof data === 'undefined') {
                return true;
            }
            if (data.existingRevisionId === revisionId) {
                return false;
            }

            return true;
        }

        function getDocumentTitle(title, abbrTitle) {
            var result = title;

            if (abbrTitle !== '' && abbrTitle !== null &&
                typeof abbrTitle !== 'undefined'
            ) {
                result = abbrTitle;
            }

            return result;
        }

        function paginate() {
            startTime = performance.now();
            subscribePaginationEvents();
            setTimeout(function timeoutFn() {
                param.eventBus.publish('paginate:inputHtmlCleanup');
            }, 0);
        }

        this.execute = function executeFn(
            htmlContent, container, title, customerLogoUrl, journalLogoUrl,
            crossMarkLogoUrl, eventBus, journalId, articleId, actor, revisionId,
            abbrTitle
        ) {
            param.eventBus = eventBus;
            param.htmlContent = htmlContent;
            overlay = new Overlay(doc, eventBus);
            overlay.render();
            data.existingRevisionId = 0;
            if (container.hasAttribute('data-revision-id') === true) {
                data.existingRevisionId = Number(container.dataset.revisionId);
            }
            if (isRevisedContent(revisionId) === false) {
                successCallBack(false);

                return;
            }
            container.setAttribute('data-revision-id', revisionId);
            layoutHandler.initializeContainer(container);
            placeHolders = layoutHandler.getPlaceholders();
            data.jid = journalId;
            data.aid = articleId;
            data.actor = actor;
            data.pageCount = 0;
            data.revisionId = revisionId;
            data.customerLogo = customerLogoUrl;
            data.journalLogo = journalLogoUrl;
            data.crossMarkLogo = crossMarkLogoUrl;
            data.documentTitle = title;
            data.abbreviatedTitle = getDocumentTitle(title, abbrTitle);
            require([
                'css!rainbow-theme/layout',
                'css!rainbow-theme/specification',
                'css!rainbow-theme/common-spec'
            ], function onCssLoaded() {
                paginate();
            });
        };

        init();
    };

    return Paginator;
});
