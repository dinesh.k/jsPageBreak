/*global define, FormData, alert*/
define([
    'phoenix/Helper', 'phoenix/RequestBuilder', 'phoenix/ConfigReader',
    'phoenix/Logger', 'rainbow-config/DownloadPdfConfig', 'rainbow/RuleEngine',
    'rainbow/config/common/PDFRuleConfig', 'rainbow-config/LayoutConfig',
    'rainbow/util/DomHelpers', 'rainbow/BrowserDetector', 'rainbow/PresetReader'
],
function pdfDownloadLoader(
    Helper, RequestBuilder, Config, Logger, DownloadPdfConfig, RuleEngine,
    PDFRules, AllLayoutConfig, DomHelper, BrowserDetector, PresetReader
) {
    var pdfDownloader = null, proto = null, pdfGenerationCallBack;

    function enablePdfLoader(self) {
        var pdfDownloadBtn = null, pdfProgressBtn = null;

        pdfDownloadBtn = self.htmlDoc.querySelector('.downloadBtn');
        pdfDownloadBtn.classList.add('hide');
        pdfDownloadBtn.classList.remove('show');
        pdfProgressBtn = self.htmlDoc.querySelector('.pdfProgressBtn');
        pdfProgressBtn.classList.remove('hide');
        pdfProgressBtn.classList.add('show');
    }

    function disablePdfLoader() {
        var pdfDownloadBtn = null, pdfProgressBtn = null;

        pdfDownloadBtn = this.htmlDoc.querySelector('.downloadBtn');
        pdfDownloadBtn.classList.remove('hide');
        pdfDownloadBtn.classList.add('show');
        pdfProgressBtn = this.htmlDoc.querySelector('.pdfProgressBtn');
        pdfProgressBtn.classList.remove('show');
        pdfProgressBtn.classList.add('hide');
    }

    function downloadPdf() {
        var target = this.domHelper.find('iframe.hideme', this.htmlDoc),
            downloadUrl = Config.getRoute('downloadPdfEndPoint');

        target.src = downloadUrl + '/' + this.articleToken;
        target.target = '_blank';
        Logger.debug('success');
        disablePdfLoader.apply(this, []);
    }

    function clearSetInterval(self) {
        disablePdfLoader.apply(self, []);
        clearInterval(pdfGenerationCallBack);
    }

    function alertErrorOnPdfGeneration() {
        alert('Sorry! Error on PDF Generation.');
    }

    function pdfGenerationTimeOut() {
        alert('Sorry! PDF generation timed out. Please try again');
    }

    function apiFailure(response) {
        var data = JSON.parse(response);

        Logger.debug(data.message);
        disablePdfLoader.apply(this, []);
        Logger.debug('failure');
        clearSetInterval(this);
    }

    function apiTimeout() {
        Logger.debug('Request timeout');
        disablePdfLoader.apply(this, []);
        clearSetInterval(this);
    }

    function checkPdfGenerationSuccess(self, url) {
        var generation, options, formData = null, request = null,
            requestBuilder = null;

        self.counter += 1;

        requestBuilder = new RequestBuilder();
        requestBuilder.setUrl(url);
        requestBuilder.setMethod('GET');
        requestBuilder.setData(formData);
        requestBuilder.setSuccessCallback(function successCallback(response) {
            generation = JSON.parse(response);
            if (generation.status === 'completed') {
                clearSetInterval(self);
                downloadPdf.apply(self, []);
            }
            else if (generation.status === 'error') {
                clearSetInterval(self);
                alertErrorOnPdfGeneration();
            }

            if (self.counter > 10) {
                clearSetInterval(self);
                pdfGenerationTimeOut();
            }

            Logger.debug('PDF CallBack Counter: ', self.counter);
        });

        requestBuilder.setFailureCallback(apiFailure.bind(self));
        requestBuilder.setTimeoutCallback(apiTimeout.bind(self));
        request = requestBuilder.build();
        request.send();
    }

    function downloadSuccess(response) {
        var data, url, callBackRate = Config.get('pdfCallBackRate');

        data = JSON.parse(response);
        data = JSON.parse(data.data);
        if (Helper.isUndefined(data.status) === false &&
            data.status === 'download'
        ) {
            downloadPdf.apply(this, []);

            return;
        }

        if (Helper.isObject(data) === false ||
            Helper.isUndefined(data.pingUrl) === true
        ) {
            disablePdfLoader.apply(this, []);

            return;
        }

        url = data.pingUrl;
        this.counter = 1;
        pdfGenerationCallBack = setInterval(
            checkPdfGenerationSuccess, callBackRate, this, url
        );
    }

    function downloadFailure(response) {
        var data = JSON.parse(response);

        Logger.debug(data.message);
        Logger.debug('failure');
        disablePdfLoader.apply(this, []);
    }

    function downloadTimeout() {
        Logger.debug('Request timeout');
    }

    function createTargetFrame(self) {
        var iframeInstance = null, body = null;

        body = self.domHelper.find('body', self.htmlDoc);
        iframeInstance = self.domHelper.find('iframe.hideme', body);

        if (iframeInstance === null) {
            iframeInstance = self.domHelper.create('iframe');
            iframeInstance.classList.add('hideme');
        }

        self.domHelper.append(iframeInstance, body);
    }

    function getRequiredStyles(self) {
        var links = null, length = 0, i = 0, link = null, result = [];

        result.push('<head>');
        links = self.domHelper.findAll(
            'head meta, head link, head style', self.htmlDoc
        );
        length = links.length;
        for (; i < length; i += 1) {
            link = links[i];
            result.push(link.outerHTML);
        }
        result.push('</head>');

        return result;
    }

    function getRequiredClassName(self) {
        return self.meta.layoutName;
    }

    function isNeedServerPagination() {
        var len = 0, i = 0, userAgent = null, userAgentName = '',
            serverSidePaginationEnabled = Config.get('isNeedServerSidePagination'),
            Browsers = Config.get('noNeedServerSidePaginationBrowsers');

        if (serverSidePaginationEnabled === false) {
            return false;
        }

        userAgent = new BrowserDetector.browserDetect(
            typeof navigator !== 'undefined' ? navigator.userAgent : ''
        );
        userAgentName = userAgent.name.toLowerCase();
        len = Browsers.length;
        for (; i < len; i += 1) {
            if (userAgentName === Browsers[i]) {
                return false;
            }
        }

        return true;
    }

    function getSaveAsPdfPoint() {
        if (isNeedServerPagination() === true) {
            return Config.getRoute('serverSidePaginateEndPoint');
        }

        return Config.getRoute('saveAsPdfEndPoint');
    }

    function makePdfCreationRequest(self, contents) {
        var requestBuilder = null, request = null,
            formData = new FormData(),
            stylingData = getRequiredStyles(self),
            classname = getRequiredClassName(self),
            saveAsPdfEndPoint = getSaveAsPdfPoint(),
            data = {
                'content': contents,
                'token': self.articleToken,
                'head': stylingData.join(''),
                'class': classname,
                'proofState': self.proofState
            };

        formData.append('json', JSON.stringify(data));
        requestBuilder = new RequestBuilder();
        requestBuilder.setUrl(saveAsPdfEndPoint);
        requestBuilder.setMethod('POST');
        requestBuilder.setData(formData);
        requestBuilder.setSuccessCallback(downloadSuccess.bind(self));
        requestBuilder.setFailureCallback(downloadFailure.bind(self));
        requestBuilder.setTimeoutCallback(downloadTimeout.bind(self));
        request = requestBuilder.build();
        request.send();
    }

    function pdfRule(self, target) {
        var pdfRuleEng = null;

        pdfRuleEng = new RuleEngine(
            self.win, self.doc, 'pdfRule', self.eventBus, []
        );
        pdfRuleEng.start(
            target, {}, function startPdfEngine() {
                setTimeout(function timeOut() {
                    makePdfCreationRequest(self, target.innerHTML);
                }, 0);
            }
        );
    }

    function applyPdfRuleToTheContents(self, contents) {
        var container = null, dHelper = self.domHelper;

        container = dHelper.create('div');
        dHelper.addIntoClassList(container, 'pdf-rule-container');
        container.innerHTML = contents;
        dHelper.append(container, self.target);
        pdfRule(self, dHelper.clone(container, true));
        dHelper.remove(container, self.target);
    }

    function checkAndInvokePdfRulerIfNeeded(self) {
        return self.meta.config.enablePdfRuler;
    }

    function invokeConversion(self, source, template) {
        var contents = source, needToInvokePdfRuler = false,
        layoutConf = null, presetReader = new PresetReader(), data = {};

        createTargetFrame(self);
        layoutConf = presetReader.getLayoutPreset(template, AllLayoutConfig);
        data.layoutName = layoutConf.name;
        data.config = presetReader.getJIDPreset(
            self.jid, data.layoutName, DownloadPdfConfig
        );
        self.meta = data;
        needToInvokePdfRuler = checkAndInvokePdfRulerIfNeeded(self);

        if (needToInvokePdfRuler === true) {
            applyPdfRuleToTheContents(self, contents);
        }
        else {
            makePdfCreationRequest(self, contents);
        }
    }

    pdfDownloader = function pdfDownloaderLoad() {
        this.domHelper = new DomHelper();
    };

    proto = pdfDownloader.prototype;

    proto.download = function download(
        win, doc, source, target, token, eventBus, jid,
        proofState, template
    ) {
        if (win instanceof win.Window === false) {
            throw new Error(
                'PDF Downloader requires window object');
        }

        if (doc instanceof win.HTMLDocument === false) {
            throw new Error(
                'PDF Downloader requires html document object');
        }

        if (Helper.isEmptyString(source) === true) {
            throw new Error('Need to some data to download pdf');
        }

        if (target instanceof win.HTMLElement === false) {
            throw new Error(
                'PDF Downloader requires html element object');
        }

        if (Helper.isEmptyString(token) === true) {
            throw new Error('Token cannot be empty');
        }

        if (Helper.isEmptyString(jid) === true) {
            throw new Error('Jid value cannot be empty');
        }

        this.win = win;
        this.htmlDoc = doc;
        this.target = target;
        this.articleToken = token;
        this.eventBus = eventBus;
        this.jid = jid;
        this.proofState = proofState === true ? 'readOnly' : 'active';
        enablePdfLoader(this);
        invokeConversion(this, source, template);
    };

    return pdfDownloader;
});
