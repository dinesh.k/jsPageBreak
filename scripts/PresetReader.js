/*global define*/
define([], function defineFn() {
    var PresetReader = function PresetReaderClass() {
        function getDefaultConfig(configs) {
            var i = 0, result = null, config = null;

            for (; i < configs.length; i += 1) {
                config = configs[i];
                if (config.default === true) {
                    result = config;
                    break;
                }
            }

            return result;
        }

        function getKeyTermConfig(keyterm, configs) {
            var i = 0, result = null, config = null;

            for (; i < configs.length; i += 1) {
                config = configs[i];
                if (config.keyterms.indexOf(keyterm) >= 0) {
                    result = config;
                    break;
                }
            }
            if (result === null) {
                result = getDefaultConfig(configs);
            }
            if (result === null) {
                throw new Error(
                    'Template name "' + keyterm + '" not configured properly.');
            }

            return result;
        }

        function getLayoutConfig(layoutName, configs) {
            var i = 0, result = null, config = null;

            for (; i < configs.length; i += 1) {
                config = configs[i];
                if (config.layouts.indexOf(layoutName) >= 0) {
                    result = config;
                    break;
                }
            }
            if (result === null) {
                throw new Error(
                    'Layout name "' + layoutName + '" not configured properly.');
            }

            return result;
        }

        function getJournalIdLayoutConfig(journalId, layoutName, configs) {
            var i = 0, results = [], result = null, config = null;

            for (; i < configs.length; i += 1) {
                config = configs[i];
                if (config.layouts.indexOf(layoutName) >= 0) {
                    results.push(config);
                }
            }
            for (i = 0; i < results.length; i += 1) {
                if (results[i].journals.length === 0) {
                    result = results[i];
                }
                if (results[i].journals.indexOf(journalId) > -1) {
                    return results[i];
                }
            }
            if (result === null) {
                throw new Error(
                    'Layout name "' + layoutName + '" not configured properly.');
            }

            return result;
        }

        this.getLayoutPreset = function getLayoutPresetFn(templateName, configs) {
            if (templateName === null || typeof templateName === 'undefined') {
                templateName = '';
            }
            templateName = templateName.toLowerCase();
            return getKeyTermConfig(templateName, configs);
        };

        this.getPreset = function getPresetFn(layoutName, configs) {
            return getLayoutConfig(layoutName, configs);
        };

        this.getJIDPreset = function getJIDPresetFn(
            journalId, layoutName, configs
        ) {
            return getJournalIdLayoutConfig(journalId, layoutName, configs);
        };
    };

    return PresetReader;
});
