/*global define, document, NodeFilter*/
define([
    'phoenix/Helper', 'rainbow-config/ReferenceStyleConfig'
],
function defineFn(Helper, Config) {
    var proto = null, trackAction = false;

    function doTrackAction(domHelper, action, target) {
        var previousActions = '', option = Config.stylingOption;

        if (trackAction === false) {
            return;
        }
        previousActions = target.getAttribute(option.actionTrackingAttribute);
        if (previousActions === null) {
            domHelper.addOrModifyAttribute(
                option.actionTrackingAttribute,
                action.id,
                target
            );
        }
        else {
            domHelper.addOrModifyAttribute(
                option.actionTrackingAttribute,
                previousActions + ' ' + action.id,
                target
            );
        }
    }

    function isRuleNameMatches(rule, name) {
        return rule.name === name;
    }

    function doRemoveNodeAction(domHelper, action, onItems) {
        var count = 0, length = 0, item = null, trackElement = null;

        if (isRuleNameMatches(action, 'removeNode') === false) {
            return;
        }

        length = onItems.length;
        for (; count < length; count += 1) {
            item = onItems[count];
            trackElement = domHelper.create('span');
            doTrackAction(domHelper, action, trackElement);
            domHelper.addBefore(trackElement, item, domHelper.getParent(item));
            domHelper.remove(item, domHelper.getParent(item));
        }
    }

    function doTextReplaceAction(domHelper, action, onItems) {
        var count = 0, length = 0, item = null;

        if (isRuleNameMatches(action, 'textReplace') === false) {
            return;
        }

        length = onItems.length;
        for (; count < length; count += 1) {
            item = onItems[count];
            item.innerHTML = action.text;
            doTrackAction(domHelper, action, item);
        }
    }

    function doRegexReplaceAction(domHelper, action, onItems) {
        var count = 0, length = 0, textContent = '', item = null;

        if (isRuleNameMatches(action, 'regexReplace') === false) {
            return;
        }

        length = onItems.length;
        for (; count < length; count += 1) {
            item = onItems[count];
            textContent = item.innerHTML;
            textContent = textContent.replace(action.find, action.replace);
            item.innerHTML = textContent;
            doTrackAction(domHelper, action, item);
        }
    }

    function getMoveTarget(domHelper, rootItem, searchItem, selector) {
        var targetItem = null;

        targetItem = domHelper.find(selector, searchItem);
        if (targetItem === null) {
            if (rootItem === searchItem) {
                targetItem = null;
            }
            else {
                targetItem = getMoveTarget(domHelper, rootItem, domHelper.getParent(searchItem), selector);
            }
        }

        return targetItem;
    }

    function doMoveBeforeAction(domHelper, action, onItems, container) {
        var count = 0, length = 0, clonedItem = null, targetItem = null, item = null;

        if (isRuleNameMatches(action, 'moveBefore') === false) {
            return;
        }

        length = onItems.length;
        for (; count < length; count += 1) {
            item = onItems[count];
            if (Helper.isObject(item) === false) {
                continue;
            }
            clonedItem = domHelper.clone(item, true);
            doTrackAction(domHelper, action, clonedItem);
            targetItem = getMoveTarget(domHelper, container, item, action.selector.join(', '));
            if (Helper.isObject(targetItem) === true) {
                domHelper.addBefore(clonedItem, targetItem, domHelper.getParent(targetItem));
                domHelper.remove(item, domHelper.getParent(item));
                onItems[count] = null;
            }
        }
    }

    function doMoveAfterAction(domHelper, action, onItems, container) {
        var count = 0, length = 0, clonedItem = null, targetItem = null, item = null;

        if (isRuleNameMatches(action, 'moveAfter') === false) {
            return;
        }

        length = onItems.length;
        for (; count < length; count += 1) {
            item = onItems[count];
            if (Helper.isObject(item) === false) {
                continue;
            }
            clonedItem = domHelper.clone(item, true);
            doTrackAction(domHelper, action, clonedItem);
            targetItem = getMoveTarget(domHelper, container, item, action.selector.join(', '));
            if (Helper.isObject(targetItem) === true) {
                domHelper.addAfter(clonedItem, targetItem, domHelper.getParent(targetItem));
                domHelper.remove(item, domHelper.getParent(item));
                onItems[count] = null;
            }
        }
    }

    function doTemplateBeforeAction(domHelper, action, onItems) {
        var count = 0, length = 0, item = null, newItem = null;

        if (isRuleNameMatches(action, 'templateBefore') === false) {
            return;
        }

        length = onItems.length;
        for (; count < length; count += 1) {
            item = onItems[count];
            newItem = domHelper.create('span');
            newItem.innerHTML = action.template.join('');
            if (newItem.childNodes.length === 1) {
                newItem = newItem.childNodes[0];
            }
            doTrackAction(domHelper, action, newItem);
            domHelper.addBefore(newItem, item, domHelper.getParent(item));
        }
    }

    function doTemplateAfterAction(domHelper, action, onItems) {
        var count = 0, length = 0, item = null, newItem = null;

        if (isRuleNameMatches(action, 'templateAfter') === false) {
            return;
        }

        length = onItems.length;
        for (; count < length; count += 1) {
            item = onItems[count];
            newItem = domHelper.create('span');
            newItem.innerHTML = action.template.join('');
            if (newItem.childNodes.length === 1) {
                newItem = newItem.childNodes[0];
            }
            doTrackAction(domHelper, action, newItem);
            domHelper.addAfter(newItem, item, domHelper.getParent(item));
        }
    }

    function doApplyAction(self, action, onItems, container) {
        var domHelper = self.domHelper;

        doRemoveNodeAction(domHelper, action, onItems);
        doTextReplaceAction(domHelper, action, onItems);
        doRegexReplaceAction(domHelper, action, onItems);
        doMoveBeforeAction(domHelper, action, onItems, container);
        doMoveAfterAction(domHelper, action, onItems, container);
        doTemplateBeforeAction(domHelper, action, onItems);
        doTemplateAfterAction(domHelper, action, onItems);
    }

    function doApplyActions(self, actions, onItems, container) {
        var count = 0;

        for (; count < actions.length; count += 1) {
            doApplyAction(self, actions[count], onItems, container);
        }
    }

    function doNodeExist(domHelper, condition, container, result) {
        var selections = null;

        if (isRuleNameMatches(condition, 'nodeExist') === false) {
            return;
        }

        if (condition.selectAll === true) {
            selections = domHelper.findAll(condition.selector.join(', '), container);
        }
        else {
            selections = domHelper.find(condition.selector.join(', '), container);
            if (selections !== null) {
                selections = [selections];
            }
        }
        result.push.apply(result, selections);
    }

    function doSwitchToPreviousSibiling(domHelper, condition, container, result) {
        var count = 0, length = result.length, item = null, cloneResult = [];

        if (isRuleNameMatches(condition, 'switchToPreviousSibiling') === false) {
            return;
        }

        cloneResult.push.apply(cloneResult, result);
        result.splice(0, length);
        for (; count < length; count += 1) {
            item = cloneResult[count].previousSibling;
            if (Helper.isObject(item) === true) {
                result.push(item);
            }
        }
    }

    function doSwitchToNextSibiling(domHelper, condition, container, result) {
        var count = 0, length = result.length, item = null, cloneResult = [];

        if (isRuleNameMatches(condition, 'switchToNextSibiling') === false) {
            return;
        }

        cloneResult.push.apply(cloneResult, result);
        result.splice(0, length);
        for (; count < length; count += 1) {
            item = cloneResult[count].nextSibling;
            if (Helper.isObject(item) === true) {
                result.push(item);
            }
        }
    }

    function doSwitchToParentNode(domHelper, condition, container, result) {
        var count = 0, length = result.length, item = null, cloneResult = [];

        if (isRuleNameMatches(condition, 'switchToParentNode') === false) {
            return;
        }

        cloneResult.push.apply(cloneResult, result);
        result.splice(0, length);
        for (; count < length; count += 1) {
            item = cloneResult[count].parentNode;
            if (Helper.isObject(item) === true) {
                result.push(item);
            }
        }
    }

    function doSwitchToFirstChildNode(domHelper, condition, container, result) {
        var count = 0, length = result.length, item = null, cloneResult = [];

        if (isRuleNameMatches(condition, 'switchToFirstChildNode') === false) {
            return;
        }

        cloneResult.push.apply(cloneResult, result);
        result.splice(0, length);
        for (; count < length; count += 1) {
            item = cloneResult[count].childNodes[0];
            if (Helper.isObject(item) === true) {
                result.push(item);
            }
        }
    }

    function doSwitchToLastChildNode(domHelper, condition, container, result) {
        var count = 0, length = result.length, item = null, cloneResult = [],
            childNodesLen = 0;

        if (isRuleNameMatches(condition, 'switchToLastChildNode') === false) {
            return;
        }

        cloneResult.push.apply(cloneResult, result);
        result.splice(0, length);
        for (; count < length; count += 1) {
            childNodesLen = cloneResult[count].childNodes.length;
            item = cloneResult[count].childNodes[childNodesLen - 1];
            if (Helper.isObject(item) === true) {
                result.push(item);
            }
        }
    }

    function doFilterRegexMatch(domHelper, condition, container, result) {
        var count = 0, length = result.length, item = null, cloneResult = [];

        if (isRuleNameMatches(condition, 'filterRegexMatch') === false) {
            return;
        }

        cloneResult.push.apply(cloneResult, result);
        result.splice(0, length);
        for (; count < length; count += 1) {
            item = cloneResult[count];
            if (item.textContent.match(condition.regex) !== null) {
                result.push(item);
            }
        }
    }

    function isFollowedByClass(node, className) {
        var next = node.nextSibling, value = null;

        if (Helper.isObject(next) === true) {
            while (next.nodeType === 1 && next.childNodes.length > 0) {
                if (next.classList.contains(className) === true) {
                    return true;
                }
                next = next.childNodes[0];
            }
            if (next.nodeType === 3) {
                value = next.nodeValue;
                if (value.length > 0) {
                    return false;
                }
            }

            return isFollowedByClass(next, className);
        }

        return isFollowedByClass(node.parentNode, className);
    }

    function doFilterFollowedByClass(domHelper, condition, container, result) {
        var count = 0, length = result.length, item = null, cloneResult = [];

        if (isRuleNameMatches(condition, 'filterFollowedByClass') === false || length === 0) {
            return;
        }

        cloneResult.push.apply(cloneResult, result);
        result.splice(0, length);
        for (; count < length; count += 1) {
            item = cloneResult[count];
            if (isFollowedByClass(item, condition.className) === condition.direction) {
                result.push(item);
            }
        }
    }

    function isPrecededWithClass(node, className) {
        var prev = node.previousSibling, value = null;

        if (Helper.isObject(prev) === true) {
            while (prev.nodeType === 1 && prev.childNodes.length > 0) {
                if (prev.classList.contains(className) === true) {
                    return true;
                }
                prev = prev.childNodes[prev.childNodes.length - 1];
            }
            if (prev.nodeType === 3) {
                value = prev.nodeValue;
                if (value.length > 0) {
                    return false;
                }
            }

            return isPrecededWithClass(prev, className);
        }

        return isPrecededWithClass(node.parentNode, className);
    }

    function doFilterPrecededWithClass(domHelper, condition, container, result) {
        var count = 0, length = result.length, item = null, cloneResult = [];

        if (isRuleNameMatches(condition, 'filterPrecededWithClass') === false || length === 0) {
            return;
        }

        cloneResult.push.apply(cloneResult, result);
        result.splice(0, length);
        for (; count < length; count += 1) {
            item = cloneResult[count];
            if (isPrecededWithClass(item, condition.className) === condition.direction) {
                result.push(item);
            }
        }
    }

    function isFollowedByText(node, regex) {
        var next = node.nextSibling, value = null;

        if (Helper.isObject(next) === true) {
            while (next.nodeType === 1 && next.childNodes.length > 0) {
                next = next.childNodes[0];
            }
            if (next.nodeType === 3) {
                value = next.nodeValue;
                if (value.length > 0) {
                    if (value.match(regex) !== null) {
                        return true;
                    }

                    return false;
                }
            }

            return isFollowedByText(next, regex);
        }

        return isFollowedByText(node.parentNode, regex);
    }

    function doFilterFollowedByText(domHelper, condition, container, result) {
        var count = 0, length = result.length, item = null, cloneResult = [];

        if (isRuleNameMatches(condition, 'filterFollowedByText') === false || length === 0) {
            return;
        }

        cloneResult.push.apply(cloneResult, result);
        result.splice(0, length);
        for (; count < length; count += 1) {
            item = cloneResult[count];
            if (isFollowedByText(item, condition.regex) === condition.direction) {
                result.push(item);
            }
        }
    }

    function isPrecededWithText(node, regex) {
        var prev = node.previousSibling, value = null;

        if (Helper.isObject(prev) === true) {
            while (prev.nodeType === 1 && prev.childNodes.length > 0) {
                prev = prev.childNodes[prev.childNodes.length - 1];
            }
            if (prev.nodeType === 3) {
                value = prev.nodeValue;
                if (value.length > 0) {
                    if (value.match(regex) !== null) {
                        return true;
                    }

                    return false;
                }
            }

            return isPrecededWithText(prev, regex);
        }

        return isPrecededWithText(node.parentNode, regex);
    }

    function doFilterPrecededWithText(domHelper, condition, container, result) {
        var count = 0, length = result.length, item = null, cloneResult = [];

        if (isRuleNameMatches(condition, 'filterPrecededWithText') === false || length === 0) {
            return;
        }

        cloneResult.push.apply(cloneResult, result);
        result.splice(0, length);
        for (; count < length; count += 1) {
            item = cloneResult[count];
            if (isPrecededWithText(item, condition.regex) === condition.direction) {
                result.push(item);
            }
        }
    }

    function doApplyCondition(self, condition, container, result) {
        var domHelper = self.domHelper;

        doNodeExist(domHelper, condition, container, result);
        doSwitchToPreviousSibiling(domHelper, condition, container, result);
        doSwitchToNextSibiling(domHelper, condition, container, result);
        doSwitchToParentNode(domHelper, condition, container, result);
        doSwitchToFirstChildNode(domHelper, condition, container, result);
        doSwitchToLastChildNode(domHelper, condition, container, result);
        doFilterRegexMatch(domHelper, condition, container, result);
        doFilterFollowedByClass(domHelper, condition, container, result);
        doFilterPrecededWithClass(domHelper, condition, container, result);
        doFilterFollowedByText(domHelper, condition, container, result);
        doFilterPrecededWithText(domHelper, condition, container, result);
    }

    function doApplyConditions(self, conditions, container) {
        var count = 0, result = [];

        for (; count < conditions.length; count += 1) {
            doApplyCondition(self, conditions[count], container, result);
        }

        return result;
    }

    function applyRules(self, rules, container) {
        var itemsSatisfied = null, count = 0, rule = null;

        for (; count < rules.length; count += 1) {
            rule = rules[count];
            itemsSatisfied = doApplyConditions(self, rule.condition, container);
            doApplyActions(self, rule.action, itemsSatisfied, container);
        }
    }

    function identifyReferenceStyleType(self, target) {
        var count = 0, length = 0, styleType = null, styleTypes = [], result = null;

        length = Config.stylingTypes.length;
        for (; count < length; count += 1) {
            styleType = Config.stylingTypes[count];
            result = doApplyConditions(self, styleType.condition, target);
            if (result.length > 0) {
                styleTypes.push(styleType);
            }
        }

        return styleTypes;
    }

    function enableReferenceStyleTypeInCss(refStyleType, target) {
        target.classList.add(refStyleType.name);
    }

    function applyRulesBasedOnReferenceItem(self, referenceItem) {
        var count = 0, length = 0, refStyleTypes = null, refStyleType = null;

        refStyleTypes = identifyReferenceStyleType(self, referenceItem);
        length = refStyleTypes.length;
        for (; count < length; count += 1) {
            refStyleType = refStyleTypes[count];
            enableReferenceStyleTypeInCss(refStyleType, referenceItem);
            applyRules(self, refStyleType.rules, referenceItem);
        }
    }

    function removeEmptyNodesFromReference(self, container) {
        var treeWalker = null, flag = 0, diff = 0, counter = 0, match = null,
            domHelper = self.domHelper, currentNode = null,
            previousNode = null, previousNodeParent = null;

        container.normalize();
        treeWalker = document.createTreeWalker(container, NodeFilter.SHOW_ALL, null, false);
        while (Helper.isNull(treeWalker.nextNode()) !== true) {
            currentNode = treeWalker.currentNode;
            if (currentNode.nodeType === 3) {
                match = currentNode.textContent.match(/^((\,)|(\.))(\s+)?$/g);
                if (Helper.isNull(match) !== true) {
                    diff = counter - flag;
                    if (diff === 1) {
                        domHelper.remove(previousNode, previousNodeParent);
                    }

                    previousNode = domHelper.getParent(currentNode);
                    previousNodeParent = domHelper.getParent(previousNode);
                    flag = counter;
                }

                counter += 1;
            }
        }
    }

    function removeNodesFromReference(
        self, dotCommaMatch, previousNodeElem, currentNode, parentOfPreviousNodeElem
    ) {
        var nbspMatch = null, domHelper = self.domHelper, currentNodeElement = null,
            parentOfCurrentNodeElem = null;

        if (Helper.isNull(dotCommaMatch) === false &&
            Helper.isNull(previousNodeElem) === false) {
            nbspMatch = previousNodeElem.textContent.match(/^\s+$/g);
            if (Helper.isNull(nbspMatch) === false) {
                currentNodeElement = domHelper.getParent(currentNode);
                parentOfCurrentNodeElem = domHelper.getParent(currentNodeElement);
                domHelper.remove(previousNodeElem, parentOfPreviousNodeElem);
                domHelper.remove(currentNodeElement, parentOfCurrentNodeElem);
            }
        }
    }

    function normalizeReference(self, container) {
        var treeWalker = null, domHelper = self.domHelper, dotCommaMatch = null,
            currentNode = null, previousNodeElem = null, parentOfPreviousNodeElem = null;

        container.normalize();
        treeWalker = document.createTreeWalker(container, NodeFilter.SHOW_ALL, null, false);
        while (Helper.isNull(treeWalker.nextNode()) !== true) {
            currentNode = treeWalker.currentNode;
            if (currentNode.nodeType === 3) {
                dotCommaMatch = currentNode.textContent.match(/^((,)|(\.))(\s+)?$/g);
                removeNodesFromReference(
                    self, dotCommaMatch, previousNodeElem, currentNode, parentOfPreviousNodeElem
                );
                previousNodeElem = domHelper.getParent(currentNode);
                parentOfPreviousNodeElem = domHelper.getParent(previousNodeElem);
            }
        }
    }

    function RefStyler(doTrackAction) {
        trackAction = doTrackAction;
    }

    proto = RefStyler.prototype;

    proto.doStyling = function doStylingFn(self, container) {
        applyRulesBasedOnReferenceItem(self, container);
        removeEmptyNodesFromReference(self, container);
        normalizeReference(self, container);
    };

    return RefStyler;
});
