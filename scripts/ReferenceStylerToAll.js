/*global define*/
define([
    'rainbow/util/DomHelpers', 'rainbow/ReferenceStyler',
    'rainbow-config/ReferenceStyleConfig', 'css!rainbow-default/reference-style'
],
function defineFn(DomHelpers, RefStyler, Config) {
    var proto = null, trackAction = false;

    function applyRulesToAll(self, container) {
        var option = Config.stylingOption, referenceItems = null,
            refStyler = null, domHelper = self.domHelper,
            count = 0, length = 0;

        referenceItems = domHelper.findAll(option.referenceItem, container);
        length = referenceItems.length;
        refStyler = new RefStyler(trackAction);
        for (; count < length; count += 1) {
            refStyler.doStyling(self, referenceItems[count]);
        }
    }

    function RefStylerAll(doTrackAction) {
        this.domHelper = new DomHelpers();
        trackAction = doTrackAction;
    }

    proto = RefStylerAll.prototype;

    proto.doStylingToAll = function doStylingToAllFn(container) {
        applyRulesToAll(this, container);
    };

    return RefStylerAll;
});
