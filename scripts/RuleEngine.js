/*global define*/
define([
    'rainbow/FigureBreaker', 'rainbow/TableSplitter', 'rainbow/BoxProcessor',
    'phoenix/TableCharAlign', 'phoenix/Logger', 'rainbow/NumberToWord'
], function defineFn(
    FigureBreaker, TableSplitter, BoxProcessor,
    TableCharAlign, Logger, NumberToWord
) {
    var RuleEngine = function RuleEngineClass(win, doc, ruleName, eventBus, rules) {
        var param = {},
            currentRules = null,
            currentCondition = null,
            currentAction = null,
            availableConditions = null,
            availableActions = null;

        function doNodeExist(condition, container, result) {
            var selections = null;

            if (condition.selectAll === true) {
                selections = container.querySelectorAll(
                    condition.selector.join(', ')
                );
            }
            else {
                selections = container.querySelector(
                    condition.selector.join(', ')
                );
                if (selections !== null) {
                    selections = [selections];
                }
            }
            result.push.apply(result, selections);
        }

        function doFilterNodeExist(condition, container, result) {
            var selections = [], length = result.length,
                item = null, cloneResult = [], count = 0, index = -1;

            selections.push.apply(
                selections,
                container.querySelectorAll(condition.selector.join(', '))
            );
            cloneResult.push.apply(cloneResult, result);
            if (condition.direction === true) {
                result.splice(0, length);
            }
            for (; count < length; count += 1) {
                item = cloneResult[count];
                index = selections.indexOf(item);
                if (index >= 0) {
                    if (condition.direction === true) {
                        result.push(item);
                    }
                    else {
                        index = result.indexOf(item);
                        result.splice(index, 1);
                    }
                }
            }
        }

        function doFilterChildExist(condition, container, result) {
            var count = 0, length = result.length, item = null,
                cloneResult = [], childSelection = null;

            cloneResult.push.apply(cloneResult, result);
            result.splice(0, length);
            for (; count < length; count += 1) {
                item = cloneResult[count];
                childSelection = item.querySelectorAll(
                    condition.selector.join(', ')
                );
                if ((childSelection.length > 0) === condition.direction) {
                    result.push(item);
                }
            }
        }

        function doFilterChildCount(condition, container, result) {
            var count = 0, length = result.length, item = null,
                cloneResult = [], childSelection = null;

            cloneResult.push.apply(cloneResult, result);
            result.splice(0, length);
            for (; count < length; count += 1) {
                item = cloneResult[count];
                childSelection = item.querySelectorAll(
                    condition.selector.join(', ')
                );
                if (
                    condition.greater === true &&
                    childSelection.length > condition.count
                ) {
                    result.push(item);
                }
                else if (
                    condition.lesser === true &&
                    childSelection.length < condition.count
                ) {
                    result.push(item);
                }
                else if (
                    condition.greater === false &&
                    condition.lesser === false &&
                    childSelection.length === condition.count
                ) {
                    result.push(item);
                }
            }
        }

        function doGetLastNode(condition, container, result) {
            var length = result.length, cloneResult = [];

            cloneResult.push.apply(cloneResult, result);
            result.splice(0, length);
            if (length > 0) {
                result.push(cloneResult[length - 1]);
            }
        }

        function doGetScopeNode(condition, container, result) {
            result.push(container);
        }

        function doGetParentNode(condition, container, result) {
            var count = 0, length = result.length,
                item = null, cloneResult = [];

            cloneResult.push.apply(cloneResult, result);
            result.splice(0, length);
            for (; count < length; count += 1) {
                item = cloneResult[count].parentNode;
                if (item !== null && typeof item !== 'undefined') {
                    result.push(item);
                }
            }
        }

        function doGetPreviousNode(condition, container, result) {
            var count = 0, length = result.length,
                item = null, cloneResult = [];

            cloneResult.push.apply(cloneResult, result);
            result.splice(0, length);
            for (; count < length; count += 1) {
                item = cloneResult[count].previousSibling;
                if (item !== null && typeof item !== 'undefined') {
                    result.push(item);
                }
            }
        }

        function dolastElementChildNode(condition, container, result) {
            var count = 0, length = result.length,
                item = null, cloneResult = [];

            cloneResult.push.apply(cloneResult, result);
            result.splice(0, length);
            for (; count < length; count += 1) {
                item = cloneResult[count].lastElementChild;
                if (item !== null && typeof item !== 'undefined') {
                    result.push(item);
                }
            }
        }

        function doFilterRegexMatch(condition, container, result) {
            var count = 0, length = result.length,
                item = null, cloneResult = [];

            cloneResult.push.apply(cloneResult, result);
            result.splice(0, length);
            for (; count < length; count += 1) {
                item = cloneResult[count];
                if (
                    (item.textContent.match(condition.regex) !== null &&
                    condition.direction === true) ||
                    (item.textContent.match(condition.regex) === null &&
                    condition.direction === false)
                ) {
                    result.push(item);
                }
            }
        }

        function doFilterSingleLine(condition, container, result) {
            var count = 0, length = result.length,
                item = null, cloneResult = [],
                firstItem = null, lastItem = null;

            cloneResult.push.apply(cloneResult, result);
            result.splice(0, length);
            for (; count < length; count += 1) {
                item = cloneResult[count];
                firstItem = item.firstElementChild;
                lastItem = item.lastElementChild;
                if (
                    firstItem !== null &&
                    lastItem !== null &&
                    firstItem !== lastItem
                ) {
                    firstItem = firstItem.getBoundingClientRect();
                    lastItem = lastItem.getBoundingClientRect();
                    if (firstItem.bottom >= lastItem.bottom) {
                        result.push(item);
                    }
                }
            }
        }

        availableConditions = {
            'nodeExist': doNodeExist,
            'filterNodeExist': doFilterNodeExist,
            'filterChildExist': doFilterChildExist,
            'filterChildCount': doFilterChildCount,
            'lastNode': doGetLastNode,
            'scopeNode': doGetScopeNode,
            'parentNode': doGetParentNode,
            'previousNode': doGetPreviousNode,
            'lastElementChildNode': dolastElementChildNode,
            'filterRegexMatch': doFilterRegexMatch,
            'filterSingleLine': doFilterSingleLine
        };

        function replaceAllKeyTerms(content, data) {
            var propkey = null, value = null;

            if (
                data === null || typeof data === 'undefined' ||
                content === null || typeof content === 'undefined'
            ) {
                return content;
            }
            for (propkey in data) {
                if (data.hasOwnProperty(propkey) === false) {
                    continue;
                }
                value = data[propkey];
                content = content.replace(':' + propkey, value);
            }

            return content;
        }

        function doApplyStyleAction(action, onItems, data) {
            var count = 0, length = 0, item = null,
                propkey = null, value = null;

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                for (propkey in action.style) {
                    if (action.style.hasOwnProperty(propkey) === false) {
                        continue;
                    }
                    value = action.style[propkey];
                    value = replaceAllKeyTerms(value, data);
                    item.style[propkey] = value;
                }
            }
        }

        function doAddClassAction(action, onItems) {
            var count = 0, i = 0, length = 0, item = null;

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                for (i = 0; i < action.classNames.length; i += 1) {
                    item.classList.add(action.classNames[i]);
                }
            }
        }

        function doAddAttributeAction(action, onItems) {
            var count = 0, length = 0, item = null;

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                item.setAttribute(
                    action.attributeName, action.attributeValue
                );
            }
        }

        function doRemoveAttributeAction(action, onItems) {
            var count = 0, length = 0, item = null;

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                item.removeAttribute(action.attributeName);
            }
        }

        function doRemoveClassAction(action, onItems) {
            var count = 0, length = 0, item = null;

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                item.classList.remove.apply(
                    item.classList, action.classNames
                );
            }
        }

        function doRemoveNodeAction(action, onItems) {
            var count = 0, length = 0, item = null, parent = null, result = [];

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                parent = item.parentNode;
                parent.removeChild(item);
                result.push(parent);
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doRemoveNodeWrapAction(action, onItems) {
            var count = 0, length = 0, item = null, parent = null,
                result = [], child = null;

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                parent = item.parentNode;
                while (item.childNodes.length > 0) {
                    child = item.childNodes[0];
                    parent.insertBefore(child, item);
                    result.push(parent);
                }
                parent.removeChild(item);
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doRegexReplaceAction(action, onItems) {
            var count = 0, length = 0, textContent = '', item = null;

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                textContent = item.innerHTML;
                textContent = textContent.replace(
                    action.find, action.replace
                );
                item.innerHTML = textContent;
            }
        }

        function doRegexAttributeReplaceAction(action, onItems) {
            var count = 0, length = 0, textContent = '', item = null;

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                textContent = item.getAttribute(action.attributeName);
                textContent = textContent.replace(
                    action.find, action.replace
                );
                item.setAttribute(action.attributeName, textContent);
            }
        }

        function copyIntoTarget(source, target, copyChilds, replaceChilds) {
            var newItem = doc.createElement('span');

            if (copyChilds === true) {
                newItem.innerHTML = source.innerHTML;
            }
            else {
                newItem.appendChild(source);
            }
            if (replaceChilds === true) {
                while (target.childNodes.length > 0) {
                    target.removeChild(target.childNodes[0]);
                }
            }
            while (newItem.childNodes.length > 0) {
                target.appendChild(newItem.childNodes[0]);
            }
        }

        function doCopyNodeAction(action, onItems, data, container) {
            var count = 0, length = 0, targetItems = null,
                targetItem = null, item = null, i = 0, clonedItem = null;

            targetItems = container.querySelectorAll(
                action.selector.join(', ')
            );
            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                clonedItem = item.cloneNode(true);
                if (action.parallelCopy === true) {
                    if (targetItems.length > count) {
                        targetItem = targetItems[count];
                        copyIntoTarget(
                            clonedItem, targetItem,
                            action.copyChilds, action.replaceChilds
                        );
                    }
                }
                else {
                    for (i = 0; i < targetItems.length; i += 1) {
                        targetItem = targetItems[i];
                        copyIntoTarget(
                            clonedItem, targetItem,
                            action.copyChilds, action.replaceChilds
                        );
                    }
                }
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, targetItems);
        }

        function doCopyAttributeAction(action, onItems) {
            var count = 0, length = 0, i = 0,
                textContent = '', existingContent = '',
                item = null, fromItem = null, toItems = null, result = [];

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                fromItem = null;
                if (action.fromSelector.length > 0) {
                    fromItem = item.querySelector(
                        action.fromSelector.join(', ')
                    );
                }
                if (fromItem === null) {
                    fromItem = item;
                }
                textContent = fromItem.getAttribute(action.fromAttribute);
                if (textContent === null) {
                    textContent = '';
                }
                toItems = null;
                if (action.toSelector.length > 0) {
                    toItems = item.querySelectorAll(
                        action.toSelector.join(', ')
                    );
                }
                if (toItems === null) {
                    toItems = [item];
                }
                for (i = 0; i < toItems.length; i += 1) {
                    item = toItems[i];
                    if (action.append === true) {
                        existingContent = item.getAttribute(action.toAttribute);
                        if (existingContent === null) {
                            existingContent = '';
                        }
                        existingContent += textContent;
                        item.setAttribute(action.toAttribute, existingContent);
                    }
                    else {
                        item.setAttribute(action.toAttribute, textContent);
                    }
                    result.push(item);
                }
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doMoveBeforeAction(action, onItems, data, container) {
            var count = 0, length = 0, clonedItem = null,
                targetItems = null, targetItem = null,
                item = null, parent = null, i = 0, result = [];

            targetItems = container.querySelectorAll(
                action.selector.join(', ')
            );
            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                for (i = 0; i < targetItems.length; i += 1) {
                    targetItem = targetItems[i];
                    clonedItem = item.cloneNode(true);
                    parent = targetItem.parentNode;
                    parent.insertBefore(clonedItem, targetItem);
                    result.push(clonedItem);
                }
                if (targetItems.length > 0) {
                    parent = item.parentNode;
                    parent.removeChild(item);
                }
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doMoveIntoAction(action, onItems, data, container) {
            var count = 0, length = 0, clonedItem = null,
                targetItems = null, targetItem = null,
                item = null, parent = null, i = 0, result = [];

            targetItems = container.querySelectorAll(
                action.selector.join(', ')
            );
            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                for (i = 0; i < targetItems.length; i += 1) {
                    targetItem = targetItems[i];
                    clonedItem = item.cloneNode(true);
                    targetItem.appendChild(clonedItem);
                    result.push(clonedItem);
                }
                if (targetItems.length > 0) {
                    parent = item.parentNode;
                    parent.removeChild(item);
                }
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doMoveAfterAction(action, onItems, data, container) {
            var count = 0, length = 0, clonedItem = null,
                targetItems = null, targetItem = null,
                item = null, parent = null, i = 0, result = [];

            targetItems = container.querySelectorAll(
                action.selector.join(', ')
            );
            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                for (i = 0; i < targetItems.length; i += 1) {
                    targetItem = targetItems[i];
                    clonedItem = item.cloneNode(true);
                    parent = targetItem.parentNode;
                    if (parent.lastChild === targetItem) {
                        parent.appendChild(clonedItem);
                    }
                    else {
                        parent.insertBefore(
                            clonedItem, targetItem.nextSibling
                        );
                    }
                    result.push(clonedItem);
                }
                if (targetItems.length > 0) {
                    parent = item.parentNode;
                    parent.removeChild(item);
                }
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doWrapChildsAction(action, onItems) {
            var count = 0, length = 0, item = null, newItem = null,
                child = null, i = 0, exceedLimit = false, moveNodes = null,
                result = [], elements = null, directChild = null;

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                directChild = [];
                if (action.directChildSeletor.length > 0) {
                    elements = item.querySelectorAll(
                        action.directChildSeletor.join(',')
                    );
                    directChild.push.apply(directChild, elements);
                }
                do {
                    exceedLimit = false;
                    newItem = doc.createElement(action.tagName);
                    result.push(newItem);
                    moveNodes = [];
                    for (i = 0; i < item.childNodes.length; i += 1) {
                        child = item.childNodes[i];

                        if ((action.direction === true && directChild.indexOf(child) >= 0) ||
                            (action.direction === false && directChild.indexOf(child) < 0)
                        ) {
                            if (moveNodes.length === 0) {
                                item.insertBefore(newItem, child);
                                i += 1;
                            }
                            moveNodes.push(child);
                            if (
                                action.wrapLimit !== null &&
                                moveNodes.length === action.wrapLimit
                            ) {
                                exceedLimit = true;
                                break;
                            }
                        }
                        else if (moveNodes.length > 0) {
                            break;
                        }
                    }
                    for (i = 0; i < moveNodes.length; i += 1) {
                        newItem.appendChild(moveNodes[i]);
                    }
                } while (exceedLimit === true);
            }
            doAddClassAction(action, result);
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doTemplateBeforeAction(action, onItems, data) {
            var count = 0, length = 0, item = null, newItem = null,
                parent = null, text = '', result = [];

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                newItem = doc.createElement('span');
                text = action.template.join('');
                text = replaceAllKeyTerms(text, data);
                newItem.innerHTML = text;
                if (newItem.childNodes.length === 1) {
                    newItem = newItem.childNodes[0];
                }
                parent = item.parentNode;
                parent.insertBefore(newItem, item);
                result.push(newItem);
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doTemplateReplaceAction(action, onItems, data) {
            var count = 0, length = 0, item = null, newItem = null,
                text = '', result = [];

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                newItem = doc.createElement('span');
                text = action.template.join('');
                text = replaceAllKeyTerms(text, data);
                newItem.innerHTML = text;
                if (newItem.childNodes.length === 1 &&
                    newItem.childNodes[0].nodeType === 1
                ) {
                    newItem = newItem.childNodes[0];
                }
                while (item.childNodes.length > 0) {
                    item.removeChild(item.childNodes[0]);
                }
                item.appendChild(newItem);
                result.push(newItem);
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doTemplateAppendAction(action, onItems, data) {
            var count = 0, length = 0, item = null, newItem = null,
                text = '', result = [];

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                newItem = doc.createElement('span');
                text = action.template.join('');
                text = replaceAllKeyTerms(text, data);
                newItem.innerHTML = text;
                if (newItem.childNodes.length === 1 &&
                    newItem.childNodes[0].nodeType === 1
                ) {
                    newItem = newItem.childNodes[0];
                }
                item.appendChild(newItem);
                result.push(newItem);
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doTemplateAfterAction(action, onItems, data) {
            var count = 0, length = 0, item = null, newItem = null,
                parent = null, text = '', result = [];

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                newItem = doc.createElement('span');
                text = action.template.join('');
                text = replaceAllKeyTerms(text, data);
                newItem.innerHTML = text;
                if (newItem.childNodes.length === 1) {
                    newItem = newItem.childNodes[0];
                }
                parent = item.parentNode;
                if (parent.lastChild === item) {
                    parent.appendChild(newItem);
                }
                else {
                    parent.insertBefore(newItem, item.nextSibling);
                }
                result.push(newItem);
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function getAllChildHeight(node) {
            var i = 0, length = 0, child = null, height = 0,
                clientRect = null;

            length = node.childNodes.length;
            for (; i < length; i += 1) {
                child = node.childNodes[i];
                clientRect = child.getBoundingClientRect();
                height += clientRect.height;
            }

            return height;
        }

        function doApplyFitHeightActionItem(action, onItems, data, container) {
            var count = 0, length = 0, item = null, child = null,
                height = 0, scrollHeight = 0, clientRect = null, result = [];

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                child = container.querySelector(action.selector.join(', '));
                if (child === null || typeof child === 'undefined') {
                    continue;
                }
                clientRect = child.getBoundingClientRect();
                height = clientRect.height - 7;
                item.style.height = height + 'px';
                result.push(item);
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doApplyFitHeightAction(action, onItems) {
            var count = 0, length = 0, item = null, child = null,
                height = 0, scrollHeight = 0, clientRect = null, result = [];

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                child = item.querySelector(action.selector.join(', '));
                if (child === null || typeof child === 'undefined') {
                    continue;
                }
                clientRect = child.getBoundingClientRect();
                height = clientRect.height;
                clientRect = item.getBoundingClientRect();
                scrollHeight = getAllChildHeight(item);
                height += clientRect.height - scrollHeight;
                child.style.height = height + 'px';
                result.push(child);
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doVerticalMiddleAction(action, onItems) {
            var count = 0, length = 0, item = null, clientRect = null,
                height = 0, childHeight = 0;

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                clientRect = item.parentNode.getBoundingClientRect();
                height = clientRect.height;
                clientRect = item.getBoundingClientRect();
                childHeight = clientRect.height;
                height = (height - childHeight) / 2;
                item.style.position = 'relative';
                item.style.top = height + 'px';
            }
        }

        function getCellElement(row, position) {
            var cells = [], i = 0, spanCount = 0,
                spanPosition = 0, item = null;

            for (; i < row.childNodes.length; i += 1) {
                item = row.childNodes[i];
                if (item.nodeType === 1) {
                    cells.push(item);
                }
            }
            for (i = 0; i < cells.length; i += 1) {
                item = cells[i];
                if ((i + spanPosition) === position) {
                    return item;
                }
                spanCount = item.getAttribute('colspan');
                if (spanCount === null || spanCount === '') {
                    continue;
                }
                spanCount -= 1;
                if (spanCount > 0) {
                    spanPosition += spanCount;
                }
            }

            return null;
        }

        function getCellPosition(row, cell) {
            var cells = [], i = 0, position = -1, item = null,
                spanCount = 0, spanPosition = 0;

            for (; i < row.childNodes.length; i += 1) {
                item = row.childNodes[i];
                if (item.nodeType === 1) {
                    cells.push(item);
                }
            }
            position = cells.indexOf(cell);
            for (i = 0; i < position; i += 1) {
                item = cells[i];
                spanCount = item.getAttribute('colspan');
                if (spanCount === null || spanCount === '') {
                    continue;
                }
                spanCount -= 1;
                if (spanCount > 0) {
                    spanPosition += spanCount;
                }
            }

            return position + spanPosition;
        }

        function getParentNode(node, nodeNames) {
            if (node !== null) {
                if (nodeNames.indexOf(node.nodeName.toLowerCase()) >= 0) {
                    return node;
                }

                return getParentNode(node.parentNode, nodeNames);
            }

            return null;
        }

        function doHandleTableRowSpanAction(action, onItems) {
            var count = 0, length = 0, currentCell = null, spanCount = 0,
                currentRow = null, newCell = null, columnPosition = 0, i = 0,
                colspan = 1, cell = null, result = [];

            length = onItems.length;
            for (; count < length; count += 1) {
                cell = onItems[count];
                colspan = cell.getAttribute('colspan');
                spanCount = cell.getAttribute('rowspan');
                currentRow = getParentNode(cell, ['tr']);
                columnPosition = getCellPosition(currentRow, cell);
                if (
                    spanCount === null || spanCount === '' ||
                    columnPosition < 0
                ) {
                    continue;
                }
                for (i = 1; i < spanCount; i += 1) {
                    currentRow = currentRow.nextSibling;
                    while (currentRow !== null && currentRow.nodeType !== 1) {
                        currentRow = currentRow.nextSibling;
                    }
                    if (currentRow === null) {
                        break;
                    }
                    newCell = doc.createElement('td');
                    newCell.style.border = '0px';
                    newCell.innerHTML = '&nbsp;';
                    if (colspan !== null && colspan !== '') {
                        newCell.setAttribute('colspan', colspan);
                    }
                    currentCell = getCellElement(currentRow, columnPosition);
                    if (currentCell !== null) {
                        currentRow.insertBefore(newCell, currentCell);
                    }
                    else {
                        currentRow.appendChild(newCell);
                    }
                    result.push(newCell);
                }
                cell.setAttribute('rowspan', '1');
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doCounterAction(action, onItems) {
            var count = 0, length = 0, item = null, targetItems = null,
                targetItem = null, counter = 1, i = 0, result = [];

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                targetItems = item.querySelectorAll(
                    action.selector.join(', ')
                );
                for (i = 0; i < targetItems.length; i += 1) {
                    targetItem = targetItems[i];
                    targetItem.innerHTML = counter;
                }
                counter += 1;
                result.push.apply(result, targetItems);
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doChildCounterAction(action, onItems) {
            var count = 0, length = 0, item = null, targetItems = null,
                targetItem = null, counter = 1, i = 0, result = [];

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                targetItems = item.querySelectorAll(
                    action.selector.join(', ')
                );
                counter = 1;
                for (i = 0; i < targetItems.length; i += 1) {
                    targetItem = targetItems[i];
                    targetItem.setAttribute(action.attributeName, counter);
                    counter += 1;
                }
                result.push.apply(result, targetItems);
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function isEmptySpace(content) {
            if (content === null || typeof content === 'undefined') {
                return true;
            }
            if (content.length > 0) {
                if (content.match(/[^\s]{1}/) === null) {
                    return true;
                }

                return false;
            }

            return true;
        }

        function untilContent(action, node, onItems) {
            var count = 0, hasTextNodeChild = false, flag = true,
                i = 0, child = null, childs = node.childNodes;

            node.classList.add(action.className);
            onItems.push(node);
            count = childs.length;
            if (action.fromTop === false) {
                i = count - 1;
            }
            while (
                (action.fromTop === true && i < count) ||
                (action.fromTop === false && i >= 0)
            ) {
                child = childs[i];
                if (
                    child.nodeType === 3 &&
                    isEmptySpace(child.textContent) === false
                ) {
                    hasTextNodeChild = true;
                    break;
                }
                if (child.nodeType === 1) {
                    flag = untilContent(action, child, onItems);
                    if (flag === false) {
                        return false;
                    }
                }
                if (action.fromTop === true) {
                    i += 1;
                }
                else {
                    i -= 1;
                }
            }
            if (
                hasTextNodeChild === true ||
                node.nodeName.toLowerCase() === 'img'
            ) {
                return false;
            }

            return flag;
        }

        function doUntilContentAction(action, onItems) {
            var count = 0, length = 0, item = null,
                result = [];

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                untilContent(action, item, result);
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function isEmptyContent(node, onItems) {
            var count = 0, hasTextNodeChild = false, flag = true,
                i = 0, child = null, childs = node.childNodes,
                computedStyle = null;

            onItems.push(node);
            computedStyle = win.getComputedStyle(node);
            if (computedStyle.display === 'none') {
                return true;
            }
            count = childs.length;
            while (i < count) {
                child = childs[i];
                if (
                    child.nodeType === 3 &&
                    isEmptySpace(child.textContent) === false
                ) {
                    hasTextNodeChild = true;
                    break;
                }
                if (child.nodeType === 1) {
                    flag = isEmptyContent(child, onItems);
                    if (flag === false) {
                        return false;
                    }
                }
                i += 1;
            }
            if (hasTextNodeChild === true || node.nodeName.toLowerCase() === 'img') {
                return false;
            }

            return flag;
        }

        function doEmptyContentAction(action, onItems) {
            var count = 0, length = 0, item = null,
                result = [];

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                if (isEmptyContent(item, result) === true) {
                    item.classList.add(action.className);
                }
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doEquationPathUpdation(action, onItems) {
            var count = 0, length = 0, item = null, version = null,
                equation = null, equationPath = null, result = [];

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                equation = item.querySelector(action.equationSelector);
                version = item.querySelector(action.versionSelector);
                if (equation === null || version === null) {
                    continue;
                }
                version = version.textContent;
                equationPath = action.updatedPathSelector.replace(
                    ':version', version
                );
                equationPath = item.querySelector(equationPath);
                if (equationPath === null) {
                    continue;
                }
                equationPath = equationPath.textContent;
                if (
                    equationPath === null ||
                    typeof equationPath === 'undefined' ||
                    equationPath.length === 0
                ) {
                    continue;
                }
                equation.setAttribute(
                    action.equationPathAttribute, equationPath
                );
                result.push(equation);
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function getAllInnerText(nodes) {
            var i = 0, texts = [], node = null;

            for (; i < nodes.length; i += 1) {
                node = nodes[i];
                texts.push(node.innerHTML);
            }

            return texts;
        }

        function removeAllNodes(nodes) {
            var i = 0, node = null;

            for (; i < nodes.length; i += 1) {
                node = nodes[i];
                node.parentNode.removeChild(node);
            }
        }

        function extractText(node) {
            var text = '', childs = null, child = null,
                length = 0, i = 0;

            if (node.nodeType === 3) {
                text = node.textContent;
            }
            childs = node.childNodes;
            length = childs.length;
            for (; i < length; i += 1) {
                child = childs[i];
                text += extractText(child);
            }

            return text;
        }

        function extractTextAll(nodes) {
            var length = 0, i = 0, node = null, text = '';

            length = nodes.length;
            for (; i < length; i += 1) {
                node = nodes[i];
                text += extractText(node);
            }

            return text;
        }

        function fetchInitialFromName(name) {
            var initials = '', i = 0, isHyphen = false, isInitial = false,
                names = [], namesLength = 0, currname = '';

            if (name.match(/-/) !== null) {
                isHyphen = true;
            }
            names = name.split(/[\s-]+/);
            namesLength = names.length;
            for (i = 0; i < namesLength; i += 1) {
                currname = names[i];
                currname = currname.replace(/^\s|\s$/, '');
                if (
                    currname.length === 0 ||
                    currname.match(/^\(/) !== null
                ) {
                    continue;
                }
                if (currname.match(/\./) !== null) {
                    isInitial = true;
                }
                if (isInitial === true) {
                    initials += currname;
                }
                if (isHyphen === true) {
                    if (initials.length > 0) {
                        initials += '-';
                    }
                    initials += currname.slice(0, 1);
                }
                if (isInitial === false && isHyphen === false) {
                    initials += currname.slice(0, 1);
                    initials += '.';
                }
            }

            return initials;
        }

        function fetchAuthorName(item, action) {
            var givenName = null, surName = null, authorName = '';

            givenName = extractTextAll(item.querySelectorAll(
                action.authorGivenNameSelector
            ));
            surName = extractTextAll(item.querySelectorAll(
                action.authorSurnameSelector
            ));
            if (givenName !== '') {
                givenName = fetchInitialFromName(givenName);
                if (surName !== '') {
                    authorName = ' (' + givenName + ' ' +
                        surName + ')';
                }
                else {
                    authorName = ' (' + givenName + ')';
                }
            }

            return authorName;
        }

        function doWebAddressFormatting(action, onItems, data, container) {
            var count = 0, length = 0, item = null, formattedContent = '',
                emails = [], urls = [], mediaUrls = [], nodes = null,
                texts = null, mediaNodes = null, mediaTexts = null,
                authorName = '', result = [];

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                nodes = item.querySelectorAll(action.emailSelector);
                mediaNodes = item.querySelectorAll(action.socialMediaSelector);
                texts = getAllInnerText(nodes);
                mediaTexts = getAllInnerText(mediaNodes);
                removeAllNodes(nodes);
                removeAllNodes(mediaNodes);
                if (texts.length > 0 || mediaTexts.length > 0) {
                    authorName = fetchAuthorName(item, action);

                    if (texts.length > 0) {
                        texts[texts.length - 1] += authorName;
                        emails.push(texts.join(', '));
                    }
                    if (mediaTexts.length > 0) {
                        mediaTexts[mediaTexts.length - 1] += authorName;
                        mediaUrls.push(mediaTexts.join(', '));
                    }
                }
                nodes = item.querySelectorAll(action.urlSelector);
                texts = getAllInnerText(nodes);
                removeAllNodes(nodes);
                if (texts.length > 0) {
                    urls.push.apply(urls, texts);
                }
            }
            if (emails.length > 1) {
                formattedContent = '<div class=\'email\'><span class=\'mail-label\'>Email addresses: </span>' +
                    emails.join('; ') + '</div>';
            }
            if (emails.length === 1) {
                formattedContent = '<div class=\'email\'><span class=\'mail-label\'>Email address: </span>' +
                    emails.join('; ') + '</div>';
            }
            if (urls.length > 1) {
                formattedContent += '<div class=\'url\'><span class=\'url-label\'>URLs: </span>' +
                    urls.join('; ') + '</div>';
            }
            if (urls.length === 1) {
                formattedContent += '<div class=\'url\'><span class=\'url-label\'>URL: </span>' +
                    urls.join('; ') + '</div>';
            }
            if (mediaUrls.length >= 1) {
                formattedContent += '<div class=\'social-media\'>' +
                    mediaUrls.join('; ') + '</div>';
            }
            nodes = container.querySelectorAll(action.moveIntoSelector.join(', '));
            for (count = 0; count < nodes.length; count += 1) {
                item = nodes[count];
                item.innerHTML = formattedContent;
                result.push(item);
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function getPreviousNonTextNode(node) {
            var prevNode = node.previousSibling;

            while (prevNode !== null) {
                if (prevNode.nodeType === 1) {
                    return prevNode;
                }
                prevNode = prevNode.previousSibling;
            }

            return getPreviousNonTextNode(node.parentNode);
        }

        function doRemoveHeightAction(action, onItems) {
            var count = 0, length = 0, item = null, target = null,
                height = 0, clientRect = null, previousNode = null,
                result = [];

            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                target = getParentNode(item.parentNode, action.blockTagNames);
                if (target === null) {
                    continue;
                }
                previousNode = getPreviousNonTextNode(item);
                clientRect = target.getBoundingClientRect();
                height = clientRect.bottom;
                clientRect = previousNode.getBoundingClientRect();
                height -= clientRect.bottom;
                height = Math.round(height);
                target.style.marginBottom = '-' + height + 'px';
                result.push(target);
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doTableCharacterAlignAction(
            action, onItems, data, container, callBack
        ) {
            var item = null, tableCharAligner = null,
                tables = [], alignProcess = null;

            tables.push.apply(tables, onItems);
            tableCharAligner = new TableCharAlign(win, doc);
            alignProcess = function alignProcessFn() {
                setTimeout(function timeoutFn() {
                    if (tables.length > 0) {
                        item = tables.pop();
                        tableCharAligner.align(item);
                        alignProcess();
                    }
                    else {
                        callBack();
                    }
                }, 100);
            };
            alignProcess();

            return false;
        }

        function generateLocationObject(dimensions, node) {
            var i = 0, dimension = null, locations = [],
                value = 0, topValue = 0, bottomValue = 0,
                computedStyle = null;

            computedStyle = win.getComputedStyle(node);
            topValue = computedStyle['margin-top'];
            bottomValue = computedStyle['margin-bottom'];
            value += parseFloat('0' + topValue, 0);
            value += parseFloat('0' + bottomValue, 0);
            topValue = computedStyle['padding-top'];
            bottomValue = computedStyle['padding-bottom'];
            value += parseFloat('0' + topValue, 0);
            value += parseFloat('0' + bottomValue, 0);
            for (; i < dimensions.length; i += 1) {
                dimension = dimensions[i];
                locations.push({
                    'width': dimension.width,
                    'height': dimension.height - value,
                    'location': '#' + i,
                    'position': ''
                });
            }

            return locations;
        }

        function doFigureBreakerAction(action, onItems, data, container) {
            var count = 0, i = 0, length = 0, item = null, parent = null,
                newNode = null, node = null, breaker = null, splitResult = null,
                dimensions = null, dimensionObj = null, dimension = null,
                result = [];

            dimensions = data.dimensions[action.linkid];
            node = container.querySelector(action.selector);
            newNode = doc.createElement('div');
            newNode = node.appendChild(newNode);
            breaker = new FigureBreaker(newNode);
            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                parent = item.parentNode;
                dimensionObj = generateLocationObject(dimensions, item);
                node = item.cloneNode(true);
                try {
                    splitResult = breaker.figureSplitter(
                        dimensionObj, node, action.options
                    );
                }
                catch (e) {
                    newNode.innerHTML = '';
                    continue;
                }

                if (
                    splitResult === null ||
                    typeof splitResult === 'undefined' ||
                    splitResult.dimensions === null ||
                    typeof splitResult.dimensions === 'undefined' ||
                    splitResult.nodes === null ||
                    typeof splitResult.nodes === 'undefined'
                ) {
                    continue;
                }
                for (
                    i = 0; i < splitResult.nodes.length &&
                    i < splitResult.dimensions.length; i += 1
                ) {
                    node = splitResult.nodes[i];
                    dimension = splitResult.dimensions[i];
                    if (
                        dimension.width !== null &&
                        typeof dimension.width !== 'undefined'
                    ) {
                        node.setAttribute('data-width', dimension.width);
                    }
                    if (
                        dimension.position !== null &&
                        typeof dimension.position !== 'undefined'
                    ) {
                        node.setAttribute(
                            'data-position', dimension.position
                        );
                    }
                    if (
                        i > 0 &&
                        node.hasAttribute('data-is-part-figure') === false
                    ) {
                        node.setAttribute('data-continued', true);
                    }
                    parent.insertBefore(node, item);
                    result.push(node);
                }
                parent.removeChild(item);
            }
            newNode.parentNode.removeChild(newNode);
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doTableBreakerAction(action, onItems, data, container) {
            var count = 0, i = 0, length = 0, item = null, dimensions = null,
                newNode = null, node = null, breaker = null, splitResult = null,
                parent = null, option = null, dimensionObj = null, result = [];

            dimensions = data.dimensions[action.linkid];
            node = container.querySelector(action.selector);
            newNode = doc.createElement('div');
            newNode = node.appendChild(newNode);
            breaker = new TableSplitter(newNode, win, doc);
            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                parent = item.parentNode;
                dimensionObj = generateLocationObject(dimensions, item);
                dimensionObj = {
                    'dimensions': dimensionObj,
                    'maximumFontSize': null
                };
                node = item.cloneNode(true);
                newNode.innerHTML = '';
                try {
                    splitResult = breaker.doPartition(
                        dimensionObj, node, action.options
                    );
                }
                catch (e) {
                    continue;
                }
                if (splitResult === null || typeof splitResult === 'undefined') {
                    continue;
                }
                for (i = 0; i < splitResult.length; i += 1) {
                    if (splitResult[i].length < 2) {
                        continue;
                    }
                    node = splitResult[i][0];
                    option = splitResult[i][1];
                    if (
                        option.tableWidth !== null &&
                        typeof option.tableWidth !== 'undefined'
                    ) {
                        node.setAttribute('data-width', option.tableWidth);
                    }
                    if (
                        option.fontSize !== null &&
                        typeof option.fontSize !== 'undefined'
                    ) {
                        node.setAttribute('data-font-size', option.fontSize);
                    }
                    if (i > 0) {
                        node.setAttribute('data-continued', true);
                    }
                    parent.insertBefore(node, item);
                    result.push(node);
                }
                parent.removeChild(item);
            }
            newNode.parentNode.removeChild(newNode);
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doBoxProcessorAction(action, onItems, data, container) {
            var count = 0, length = 0, item = null, node = null, result = [],
                processor = null;

            processor = new BoxProcessor(win, doc);
            length = onItems.length;
            for (; count < length; count += 1) {
                item = onItems[count];
                node = processor.execute(action, item, container);
                result.push(node);
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function proceedReorderingProcess(action, footnotes, result) {
            var i = 0, length = 0, footnoteWrapper = null,
                parent = null;

            length = footnotes.length;
            if (length > 1) {
                footnotes = Array.prototype.slice.call(footnotes);
                footnotes.sort(function sortFn(item, nextItem) {
                    var nextId = '', currId = '', status = -1;

                    currId = item.getAttribute('id');
                    currId = currId.match(/([0-9]+)/g);
                    if (currId !== null && currId.length > 0) {
                        currId = parseInt(currId[currId.length - 1], 10);
                    }
                    nextId = nextItem.getAttribute('id');
                    nextId = nextId.match(/([0-9]+)/g);
                    if (nextId !== null && nextId.length > 0) {
                        nextId = parseInt(nextId[nextId.length - 1], 10);
                    }
                    if (currId === nextId) {
                        status = 0;
                    }
                    else if (currId > nextId) {
                        status = 1;
                    }

                    return status;
                });

                for (; i < length; i += 1) {
                    footnoteWrapper = footnotes[i].parentNode;
                    parent = footnoteWrapper.parentNode;
                    parent.appendChild(footnoteWrapper);
                    result.push(footnoteWrapper);
                }
            }
        }

        function doReorderFootnotes(action, onItems) {
            var i = 0, length = onItems.length,
                item = null, footnotes = [], result = [];

            for (; i < length; i += 1) {
                item = onItems[i];
                footnotes = item.querySelectorAll(action.selector);
                proceedReorderingProcess(action, footnotes, result);
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doFreezeImagesAction(action, onItems) {
            var i = 0, length = 0, item = null,
                clientRect = null, scale = 'px';

            length = onItems.length;
            for (; i < length; i += 1) {
                item = onItems[i];
                clientRect = item.getBoundingClientRect();
                item.style.width = clientRect.width + scale;
                item.style.height = clientRect.height + scale;
            }
        }

        function isImageLoaded(img, callBack) {
            var image = null, src = '';

            if (img.hasAttribute('src') === true) {
                src = img.getAttribute('src');
            }
            image = new Image();
            function imageLoadCompleted() {
                setTimeout(callBack(img), 0);
            }
            image.onerror = imageLoadCompleted;
            image.onload = imageLoadCompleted;
            image.src = src;
        }

        function isImagesLoaded(images, callBack) {
            var image = null;

            if (images.length > 0) {
                image = images[0];
                isImageLoaded(image, function isImageLoadedCallback(img) {
                    var index = -1;

                    index = images.indexOf(img);
                    images.splice(index, 1);
                    setTimeout(function timeoutFn() {
                        isImagesLoaded(images, callBack);
                    }, 0);
                });
            }
            else {
                callBack();
            }
        }

        function doWaitImageLoadAction(
            action, onItems, data, container, callBack
        ) {
            var images = [];

            images.push.apply(images, onItems);
            isImagesLoaded(images, callBack);

            return false;
        }

        function doEquationRenderingAction(action, onItems) {
            var i = 0, item = null, img = null, clientRect = null, fontSize = 0,
                computedStyle = null, height = 0;

            for (; i < onItems.length; i += 1) {
                item = onItems[i];
                item.style.height = null;
                img = item.querySelector(action.selector.join(', '));
                if (img === null) {
                    continue;
                }
                clientRect = img.getBoundingClientRect();
                computedStyle = win.getComputedStyle(item);
                fontSize = computedStyle['font-size'];
                fontSize = parseFloat('0' + fontSize, 0);
                height = (clientRect.height / action.fontSize) * fontSize;
                img.style.height = height + 'px';
            }
        }

        function getLastPageSize(lastPage, textareaSelector) {
            var textArea = null, textAreaChilds = [], pageSize = 0,
                actualPageSize = 0, i = 0, length = 0;

            textArea = lastPage.querySelector(textareaSelector);
            actualPageSize = textArea.getBoundingClientRect().height;
            textAreaChilds = textArea.childNodes;
            length = textAreaChilds.length;
            for (; i < length; i += 1) {
                pageSize += textAreaChilds[i].getBoundingClientRect().height;
            }
            pageSize /= actualPageSize;

            return parseFloat(pageSize.toFixed(2));
        }

        function doGetPageCountAction(action, onItems, data) {
            var length = 0, pageSize = 0, item = null;

            length = onItems.length;
            if (length > 0) {
                item = onItems[length - 1];
                pageSize = getLastPageSize(item, action.textareaSelector);
                pageSize += length - 1;
            }
            data[action.onVariable] = pageSize;
            onItems.splice(0, length);
        }

        function removeNestingChilds(nodes) {
            var i = 0, j = 0, length = 0, index = 0,
                item = null, node = null, nesting = [];

            length = nodes.length;
            for (; i < length; i += 1) {
                node = nodes[i];
                for (j = 0; j < length; j += 1) {
                    item = nodes[j];
                    if (i !== j && node.contains(item) === true) {
                        nesting.push(item);
                    }
                }
            }
            while (nesting.length > 0) {
                index = nodes.indexOf(nesting[0]);
                if (index >= 0) {
                    nodes.splice(index, 1);
                }
                nesting.splice(0, 1);
            }
        }

        function getNodes(node, selector, allNodes, skipNesting) {
            var nodes = null;

            if (allNodes === true) {
                nodes = node.querySelectorAll(selector);
                nodes = Array.prototype.slice.call(nodes);
            }
            else {
                nodes = node.querySelector(selector);
                if (nodes === null || typeof nodes === 'undefined') {
                    return null;
                }
            }
            if (skipNesting === true) {
                removeNestingChilds(nodes);
            }

            return nodes;
        }

        function getBalancingNodes(
            node, parentSelector, leftSelector, rightSelector
        ) {
            var childs = null, child = null, i = 0,
                instances = [], left = null, right = null;

            childs = getNodes(node, parentSelector, true, true);
            for (; i < childs.length; i += 1) {
                child = childs[i];
                left = getNodes(
                    child, leftSelector, false
                );
                right = getNodes(
                    child, rightSelector, false
                );
                if (left === null || right === null) {
                    continue;
                }
                instances.push({
                    'left': left, 'right': right, 'parent': child
                });
            }

            return instances;
        }

        function getBalancingLeftWidth(instances) {
            var clientRect = null, computedStyle = null,
                instance = null, i = 0, leftWidth = 0, value = null;

            for (; i < instances.length; i += 1) {
                instance = instances[i];
                clientRect = instance.left.getBoundingClientRect();
                instance.leftWidth = clientRect.width;
                instance.leftHeight = clientRect.height;
                instance.left.style.width = 'auto';
                clientRect = instance.left.getBoundingClientRect();
                instance.leftActualWidth = clientRect.width;
                instance.left.style.width = null;
                clientRect = instance.right.getBoundingClientRect();
                instance.rightWidth = clientRect.width;
                if (clientRect.height > 0) {
                    computedStyle = win.getComputedStyle(instance.right);
                    value = computedStyle['line-height'];
                    value = parseFloat('0' + value, 0);
                    instance.rightHeight = value;
                }
                else {
                    instance.rightHeight = 0;
                }
                if (leftWidth < instance.leftWidth) {
                    leftWidth = instance.leftWidth;
                }
            }

            return leftWidth;
        }

        function doBalanceWidthAction(action, onItems) {
            var i = 0, j = 0, length = 0, maxWidth = 0, item = null,
                instances = null, instance = null, left = null, right = null,
                computedStyle = null, value = 0, result = [];

            length = onItems.length;
            for (; i < length; i += 1) {
                item = onItems[i];

                instances = getBalancingNodes(
                    item, action.selector.join(', '),
                    action.onLeft.join(', '), action.onRight.join(', ')
                );
                maxWidth = getBalancingLeftWidth(instances);
                for (j = 0; j < instances.length; j += 1) {
                    instance = instances[j];
                    left = instance.left;
                    left.style.minWidth = maxWidth + 'px';
                    left.style.maxWidth = maxWidth + 'px';
                    right = instance.right;
                    computedStyle = win.getComputedStyle(right);
                    value = computedStyle['margin-left'];
                    value = parseFloat('0' + value, 0);
                    right.style.marginLeft = (value + maxWidth) + 'px';
                    if (instance.rightHeight > 0) {
                        if (instance.leftHeight < instance.rightHeight) {
                            right.style.marginTop = '-' +
                                instance.leftHeight + 'px';
                        }
                        else {
                            right.style.marginTop = '-' +
                                instance.rightHeight + 'px';
                        }
                    }
                    if (instance.leftActualWidth > instance.leftWidth) {
                        left.style.width = 'auto';
                        left.style.maxWidth = null;
                        value = computedStyle['padding-left'];
                        value = parseFloat('0' + value, 0);
                        right.style.paddingLeft = (
                            value + instance.leftActualWidth -
                            instance.leftWidth
                        ) + 'px';
                    }
                    result.push(left);
                    result.push(right);
                }
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doGetMaximumWidthAction(action, onItems, data) {
            var i = 0, length = 0, maxWidth = 0, item = null,
                clientRect = null;

            length = onItems.length;
            for (; i < length; i += 1) {
                item = onItems[i];
                clientRect = item.getBoundingClientRect();
                if (maxWidth < clientRect.width) {
                    maxWidth = clientRect.width;
                }
            }
            data[action.onVariable] = maxWidth + 'px';
            onItems.splice(0, length);
        }

        function doSetInnerTextToAttributeAction(action, onItems) {
            var i = 0, length = 0, item = null, text = '';

            length = onItems.length;
            for (; i < length; i += 1) {
                item = onItems[i];
                text = extractText(item);
                item.setAttribute(action.attributeName, text);
            }
        }

        function doSetAttributeToInnerTextAction(action, onItems) {
            var i = 0, length = 0, item = null, text = '';

            length = onItems.length;
            for (; i < length; i += 1) {
                item = onItems[i];
                if (item.hasAttribute(action.attributeName) === true) {
                    text = item.getAttribute(action.attributeName);
                }
                item.innerHTML = text;
            }
        }

        function processAuthorName(name, gnName, srName) {
            var givenName = null, surName = null;

            givenName = extractTextAll(name.querySelectorAll(gnName));
            surName = extractTextAll(name.querySelectorAll(srName));
            if (givenName !== '') {
                givenName = toGenerateAuthorName(givenName, surName);

                return givenName;
            }

            return surName;
        }

        function toGenerateAuthorName(givenName, surName) {
            var gname = null;

            gname = fetchInitialFromName(givenName);
            if (surName !== '') {
                return gname + ' ' + surName;
            }

            return gname;
        }

        function doGenerateAuthorNameAction(action, onItems, data, container) {
            var i = 0, j = 0, length = 0, authorName = '', targets = null,
                target = null, result = [];

            length = onItems.length;
            if (length > action.allowableAuthors) {
                authorName = processAuthorName(
                    onItems[0], action.authorGivenNameSelector,
                    action.authorSurnameSelector
                );
                if (authorName !== '') {
                    authorName += ' et al.';
                }
            }
            else if (length < action.allowableAuthors) {
                authorName = processAuthorName(
                    onItems[0], action.authorGivenNameSelector,
                    action.authorSurnameSelector
                );
            }
            else if (length === action.allowableAuthors) {
                for (j = 0; j < length; j += 1) {
                    authorName += processAuthorName(
                        onItems[j], action.authorGivenNameSelector,
                        action.authorSurnameSelector
                    );
                    if (j !== length - 1) {
                        authorName += ', ';
                    }
                }
            }
            targets = container.querySelectorAll(action.moveIntoSelector.join(', '));
            for (i = 0; i < targets.length; i += 1) {
                target = targets[i];
                target.innerHTML = authorName;
                result.push(target);
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doMoveAsLastChildOfParentAction(action, onItems) {
            var count = 0, length = 0, clonedItem = null,
                sourceItems = [], sourceItem = null,
                parent = null, i = 0, result = [];

            length = onItems.length;
            if (length <= 0) {
                return;
            }
            for (; count < length; count += 1) {
                parent = onItems[count];
                sourceItems = parent.querySelectorAll(
                    action.selector.join(', ')
                );
                for (i = 0; i < sourceItems.length; i += 1) {
                    sourceItem = sourceItems[i];
                    clonedItem = sourceItem.cloneNode(true);
                    if (
                        clonedItem !== null && typeof clonedItem !== 'undefined'
                    ) {
                        parent.removeChild(sourceItem);
                        parent.appendChild(clonedItem);
                    }
                }
            }
            onItems.splice(0, length);
            onItems.push.apply(onItems, result);
        }

        function doConvertNumberToWordAction(action, onItems) {
            var count = 0, length = 0, target = null, textContent = '',
                replaceContent = '';

            length = onItems.length;
            if (length <= 0) {
                return;
            }
            for (; count < length; count += 1) {
                target = onItems[count];
                textContent = target.innerHTML;

                if (textContent.match(action.find) !== null) {
                    replaceContent = NumberToWord.toWords(
                        Number(textContent.match(action.find)[0])
                    );

                    textContent = textContent.replace(
                        action.find, replaceContent
                    );
                }
                target.innerHTML = textContent;
            }
        }

        function doBalanceArticleInfoWidth(action, onItems, data, container) {
            var length = 0, targetItems = null, targetLength = 0;

            length = onItems.length;
            if (length <= 0) {
                return;
            }
            targetItems = container.querySelectorAll(action.widthIndicator);
            targetLength = targetItems.length;

            if (targetLength === 0) {
                doAddClassAction(action, onItems);
            }
        }

        availableActions = {
            'applyStyle': doApplyStyleAction,
            'addClass': doAddClassAction,
            'addAttribute': doAddAttributeAction,
            'removeAttribute': doRemoveAttributeAction,
            'removeClass': doRemoveClassAction,
            'removeNode': doRemoveNodeAction,
            'removeNodeWrap': doRemoveNodeWrapAction,
            'regexReplace': doRegexReplaceAction,
            'regexAttributeReplace': doRegexAttributeReplaceAction,
            'copyNode': doCopyNodeAction,
            'copyAttribute': doCopyAttributeAction,
            'moveBefore': doMoveBeforeAction,
            'moveInto': doMoveIntoAction,
            'moveAfter': doMoveAfterAction,
            'wrapChilds': doWrapChildsAction,
            'templateBefore': doTemplateBeforeAction,
            'templateReplace': doTemplateReplaceAction,
            'templateAppend': doTemplateAppendAction,
            'templateAfter': doTemplateAfterAction,
            'fitHeight': doApplyFitHeightAction,
            'fitHeightItem': doApplyFitHeightActionItem,
            'verticalMiddle': doVerticalMiddleAction,
            'handleTableRowSpan': doHandleTableRowSpanAction,
            'counter': doCounterAction,
            'childCounter': doChildCounterAction,
            'untilContent': doUntilContentAction,
            'emptyContent': doEmptyContentAction,
            'equationPathUpdation': doEquationPathUpdation,
            'webAddressFormatting': doWebAddressFormatting,
            'removeHeight': doRemoveHeightAction,
            'tableCharacterAlign': doTableCharacterAlignAction,
            'figureBreaker': doFigureBreakerAction,
            'tableBreaker': doTableBreakerAction,
            'boxProcessor': doBoxProcessorAction,
            'reorderFootnotes': doReorderFootnotes,
            'freezeImages': doFreezeImagesAction,
            'waitImageLoad': doWaitImageLoadAction,
            'equationRendering': doEquationRenderingAction,
            'pageCount': doGetPageCountAction,
            'balanceWidth': doBalanceWidthAction,
            'maxWidth': doGetMaximumWidthAction,
            'innerTextToAttribute': doSetInnerTextToAttributeAction,
            'attributeToInnerText': doSetAttributeToInnerTextAction,
            'generateAuthorName': doGenerateAuthorNameAction,
            'moveAsLastChildOfParent': doMoveAsLastChildOfParentAction,
            'balanceArticleInfoWidth': doBalanceArticleInfoWidth,
            'convertNumberToWord': doConvertNumberToWordAction
        };

        function errorHandlingCallBack(error, message) {
            unsubscribeRuleEngineEvents();
            param.error(error, message);
        }

        function applyCondition(condition, container, result, callBack) {
            var caller = null;

            caller = availableConditions[condition.name];
            if (caller !== null || typeof caller !== 'undefined') {
                try {
                    caller(condition, container, result);
                }
                catch (e) {
                    errorHandlingCallBack(
                        e, ruleName + '-condition-name:' + condition.name
                    );
                }
            }
            callBack();
        }

        function addActionTracker(action, targets) {
            var previousActions = '', i = 0, length = 0, target = null,
                actionTrackingAttribute = 'data-trackaction';

            length = targets.length;
            for (; i < length; i += 1) {
                target = targets[i];
                previousActions = target.getAttribute(
                    actionTrackingAttribute
                );
                if (previousActions === null) {
                    target.setAttribute(
                        actionTrackingAttribute, action.id
                    );
                }
                else if (
                    previousActions.match(
                        '(^| )' + action.id + '( |$)'
                    ) === null
                ) {
                    target.setAttribute(
                        actionTrackingAttribute,
                        previousActions + ' ' + action.id
                    );
                }
            }
        }

        function applyAction(action, onItems, data, container, callBack) {
            var caller = null, complete = false;

            caller = availableActions[action.name];
            if (caller !== null || typeof caller !== 'undefined') {
                try {
                    complete = caller(
                        action, onItems, data, container, callBack
                    );
                }
                catch (e) {
                    errorHandlingCallBack(
                        e, ruleName + '-action-id:' + action.id
                    );
                }
                if (data.debugMode === true) {
                    addActionTracker(action, onItems);
                }
                if (complete !== false) {
                    callBack();
                }
            }
            else {
                callBack();
            }
        }

        function applyConditions(result, callBack) {
            var condition = null, applyConditionCallBack = null;

            if (currentCondition.length > 0) {
                condition = currentCondition[0];
                currentCondition.splice(0, 1);
                applyConditionCallBack = function callBackFn() {
                    setTimeout(function timeoutFn() {
                        eventBus.publish(
                            'paginate:' + ruleName +
                            ':ruleEngine:applyConditions',
                            result, callBack
                        );
                    }, 0);
                };
                applyCondition(
                    condition, param.source,
                    result, applyConditionCallBack
                );
            }
            else {
                callBack();
            }
        }

        function applyActionOnlyWhenItemExists(
            action, items, data, source, callBack
        ) {
            var length = items.length;

            if (length > 0) {
                applyAction(action, items, data, source, callBack);

                return;
            }

            setTimeout(function timeoutFn() {
                eventBus.publish(
                    'paginate:' + ruleName +
                    ':ruleEngine:applyRule'
                );
            }, 0);
        }

        function applyActions(onItems, callBack) {
            var action = null, applyActionCallBack = null;

            if (currentAction.length > 0) {
                action = currentAction[0];
                currentAction.splice(0, 1);
                applyActionCallBack = function callBackFn() {
                    setTimeout(function timeoutFn() {
                        eventBus.publish(
                            'paginate:' + ruleName +
                            ':ruleEngine:applyActions',
                            onItems, callBack
                        );
                    }, 0);
                };
                applyActionOnlyWhenItemExists(
                    action, onItems, param.data, param.source,
                    applyActionCallBack
                );
            }
            else {
                callBack();
            }
        }

        function init() {
            currentRules = [];
            currentRules.push.apply(currentRules, rules);
            setTimeout(function timeoutFn() {
                eventBus.publish(
                    'paginate:' + ruleName + ':ruleEngine:applyRule'
                );
            }, 0);
        }

        function applyRule() {
            var applyConditionCallBack = null, rule = null,
                itemsSatisfied = [], applyActionCallBack = null;

            if (currentRules.length > 0) {
                rule = currentRules[0];
                currentRules.splice(0, 1);
                currentCondition = [];
                currentCondition.push.apply(
                    currentCondition, rule.condition
                );
                currentAction = [];
                currentAction.push.apply(
                    currentAction, rule.action
                );
                applyActionCallBack = function callBackFn1() {
                    setTimeout(function timeoutFn() {
                        eventBus.publish(
                            'paginate:' + ruleName +
                            ':ruleEngine:applyRule'
                        );
                    }, 0);
                };
                applyConditionCallBack = function callBackFn2() {
                    applyActions(
                        itemsSatisfied, applyActionCallBack
                    );
                };
                applyConditions(
                    itemsSatisfied, applyConditionCallBack
                );
            }
            else {
                setTimeout(function timeoutFn() {
                    eventBus.publish(
                        'paginate:' + ruleName + ':ruleEngine:deinit'
                    );
                }, 0);
            }
        }

        function deinit() {
            unsubscribeRuleEngineEvents();
            param.success();
        }

        function unsubscribeRuleEngineEvents() {
            eventBus.unsubscribe(
                'paginate:' + ruleName + ':ruleEngine:init', init
            );
            eventBus.unsubscribe(
                'paginate:' + ruleName + ':ruleEngine:applyRule', applyRule
            );
            eventBus.unsubscribe(
                'paginate:' + ruleName +
                ':ruleEngine:applyConditions', applyConditions
            );
            eventBus.unsubscribe(
                'paginate:' + ruleName +
                ':ruleEngine:applyActions', applyActions
            );
            eventBus.unsubscribe(
                'paginate:' + ruleName + ':ruleEngine:deinit', deinit
            );
        }

        function subscribeRuleEngineEvents() {
            eventBus.subscribe(
                'paginate:' + ruleName + ':ruleEngine:deinit', deinit
            );
            eventBus.subscribe(
                'paginate:' + ruleName +
                ':ruleEngine:applyActions', applyActions
            );
            eventBus.subscribe(
                'paginate:' + ruleName +
                ':ruleEngine:applyConditions', applyConditions
            );
            eventBus.subscribe(
                'paginate:' + ruleName + ':ruleEngine:applyRule', applyRule
            );
            eventBus.subscribe(
                'paginate:' + ruleName + ':ruleEngine:init', init
            );
        }

        this.start = function startFn(source, data, successCallBack, errorCallBack) {
            param.source = source;
            param.data = data;
            param.success = successCallBack;
            param.error = errorCallBack;
            subscribeRuleEngineEvents();
            setTimeout(function timeoutFn() {
                eventBus.publish('paginate:' + ruleName + ':ruleEngine:init');
            }, 0);
        };
    };

    return RuleEngine;
});
