/*global define*/
define([
    'rainbow/PresetReader', 'rainbow-config/RuleManagerConfig'
], function defineFn(PresetReader, RuleManagerConfig) {
    var RuleManager = function RuleManagerClass(layoutName) {
        var ruleManagerConf = null;

        function init() {
            var presetReader = new PresetReader();

            ruleManagerConf = presetReader.getPreset(layoutName, RuleManagerConfig);
        }

        this.getRules = function getRulesFn(rules) {
            var result = [], rulesLength = 0, j = 0, rule = null;

            rulesLength = rules.length;

            for (; j < rulesLength; j += 1) {
                rule = rules[j];

                if (ruleManagerConf.requiredRules.indexOf(rule.ruleId) > -1) {
                    result.push(rule);
                }
            }

            return result;
        };

        init();
    };

    return RuleManager;
});
