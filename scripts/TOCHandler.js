/*global define*/
define([
    'rainbow/PresetReader', 'rainbow-config/TOCConfig'
], function defineFn(PresetReader, TOCConfig) {
    var TOCHandler = function TOCHandlerClass(doc, layoutName) {
        var tocterms = null, config = null;

        function init() {
            var presetReader = new PresetReader();

            config = presetReader.getPreset(layoutName, TOCConfig);
        }

        function filterNonExistItems(givenItems, items) {
            var i = 0, node = null, actualElements = [];

            for (; i < items.length; i += 1) {
                node = items[i];
                if (givenItems.indexOf(node) >= 0) {
                    continue;
                }
                actualElements.push(node);
            }

            return actualElements;
        }

        function filterValidElements(container, elements) {
            var nodes = null, excludeElements = [];

            if (config.excludeSelectors.length > 0) {
                nodes = container.querySelectorAll(
                    config.excludeSelectors.join(',')
                );
                excludeElements.push.apply(excludeElements, nodes);
            }

            return filterNonExistItems(excludeElements, elements);
        }

        function getLevelCount() {
            var propkey = null, levelCount = 0;

            for (propkey in config.levels) {
                if (config.levels.hasOwnProperty(propkey) === false) {
                    continue;
                }
                levelCount += 1;
            }

            return levelCount;
        }

        function getLevelElements(levelCount, container) {
            var levels = {}, current = null, i = 0, elements = null,
                actualElements = [];

            for (i = levelCount - 1; i >= 0; i -= 1) {
                current = [];
                levels[i] = current;
                elements = container.querySelectorAll(config.levels[i].join(','));
                elements = filterValidElements(container, elements);
                elements = filterNonExistItems(actualElements, elements);
                current.push.apply(current, elements);
                actualElements.push.apply(actualElements, elements);
            }

            return levels;
        }

        function getOrderedElements(levelCount, container) {
            var i = 0, selectors = [], elements = null;

            for (;i < levelCount; i += 1) {
                selectors.push.apply(selectors, config.levels[i]);
            }
            if (selectors.length > 0) {
                elements = container.querySelectorAll(selectors.join(','));
                elements = filterValidElements(container, elements);
            }

            return elements;
        }

        function getLevel(levelCount, levelWiseElements, element) {
            var i = 0;

            for (;i < levelCount; i += 1) {
                if (levelWiseElements[i].indexOf(element) >= 0) {
                    return i;
                }
            }

            return -1;
        }

        function getLabel(element) {
            var node = null;

            node = element.querySelector(config.labelSelectors.join(','));
            if (node !== null) {
                return node.innerHTML;
            }

            return '';
        }

        function getTitle(element) {
            var node = null;

            node = element.querySelector(config.titleSelectors.join(','));
            if (node !== null) {
                return node.innerHTML;
            }

            return '';
        }

        function getOrderedElementsWithLevel(
            levelCount, levelWiseElements, orderedElements
        ) {
            var i = 0, result = [], node = null, level = 0, label = '', title = '';

            for (; orderedElements !== null && i < orderedElements.length; i += 1) {
                node = orderedElements[i];
                level = getLevel(levelCount, levelWiseElements, node);
                label = getLabel(node);
                title = getTitle(node);
                if (level < 0 || title === '') {
                    continue;
                }
                result.push({
                    'levelTarget': node,
                    'level': level,
                    'label': label,
                    'title': title,
                    'index': i,
                    'position': 0
                });
            }

            return result;
        }

        function generateTOCContent(leveledElements) {
            var i = 0, result = '', element = null,
                sepLocationColumn = false;

            if (config.separateLocationColumn === true) {
                sepLocationColumn = true;
            }
            result += '<table><tbody>';
            for (; i < leveledElements.length; i += 1) {
                element = leveledElements[i];
                element.levelTarget.setAttribute('data-toc-dest', i);
                if (sepLocationColumn === true) {
                    result += '<tr data-toc-level="' + element.level + '">' +
                        '<td class="toc-term">' +
                        '<span class="toc-label">' + element.label + '</span>' +
                        '<span class="toc-desc">' + element.title + '</span>' +
                        '</td><td class="toc-location" data-toc-source="' + i +
                        '">XXX</td></tr>';
                }
                else {
                    result += '<tr data-toc-level="' + element.level + '">' +
                        '<td class="toc-term">' +
                        '<span class="toc-label">' + element.label + '</span>' +
                        '<span class="toc-desc">' + element.title +
                        '<span class="toc-location" data-toc-source="' + i +
                        '">XXX</span></span></td></tr>';
                }
            }
            result += '</tbody></table>';
            return result;
        }

        function renderTOCContent(tocContent, container) {
            var dests = null, dest = null, i = 0, parent = null, next = null,
                newNode = null, targets = [];

            if (
                config.destinationSelectors === null ||
                typeof config.destinationSelectors === 'undefined'
            ) {
                return;
            }
            dests = container.querySelectorAll(
                config.destinationSelectors.join(',')
            );
            for (; i < dests.length; i += 1) {
                dest = dests[i];
                parent = dest.parentNode;
                next = dest.nextSibling;
                newNode = doc.createElement('div');
                newNode.innerHTML = config.template.join('');
                targets.push.apply(
                    targets,
                    newNode.querySelectorAll(config.templateSelectors.join(','))
                );
                if (next === null) {
                    parent.appendChild(newNode);
                }
                else {
                    parent.insertBefore(newNode, next);
                }
            }
            for (i = 0; i < targets.length; i += 1) {
                dest = targets[i];
                dest.innerHTML = tocContent;
            }
        }

        function getLocations(container) {
            var elements = null;

            elements = container.querySelectorAll(
                config.locationSelectors.join(',')
            );

            return elements;
        }

        function getPosition(index, locations) {
            var i = 0, location = null, element;

            for (; i < locations.length; i += 1) {
                location = locations[i];
                element = location.querySelector('*[data-toc-dest="' + index + '"]');
                if (element !== null) {
                    return i + 1;
                }
            }

            return 0;
        }

        function getAllPositions(container, leveledElements) {
            var locations = null, i = 0, element = null;

            locations = getLocations(container);
            for (; i < leveledElements.length; i += 1) {
                element = leveledElements[i];
                element.position = getPosition(i, locations);
            }
        }

        function getParentNode(name, node) {
            if (node.tagName === "BODY") {
                return null;
            } else if (node.tagName === name) {
                return node;
            } else {
                return getParentNode(name, node.parentNode);
            }
        }

        function swapElements(firstElement, secondElement) {
            var parent = null, nextParent = null, nextSib = null;

            parent = firstElement.parentNode;
            nextSib = firstElement.nextSibling;
            nextParent = secondElement.parentNode;
            if (nextParent.lastChild === secondElement) {
                nextParent.appendChild(firstElement);
            }
            else {
                nextParent.insertBefore(firstElement, secondElement.nextSibling);
            }
            if (parent !== nextParent) {
                if (nextSib === null) {
                    parent.appendChild(secondElement);
                }
                else {
                    parent.insertBefore(secondElement, nextSib);
                }
            }
        }

        function reorderAllPositions(container, leveledElements) {
            var i = 0, element = null, nextElement = null, node = null,
                nextNode = null;

            for (; i < leveledElements.length - 1; i += 1) {
                element = leveledElements[i];
                nextElement = leveledElements[i + 1];
                if (element.position > nextElement.position) {
                    leveledElements[i] = nextElement;
                    leveledElements[i + 1] = element;
                    node = container.querySelector(
                        '*[data-toc-source="' + element.index + '"]');
                    node = getParentNode("TR", node);
                    nextNode = container.querySelector(
                        '*[data-toc-source="' + nextElement.index + '"]');
                    nextNode = getParentNode("TR", nextNode);
                    if (node === null || nextNode === null) {
                        continue;
                    }
                    swapElements(node, nextNode);
                }
            }
        }

        function renderAllPositions(container, leveledElements) {
            var i = 0, element = null, node = null;

            for (; i < leveledElements.length; i += 1) {
                element = leveledElements[i];
                node = container.querySelector('*[data-toc-source="' + i + '"]');
                node.innerHTML = element.position;
            }
        }

        this.locateTerms = function locateTermsFn(container) {
            if (tocterms === null || tocterms.length === 0) {
                return;
            }
            getAllPositions(container, tocterms);
            renderAllPositions(container, tocterms);
            reorderAllPositions(container, tocterms);
        };

        this.generate = function generateFn(container) {
            var levelWiseElements = {}, orderedElements = null, levelCount = 0,
                leveledElements = null, tocContent = '';

            levelCount = getLevelCount();
            levelWiseElements = getLevelElements(levelCount, container);
            orderedElements = getOrderedElements(levelCount, container);
            leveledElements = getOrderedElementsWithLevel(
                levelCount, levelWiseElements, orderedElements
            );
            tocContent = generateTOCContent(leveledElements);
            renderTOCContent(tocContent, container);
            tocterms = leveledElements;
        };

        init();
    };

    return TOCHandler;
});
