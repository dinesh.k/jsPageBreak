/*global define, document, HTMLElement*/
define(['phoenix/Helper'], function tableSplitter(Helper) {
    function initializeVariables(instance) {
        instance.win = null;
        instance.doc = null;
        instance.tblTarget = null;
        instance.location = null;
        instance.pageHeight = null;
        instance.tableWidth = null;
        instance.tableHeight = null;
        instance.givenWidth = null;
        instance.givenHeight = null;
        instance.fontSize = null;
        instance.firstBreak = false;
        instance.tableWrapper = null;
        instance.tableContinued = null;
        instance.captionContinued = null;
        instance.thead = null;
        instance.tables = [];
        instance.wideTable = false;
        instance.tHeads = false;
    }

    function TableSplitter(target, win, doc) {
        if (Helper.isNull(target) === true) {
            throw new Error('tablesplitter.provide.target');
        }
        initializeVariables(this);
        this.tblTarget = target;
        this.win = win;
        this.doc = doc;
    }

    TableSplitter.prototype.setupTable = function setupTable(table) {
        var instance = this, clonedTable = null;

        instance.tblTarget.appendChild(table.cloneNode(true));
        clonedTable = instance.tblTarget.firstElementChild;

        return clonedTable;
    };

    TableSplitter.prototype.processRowSpan = function processRowSpan(table) {
        var cells = null, i = 0, j = 0, k = 0, cell = null,
            position = 0, rowSpan = 0, row = null, nextRow = null,
            colSpan = 0, innerTable = null, innerTables = [],
            cellsLength = 0, tablesLength = 0;

        innerTables = table.querySelectorAll('table');
        tablesLength = innerTables.length;

        for (;i < tablesLength; i += 1) {
            innerTable = innerTables[i];
            cells = this.getRowSpanNodes(innerTable);
            cellsLength = cells.length;

            for (;j < cellsLength; j += 1) {
                k = 1;
                cell = cells[j];
                rowSpan = cell.rowSpan;

                if (cell.hasAttribute('colspan')) {
                    colSpan = cell.colSpan;
                }

                position = cell.cellIndex;
                row = cell.parentElement;

                for (; k < rowSpan; k += 1) {
                    nextRow = row.nextSibling;
                    this.doInsertCell(nextRow, position, colSpan);
                    row = nextRow;
                }

                colSpan = 0;
                cell.setAttribute('data-rowspan', rowSpan);
                cell.removeAttribute('rowspan');
            }
        }
    };

    TableSplitter.prototype.getRowSpanNodes = function getRowSpanNodes(table) {
        var rows = [], rowSpanCells = [], cells = [],
            i = 0, j = 0, cell = null, rowsLength = 0,
            cellsLength = 0;

        this.freezeTableColumnWidth(table);
        rows = table.rows;
        rowsLength = rows.length;

        for (; i < rowsLength; i += 1) {
            j = 0;
            cellsLength = 0;
            cells = rows[i].cells;
            cellsLength = cells.length;

            for (; j < cellsLength; j += 1) {
                cell = cells[j];

                if (cell.hasAttribute('rowspan')) {
                    rowSpanCells.push(cell);
                }
            }
        }

        return rowSpanCells;
    };

    TableSplitter.prototype.getNodes = function getNodes(selector, obj) {
        var doc = this.doc;

        if (Helper.isEmptyString(selector) === true) {
            throw new Error('tablesplitter.selector.is.empty');
        }

        if (obj instanceof HTMLElement) {
            doc = obj;
        }

        return doc.querySelectorAll(selector);
    };

    TableSplitter.prototype.getAttrIntCount = function getAttrIntCount(attr, obj) {
        if (Helper.isNull(obj) === true ||
                Helper.isObject(obj) === false) {
            throw new Error('tablesplitter.node.is.not.an.object');
        }

        return parseInt(obj.getAttribute(attr), 10);
    };

    TableSplitter.prototype.doInsertCell = function doInsertCell(row, position, colSpan) {
        var cellPosition = 0, targetCell = null, newCell = null, prevCell = false, difference = 0;

        if (position > 0) {
            position -= 1;
            prevCell = true;
        }

        newCell = this.doc.createElement('td');

        if (colSpan > 0) {
            newCell.colSpan = colSpan;
        }

        if (Helper.isUndefined(row.cells[position]) === false) {
            targetCell = row.cells[position];
            cellPosition = targetCell.cellIndex;

            if (position !== 0 && position !== cellPosition) {
                difference = cellPosition - position;
                position -= difference;
                targetCell = row.cells[position];
            }

            if (prevCell === false) {
                this.doInsertCellBefore(newCell, targetCell, row);
            }
            else {
                this.doInsertCellAfter(newCell, targetCell, row);
            }
        }
        else {
            row.appendChild(newCell);
        }
    };

    TableSplitter.prototype.doInsertCellBefore = function doInsertCellBefore(toBeInserted, target, parent) {
        parent.insertBefore(toBeInserted, target);
    };

    TableSplitter.prototype.doInsertCellAfter = function doInsertCellAfter(toBeInserted, target, parent) {
        if (parent.lastChild === target) {
            target.appendChild(toBeInserted);
        }

        if (toBeInserted === target.nextSibling) {
            return;
        }

        parent.insertBefore(toBeInserted, target.nextSibling);
    };

    TableSplitter.prototype.decidePlacement = function decidePlacement(dimensionsObj, table, target) {
        var width = null, location = null, length = 0, j = 0,
            pageHeight = null, instance = this, scale = 'px';

        length = dimensionsObj.dimensions.length;

        for (; j < length; j += 1) {
            width = dimensionsObj.dimensions[j].width;
            pageHeight = dimensionsObj.dimensions[j].height;
            location = dimensionsObj.dimensions[j].location;
            target.style.width = (width * 95 / 100) + scale;
            instance.wideTable = this.isWidthExceeds(table, width);
            target.style.width = width + scale;
            if (instance.wideTable === false) {
                instance.location = location;
                instance.pageHeight = pageHeight;
                instance.tableWidth = this.getDimension(table, 'width');
                instance.tableHeight = this.getDimension(table, 'height');
                instance.givenWidth = width;
                instance.givenHeight = pageHeight;

                return location;
            }

            if (j === (length - 1) && instance.wideTable === true) {
                instance.location = location;
                instance.pageHeight = pageHeight;
                instance.tableWidth = this.getDimension(table, 'width');
                instance.tableHeight = this.getDimension(table, 'height');
                instance.givenWidth = width;
                instance.givenHeight = pageHeight;
                instance.maximumFontSize = dimensionsObj.maximumFontSize;
                if (Helper.isNull(instance.maximumFontSize) !== true) {
                    this.reduceFontSize(table, target);
                }

                return location;
            }
        }

        return location;
    };
    TableSplitter.prototype.isWidthExceeds = function isWidthExceeds(table, width) {
        var tableWidth = null;

        tableWidth = Math.round(table.scrollWidth);
        if (tableWidth > width) {
            return true;
        }

        return false;
    };

    TableSplitter.prototype.reduceFontSize = function reduceFontSize(table, target) {
        var maximumFontSize = null, tableFontSize = null, innerTable = null,
            scale = 'px', instance = this, wideTable = instance.wideTable,
            selectors = 'div', div = null, divs = null, divsLength = 0, j = 0;

        maximumFontSize = instance.maximumFontSize;
        tableFontSize = parseFloat(this.win.getComputedStyle(table).fontSize);
        innerTable = table.querySelector('table');
        divs = table.querySelectorAll(selectors);
        tableFontSize -= 0.5;

        while (wideTable === true && tableFontSize >= maximumFontSize) {
            j = 0;
            table.style.fontSize = tableFontSize + scale;
            innerTable.style.fontSize = tableFontSize + scale;

            if (Helper.isNull(divs) !== true) {
                divsLength = divs.length;
                for (; j < divsLength; j += 1) {
                    div = divs[j];
                    div.style.fontSize = tableFontSize + scale;
                }
            }
            instance.wideTable = this.isWidthExceeds(table, width);
            this.applyCellFontSize(innerTable, tableFontSize + scale);
            tableFontSize -= 0.5;
        }

        if (instance.wideTable === false) {
            instance.fontSize = tableFontSize + scale;

            return false;
        }

        instance.fontSize = tableFontSize + scale;

        return true;
    };

    TableSplitter.prototype.applyCellFontSize = function applyCellFontSize(table, fontSize) {
        var rows = [], cells = [], cell = null,
            j = 0, k = 0, rowsLength = 0, cellsLength = 0;

        rows = table.rows;
        rowsLength = rows.length;

        for (; j < rowsLength; j += 1) {
            k = 0;
            cellsLength = 0;
            cells = rows[j].cells;
            cellsLength = cells.length;

            for (; k < cellsLength; k += 1) {
                cell = cells[k];
                cell.style.fontSize = fontSize;
            }
        }
    };

    TableSplitter.prototype.isHeightExceedsPage = function isHeightExceedsPage(instance) {
        var pageHeight = instance.pageHeight,
            tableHeight = instance.tableHeight;

        if (tableHeight > pageHeight) {
            return true;
        }

        return false;
    };

    TableSplitter.prototype.processBreakers = function processBreakers(table, target, selectorsObj) {
        var breakers = null, breakerSelectors = null, breaker = null, length = 0,
            i = 0, instance = this, nextBreaker = null, pageHeight = instance.pageHeight,
            footerSelectors = selectorsObj.selectors.footer;

        pageHeight = instance.pageHeight;
        breakerSelectors = 'thead, tbody > tr, ' + footerSelectors;
        breakers = this.getNodes(breakerSelectors, table);
        length = breakers.length;

        for (; i < length; i += 1) {
            breaker = breakers[i];
            nextBreaker = breakers[i + 1];

            this.updateHeader(breaker);

            if (this.canBreakTheRow(table, breaker, nextBreaker, pageHeight) === true) {
                if (instance.firstBreak === true) {
                    this.createWrapper(breaker, table);

                    if (breaker.nodeName.toLowerCase() === 'tr') {
                        this.createTable(breaker, table, selectorsObj);
                    }

                    instance.firstBreak = false;
                }

                this.detachTable(table, breaker, target, selectorsObj);

                return;
            }

            if ((i === length - 1) ||
                (breaker.nodeName.toLowerCase() !== 'tr' &&
                 breaker.nodeName.toLowerCase() !== 'thead')
            ) {
                this.addTableArray(table);

                return;
            }
        }
    };

    TableSplitter.prototype.updateHeader = function updateHeader(breaker) {
        var instance = this;

        if (instance.tHeads === true && instance.firstBreak === false &&
            breaker.nodeName.toLowerCase() === 'thead') {
            instance.tableContinued.tHead.innerHTML = breaker.cloneNode('true').innerHTML;
        }
    };

    TableSplitter.prototype.detachTable = function detachTable(table, breaker, target, selectorsObj) {
        var newTable = null;

        newTable = this.addNewTable(breaker, table, target);
        this.addTableArray(table);
        this.processBreakers(newTable, target, selectorsObj);
    };

    TableSplitter.prototype.addNewTable = function addNewTable(breaker, table, target) {
        var followingRows = [], footerElements = [], footerSelector = null, tbody = null,
            newTable = null, innerTable = null,
            instance = this;

        target.appendChild(instance.tableWrapper.cloneNode(true));
        newTable = target.lastElementChild;
        newTable.style.fontSize = instance.fontSize;
        footerSelector = breaker;

        if (breaker.nodeName.toLowerCase() === 'thead') {
            footerSelector = breaker.parentElement.previousElementSibling;
        }
        else if (breaker.nodeName.toLowerCase() === 'tr') {
            newTable.appendChild(instance.tableContinued.cloneNode(true));
            tbody = newTable.querySelector('tbody');
            followingRows = this.getSiblings(breaker);
            footerSelector = breaker.parentElement.parentElement;
            tbody.appendChild(breaker);
            this.addOver(tbody, followingRows);
            footerElements = this.getSiblings(footerSelector);
            innerTable = newTable.querySelector('table');
            innerTable.style.fontSize = instance.fontSize;
        }
        else {
            footerElements = this.getSiblings(footerSelector);
            newTable.appendChild(breaker);
        }

        if (footerElements.length !== 0) {
            this.addOver(newTable, footerElements);
        }

        return newTable;
    };

    TableSplitter.prototype.addTableArray = function addTableArray(table) {
        var dimension = null, instance = this;

        dimension = this.createDimension();
        instance.tables.push([table.cloneNode(true), dimension]);
        table.parentNode.removeChild(table);
    };

    TableSplitter.prototype.iterateCells = function iterateCells(table) {
        var i = 0, innerTables = [], innerTable = null,
            tablesLength = 0;

        innerTables = table.querySelectorAll('table');
        tablesLength = innerTables.length;

        for (;i < tablesLength; i += 1) {
            innerTable = innerTables[i];
            this.freezeTableColumnWidth(innerTable);
        }
    };

    TableSplitter.prototype.freezeTableColumnWidth = function freezeTableColumnWidth(table) {
        var rows = [], cells = [], cell = null, tableWidth = 0,
            i = 0, j = 0, k = 0, rowsLength = 0, cellsLength = 0,
            widths = [], width = 0, scale = 'px';

        tableWidth = table.getBoundingClientRect().width;
        rows = table.rows;
        rowsLength = rows.length;

        for (; j < rowsLength; j += 1) {
            k = 0;
            cellsLength = 0;
            cells = rows[j].cells;
            cellsLength = cells.length;

            for (; k < cellsLength; k += 1) {
                cell = cells[k];
                widths.push(cell.getBoundingClientRect().width);
            }
        }
        table.style.width = tableWidth + scale;
        for (j = 0; j < rowsLength; j += 1) {
            k = 0;
            cellsLength = 0;
            cells = rows[j].cells;
            cellsLength = cells.length;

            for (; k < cellsLength; k += 1) {
                cell = cells[k];
                width = widths[i];
                i += 1;
                cell.style.width = width + scale;
            }
        }
    };

    TableSplitter.prototype.createWrapper = function createWrapper(breaker, table) {
        var instance = this;

        instance.tableWrapper = table.cloneNode(false);
    };

    TableSplitter.prototype.createTable = function createTable(breaker, table, selectorsObj) {
        var continuedTable = null, continuedTbody = null, instance = this;

        this.addCaption(breaker, table, selectorsObj);
        instance.tableWrapper.appendChild(instance.captionContinued);
        this.addHeader(breaker, table);
        continuedTable = table.querySelector('table');
        if (continuedTable !== null) {
            continuedTable = continuedTable.cloneNode(false);
        }
        else {
            continuedTable = document.createElement('table');
        }
        continuedTable.appendChild(instance.thead);
        continuedTbody = document.createElement('tbody');
        continuedTable.appendChild(continuedTbody);
        instance.tableContinued = continuedTable;
    };

    TableSplitter.prototype.addCaption = function addCaption(breaker, table, selectorsObj) {
        var captionParent = null, caption = null, captionClass, instance = this;

        captionClass = selectorsObj.selectors.caption;
        caption = table.querySelector(captionClass);
        captionParent = caption.parentElement;
        captionParent = captionParent.cloneNode(true);
        caption = captionParent.querySelector(captionClass);
        caption.innerHTML = 'Continued';
        instance.captionContinued = captionParent;
    };

    TableSplitter.prototype.addHeader = function addHeader(breaker, table) {
        var instance = this, length = 0,
            thead = breaker.parentElement.previousElementSibling;

        instance.thead = thead.cloneNode(true);
        if (instance.firstBreak === true) {
            length = table.querySelectorAll('thead').length;
            if (length > 1) {
                instance.tHeads = true;
            }
        }
    };

    TableSplitter.prototype.canBreakTheRow = function canBreakTheRow(table, breaker, nextBreaker, pageHeight) {
        var tableTop = null, breakerTop = null, tableHeight = null,
            breakerHeight = null, breakerBottom = null, nextBreakerTop = null,
            nextBreakerHeight = null, nextBreakerBottom = null;

        tableTop = this.getDimension(table, 'top');
        breakerTop = this.getDimension(breaker, 'top');
        breakerHeight = this.getDimension(breaker, 'height');
        breakerBottom = breakerTop + breakerHeight;
        tableHeight = breakerBottom - tableTop;

        if (breaker.nodeName.toLowerCase() === 'thead') {
            nextBreakerTop = this.getDimension(nextBreaker, 'top');
            nextBreakerHeight = this.getDimension(nextBreaker, 'height');
            nextBreakerBottom = nextBreakerTop + nextBreakerHeight;
            tableHeight = nextBreakerBottom - tableTop;
        }

        if (tableHeight > pageHeight) {
            return true;
        }

        return false;
    };

    TableSplitter.prototype.getSiblings = function getSiblings(element) {
        var followingElements = [], next = null;

        next = element.nextElementSibling || element.nextSibling;
        while (next && next.nodeType === 1) {
            followingElements.push(next);
            next = next.nextElementSibling || next.nextSibling;
        }

        return followingElements;
    };

    TableSplitter.prototype.addOver = function addOver(target, children) {
        var i = 0, length = children.length;

        for (; i < length; i += 1) {
            target.appendChild(children[i]);
        }
    };

    TableSplitter.prototype.createDimension = function createDimension() {
        var dimension = {}, instance = this;

        dimension.tableWidth = instance.givenWidth;
        dimension.tableHeight = instance.givenHeight;
        dimension.location = instance.location;
        dimension.fontSize = instance.fontSize;

        return dimension;
    };

    TableSplitter.prototype.getDimension = function getDimension(node, dimension) {
        return Math.round(node.getBoundingClientRect()[dimension]);
    };

    TableSplitter.prototype.doPartition = function doPartition(dimensionsObj, table, selectorsObj) {
        var instance = this;

        if (Helper.isNull(dimensionsObj) === true) {
            throw new Error('tablesplitter.provide.dimensions');
        }

        if (Helper.isNull(table) === true) {
            throw new Error('tablesplitter.provide.table');
        }

        if (Helper.isNull(selectorsObj) === true) {
            throw new Error('tablesplitter.provide.selectors');
        }
        table = this.setupTable(table);
        this.decidePlacement(dimensionsObj, table, instance.tblTarget);
        instance.tables = [];

        if (this.isHeightExceedsPage(this) === true) {
            this.processRowSpan(table);
            instance.firstBreak = true;
            this.processBreakers(table, instance.tblTarget, selectorsObj);
        }
        else {
            this.iterateCells(table);
            this.addTableArray(table);
        }

        return instance.tables;
    };

    return TableSplitter;
});
