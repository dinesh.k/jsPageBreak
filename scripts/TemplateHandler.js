/*global define*/
define([
    'rainbow/PresetReader', 'rainbow-config/TemplateConfig'
], function defineFn(PresetReader, TemplateConfig) {
    var TemplateHandler = function TemplateHandlerClass(
        doc, source, destination, layoutHandler
    ) {
        var pages = null, self = this;

        function getPageItemsOfColumns(colGrp) {
            var i = 0, pageItems = null, column = null, result = [];

            for (; i < colGrp.columns.length; i += 1) {
                column = colGrp.columns[i];
                pageItems = column.pageItems;
                result.push.apply(result, pageItems);
            }

            return result;
        }

        function getColumnOfPageItem(colGrp, pageItem) {
            var i = 0, pageItems = null, column = null;

            for (; i < colGrp.columns.length; i += 1) {
                column = colGrp.columns[i];
                pageItems = column.pageItems;
                if (pageItems.indexOf(pageItem) >= 0) {
                    return column;
                }
            }

            return null;
        }

        function getColumnTopPosition(colGrp, curPgeItem) {
            var i = 0, existColumn = null, index = -1, result = 0;

            existColumn = getColumnOfPageItem(colGrp, curPgeItem);
            if (existColumn !== null) {
                index = existColumn.pageItems.indexOf(curPgeItem);
                for (i = 0; i < index; i += 1) {
                    result += existColumn.pageItems[i].height;
                }
            }

            return result;
        }

        function getColumnBottomPosition(colGrp, curPgeItem) {
            var i = 0, existColumn = null,
                index = -1, length = 0, result = 0;

            existColumn = getColumnOfPageItem(colGrp, curPgeItem);
            if (existColumn !== null) {
                index = existColumn.pageItems.indexOf(curPgeItem);
                length = existColumn.pageItems.length;
                for (i = index + 1; i < length; i += 1) {
                    result += existColumn.pageItems[i].height;
                }
            }

            return result;
        }

        function getColumnFillHeight(column) {
            var i = 0, pageItems = column.pageItems, height = 0;

            for (; i < pageItems.length; i += 1) {
                height += pageItems[i].height;
            }

            return height;
        }

        function getColumnsFillHeight(colGrp) {
            var i = 0, column = null, height = 0, result = 0;

            for (; i < colGrp.columns.length; i += 1) {
                column = colGrp.columns[i];
                height = getColumnFillHeight(column);
                if (height > result) {
                    result = height;
                }
            }

            return result;
        }

        function getTextareaFillHeight(page) {
            var i = 0, textarea = null, colGrp = null,
                items = null, item = null, height = 0;

            textarea = page.textarea;
            items = textarea.pageItems;
            for (; i < items.length; i += 1) {
                item = items[i];
                if (textarea.columnPageItems.indexOf(item) < 0) {
                    height += item.height;
                }
            }
            for (i = 0; i < textarea.columnGroups.length; i += 1) {
                colGrp = textarea.columnGroups[i];
                height += getColumnsFillHeight(colGrp);
            }

            return height;
        }

        function getTextareaTopPosition(page, curPgeItem) {
            var i = 0, textarea = null, position = -1, index = 0,
                colGrp = null, items = null, item = null, height = 0;

            textarea = page.textarea;
            items = textarea.pageItems;
            position = items.indexOf(curPgeItem);
            for (; i < position; i += 1) {
                item = items[i];
                if (textarea.columnPageItems.indexOf(item) < 0) {
                    height += item.height;
                    index += 1;
                }
            }
            for (i = 0; i < textarea.columnGroups.length; i += 1) {
                colGrp = textarea.columnGroups[i];
                if (getColumnOfPageItem(colGrp, curPgeItem) !== null) {
                    height += getColumnTopPosition(colGrp, curPgeItem);
                    break;
                }
                else if (index < position) {
                    height += getColumnsFillHeight(colGrp);
                    index += getPageItemsOfColumns(colGrp).length;
                }
            }

            return height;
        }

        function getTextareaBottomPosition(page, curPgeItem) {
            var i = 0, textarea = null, position = -1, colGrp = null,
                index = 0, length = 0, items = null, item = null, height = 0;

            textarea = page.textarea;
            items = textarea.pageItems;
            position = items.indexOf(curPgeItem);
            length = items.length;
            index = length - position - 1;
            for (i = position + 1; i < length; i += 1) {
                item = items[i];
                if (textarea.columnPageItems.indexOf(item) < 0) {
                    height += item.height;
                    index -= 1;
                }
            }
            length = textarea.columnGroups.length;
            for (i = length - 1; i >= 0; i -= 1) {
                colGrp = textarea.columnGroups[i];
                if (getColumnOfPageItem(colGrp, curPgeItem) !== null) {
                    height += getColumnBottomPosition(colGrp, curPgeItem);
                    break;
                }
                else if (index > 0) {
                    height += getColumnsFillHeight(colGrp);
                    index -= getPageItemsOfColumns(colGrp).length;
                }
            }

            return height;
        }

        function hasRunningPageItem(pageItems) {
            var i = 0;

            for (; i < pageItems.length; i += 1) {
                if (self.isRunningItem(pageItems[i]) === true) {
                    return true;
                }
            }

            return false;
        }

        function isBottomDynamicPageItem(page, curPgeItem) {
            var i = 0, textarea = null, position = -1, colGrp = null,
                index = 0, length = 0, column = null, pageItems = null;

            if (self.isDynamicItem(curPgeItem) === false) {
                return false;
            }
            textarea = page.textarea;
            length = textarea.columnGroups.length;
            for (i = length - 1; i >= 0; i -= 1) {
                colGrp = textarea.columnGroups[i];
                column = getColumnOfPageItem(colGrp, curPgeItem);
                if (column !== null) {
                    pageItems = column.pageItems;
                    index = pageItems.indexOf(curPgeItem);
                    pageItems = pageItems.slice(index + 1);
                    return !(hasRunningPageItem(pageItems));
                }
            }
            pageItems = textarea.pageItems;
            position = pageItems.indexOf(curPgeItem);
            pageItems = pageItems.slice(position + 1);

            return !(hasRunningPageItem(pageItems));
        }

        function getGeomentricBound(page, curPgeItem) {
            var result = {'top': 0, 'bottom': 0, 'height': 0, 'width': 0};

            if (curPgeItem !== null && typeof curPgeItem !== 'undefined') {
                result.height = curPgeItem.height;
                result.width = curPgeItem.width;
                if (page.textarea.pageItems.indexOf(curPgeItem) >= 0) {
                    result.top = getTextareaTopPosition(page, curPgeItem);
                    result.bottom = getTextareaBottomPosition(page, curPgeItem);
                }
            }
            else {
                result.height = getTextareaFillHeight(page);
                result.width = page.textarea.maxWidth;
                result.bottom = page.textarea.maxHeight - result.height;
            }

            return result;
        }

        function hasEqualColumnWidths(columns) {
            var widths = [], width = 0, i = 0, j = 0, column = null,
                pageItem = null;

            for (; i < columns.length; i += 1) {
                column = columns[i];
                for (j = 0; j < column.pageItems.length; j += 1) {
                    pageItem = column.pageItems[j];
                    width = Math.round(self.getPageItemWidth(pageItem));
                    if (widths.indexOf(width) < 0) {
                        widths.push(width);
                    }
                }
            }
            if (widths.length === 1) {
                return true;
            }

            return false;
        }

        function isDynamicItemWidthExist(page, dynamicItemWidth) {
            var pageItems = null, pageItem = null, i = 0, width = 0;

            pageItems = page.pageItems;
            for (; i < pageItems.length; i += 1) {
                pageItem = pageItems[i];
                if (self.isDynamicItem(pageItem) === true) {
                    width = self.getPageItemWidth(pageItem);
                    if (Math.round(width) === Math.round(dynamicItemWidth)) {
                        return true;
                    }
                }
            }

            return false;
        }

        function getItemOfType(items, type) {
            var i = 0, item = null, result = [];

            for (; i < items.length; i += 1) {
                item = items[i];
                if (item.type === type) {
                    result.push(item);
                }
            }

            return result;
        }

        function generatePageItemObject(pageItemTemplate, pageItemType) {
            var pageItem = {}, clientRect = null;

            pageItem.htmlElement = pageItemTemplate;
            clientRect = pageItemTemplate.getBoundingClientRect();
            pageItem.type = pageItemType;
            pageItem.lineType = layoutHandler.getPageItemLine(pageItemTemplate);
            pageItem.lineMinLimit = layoutHandler.getPageItemLineMinLimit(
                pageItemTemplate
            );
            pageItem.contentStart = layoutHandler.isPageItemContentStart(
                pageItemTemplate
            );
            pageItem.height = clientRect.height;
            pageItem.width = clientRect.width;
            pageItem.links = layoutHandler.getPageItemLink(pageItemTemplate);

            return pageItem;
        }

        function getPageItemType(pageItemTemplate, pageItemCategory) {
            if (
                Array.isArray(pageItemCategory.runningitem) === true &&
                pageItemCategory.runningitem.indexOf(pageItemTemplate) >= 0
            ) {
                return 'runningitem';
            }
            if (
                Array.isArray(pageItemCategory.staticitem) === true &&
                pageItemCategory.staticitem.indexOf(pageItemTemplate) >= 0
            ) {
                return 'staticitem';
            }
            if (
                Array.isArray(pageItemCategory.dynamicitem) === true &&
                pageItemCategory.dynamicitem.indexOf(pageItemTemplate) >= 0
            ) {
                return 'dynamicitem';
            }

            return '';
        }

        function generatePageColumn(existingCol, pageItems) {
            var column = {}, existingPgeItms = null, i = 0,
                pageItem = null;

            existingPgeItms = layoutHandler.getPageItems(existingCol);
            if (existingPgeItms === null || existingPgeItms.length === 0) {
                return null;
            }
            column.htmlElement = existingCol;
            column.pageItems = [];
            for (; i < pageItems.length; i += 1) {
                pageItem = pageItems[i];
                if (existingPgeItms.indexOf(pageItem.htmlElement) >= 0) {
                    column.pageItems.push(pageItem);
                }
            }

            return column;
        }

        function generatePageColumnGroup(existingColGroup, pageItems) {
            var columnGroup = {}, existingCols = null, i = 0,
                existingCol = null;

            existingCols = layoutHandler.getPageColumns(existingColGroup);
            columnGroup.htmlElement = existingColGroup;
            columnGroup.columns = [];
            for (; i < existingCols.length; i += 1) {
                existingCol = generatePageColumn(existingCols[i], pageItems);
                if (existingCol !== null) {
                    columnGroup.columns.push(existingCol);
                }
            }
            if (columnGroup.columns.length === 0) {
                return null;
            }

            return columnGroup;
        }

        function generatePageColumnGroups(pageTemplate, pageItems) {
            var columnGroups = [], columnGroup = null, i = 0,
                existingColGroups = null;

            existingColGroups = layoutHandler.getPageColumnGroup(pageTemplate);
            for (; i < existingColGroups.length; i += 1) {
                columnGroup = generatePageColumnGroup(
                    existingColGroups[i], pageItems
                );
                if (columnGroup !== null) {
                    columnGroups.push(columnGroup);
                }
            }

            return columnGroups;
        }

        function getColumnPageItems(page) {
            var pageItems = [], i = 0, j = 0, columnGroup = null,
                column = null;

            for (; i < page.columnGroups.length; i += 1) {
                columnGroup = page.columnGroups[i];
                for (j = 0; j < columnGroup.columns.length; j += 1) {
                    column = columnGroup.columns[j];
                    pageItems.push.apply(pageItems, column.pageItems);
                }
            }

            return pageItems;
        }

        function getRelativeHeight(currentNode) {
            var parent = currentNode.parentNode, height = 0,
                childs = null, node = null, result = 0,
                clientRect = null, i = 0, length = 0;

            childs = parent.childNodes;
            length = childs.length;
            for (; i < length; i += 1) {
                node = childs[i];
                if (node === currentNode) {
                    continue;
                }
                clientRect = node.getBoundingClientRect();
                height += clientRect.height;
            }
            clientRect = parent.getBoundingClientRect();
            result = clientRect.height - height;

            return result;
        }

        function generatePageTextarea(pageTemplate, pageItems) {
            var i = 0, textarea = {}, existingPgeItms = null, pageItem = null,
                clientRect = null;

            textarea.htmlElement = layoutHandler.getPageTextarea(pageTemplate);
            clientRect = textarea.htmlElement.getBoundingClientRect();
            textarea.pageItems = [];
            textarea.maxHeight = getRelativeHeight(textarea.htmlElement);
            textarea.maxWidth = clientRect.width;
            existingPgeItms = layoutHandler.getPageItems(textarea.htmlElement);
            if (existingPgeItms === null || existingPgeItms.length === 0) {
                return textarea;
            }
            for (; i < pageItems.length; i += 1) {
                pageItem = pageItems[i];
                if (existingPgeItms.indexOf(pageItem.htmlElement) >= 0) {
                    textarea.pageItems.push(pageItem);
                }
            }
            if (textarea.pageItems.length > 0) {
                pageItem = textarea.pageItems[0];
                textarea.parent = pageItem.parent;
                textarea.columnGroups = generatePageColumnGroups(
                    textarea.htmlElement, textarea.pageItems
                );
                textarea.columnPageItems = getColumnPageItems(textarea);
            }

            return textarea;
        }

        function generatePageObject(pageTemplate, pageType) {
            var page = {}, i = 0, allPageItems = null, pageItemCategory = null,
                pageItemTemplate = null, pageItemType = '', pageItem = null;

            page.htmlElement = pageTemplate;
            page.type = pageType;
            page.pageItems = [];
            allPageItems = layoutHandler.getPageItems(pageTemplate);
            pageItemCategory = layoutHandler.getCategorisedPageItems(pageTemplate);
            for (; i < allPageItems.length; i += 1) {
                pageItemTemplate = allPageItems[i];
                pageItemType = getPageItemType(pageItemTemplate, pageItemCategory);
                pageItem = generatePageItemObject(pageItemTemplate, pageItemType);
                pageItem.parent = page;
                page.pageItems.push(pageItem);
            }
            page.columnGroups = generatePageColumnGroups(pageTemplate, page.pageItems);
            page.columnPageItems = getColumnPageItems(page);
            page.textarea = generatePageTextarea(pageTemplate, page.pageItems);

            return page;
        }

        function getPageType(pageTemplate, pageCategory) {
            if (
                Array.isArray(pageCategory.firstpage) === true &&
                pageCategory.firstpage.indexOf(pageTemplate) >= 0
            ) {
                return 'firstpage';
            }
            if (
                Array.isArray(pageCategory.otherpage) === true &&
                pageCategory.otherpage.indexOf(pageTemplate) >= 0
            ) {
                return 'otherpage';
            }
            if (
                Array.isArray(pageCategory.rectopage) === true &&
                pageCategory.rectopage.indexOf(pageTemplate) >= 0
            ) {
                return 'rectopage';
            }
            if (
                Array.isArray(pageCategory.versopage) === true &&
                pageCategory.versopage.indexOf(pageTemplate) >= 0
            ) {
                return 'versopage';
            }
            if (
                Array.isArray(pageCategory.landscapepage) === true &&
                pageCategory.landscapepage.indexOf(pageTemplate) >= 0
            ) {
                return 'landscapepage';
            }
            if (
                Array.isArray(pageCategory.versolandscapepage) === true &&
                pageCategory.versolandscapepage.indexOf(pageTemplate) >= 0
            ) {
                return 'versolandscapepage';
            }
            if (
                Array.isArray(pageCategory.rectolandscapepage) === true &&
                pageCategory.rectolandscapepage.indexOf(pageTemplate) >= 0
            ) {
                return 'rectolandscapepage';
            }

            return '';
        }

        function init() {
            var allPages = null, pageCategory = null, i = 0,
                pageTemplate = null, pageType = '',
                presetReader = new PresetReader(),
                layoutName = layoutHandler.getLayoutName(),
                holders = layoutHandler.getPlaceholders(), config = null;

            pages = [];
            config = presetReader.getPreset(layoutName, TemplateConfig);
            holders.layout.innerHTML = config.template.join('');
            allPages = layoutHandler.getPages(source);
            pageCategory = layoutHandler.getCategorisedPages(source);
            for (; i < allPages.length; i += 1) {
                pageTemplate = allPages[i];
                pageType = getPageType(pageTemplate, pageCategory);
                pageTemplate = layoutHandler.flowPage(pageTemplate, destination);
                pages.push(generatePageObject(pageTemplate, pageType));
            }
        }

        this.destroy = function destroyFn() {
            var i = 0, page = null;

            for (; i < pages.length; i += 1) {
                page = pages[i];
                page.htmlElement.style.display = 'none';
            }
            pages = [];
        };

        this.createNewPage = function createNewPageFn(template, target) {
            var pageTemplate = null, pageCategory = null, pageType = null;

            pageTemplate = template.htmlElement;
            pageCategory = layoutHandler.getCategorisedPages(source);
            pageType = getPageType(pageTemplate, pageCategory);
            pageTemplate = layoutHandler.flowPage(pageTemplate, target);

            return generatePageObject(pageTemplate, pageType);
        };

        this.getPage = function getPageFn(position, dynamicItemWidth) {
            var page = null;

            if (position > 1) {
                page = getItemOfType(pages, 'otherpage');
                if (page.length === 0) {
                    if ((position % 2) === 0) {
                        page = getItemOfType(pages, 'versopage');
                    }
                    if ((position % 2) === 1) {
                        page = getItemOfType(pages, 'rectopage');
                    }
                }
            }
            if (page === null || page.length === 0) {
                page = getItemOfType(pages, 'firstpage');
            }
            if (page !== null && page.length > 0) {
                page = page[0];
            }
            if (
                dynamicItemWidth > 0 &&
                isDynamicItemWidthExist(page, dynamicItemWidth) === false
            ) {
                page = getItemOfType(pages, 'landscapepage');
                if (page.length === 0) {
                    if ((position % 2) === 0) {
                        page = getItemOfType(pages, 'versolandscapepage');
                    }
                    if ((position % 2) === 1) {
                        page = getItemOfType(pages, 'rectolandscapepage');
                    }
                }
                if (page !== null && page.length > 0) {
                    page = page[0];
                }
            }

            return page;
        };

        this.getAllPageDynamicItems = function getAllPageDynamicItemsFn() {
            var items = [], i = 0;

            for (; i < pages.length; i += 1) {
                items.push.apply(items, getItemOfType(pages[i].pageItems, 'dynamicitem'));
            }

            return items;
        };

        this.getAllPageRunningItems = function getAllPageRunningItemsFn() {
            var items = [], i = 0;

            for (; i < pages.length; i += 1) {
                items.push.apply(items, getItemOfType(pages[i].pageItems, 'runningitem'));
            }

            return items;
        };

        this.isRunningItem = function isRunningItemFn(pageItem) {
            return pageItem.type === 'runningitem';
        };

        this.isStaticItem = function isStaticItemFn(pageItem) {
            return pageItem.type === 'staticitem';
        };

        this.isDynamicItem = function isDynamicItemFn(pageItem) {
            return pageItem.type === 'dynamicitem';
        };

        this.isFullPage = function isFullPageFn(pageItem) {
            var textarea = pageItem.parent.textarea,
                pageItemHeight = self.getPageItemHeight(pageItem);

            if (
                textarea.pageItems.indexOf(pageItem) >= 0 &&
                pageItemHeight >= textarea.maxHeight
            ) {
                return true;
            }

            return false;
        };

        this.isPageOverflow = function isPageOverflowFn(page) {
            var geoBound = null;

            geoBound = getGeomentricBound(page);
            if (geoBound.bottom >= 0) {
                return false;
            }

            return true;
        };

        this.isEmptyPage = function isEmptyPageFn(page) {
            var geoBound = null, textarea = page.textarea;

            geoBound = getGeomentricBound(page);
            if (geoBound.bottom === textarea.maxHeight) {
                return true;
            }

            return false;
        };

        this.getPageItemWidth = function getPageItemWidthFn(pageItem) {
            return pageItem.width;
        };

        this.getPageItemHeight = function getPageItemHeightFn(pageItem) {
            return pageItem.height;
        };

        this.getParentTextAreaHeight = function getParentTextAreaHeightFn(pageItem) {
            return pageItem.parent.textarea.maxHeight;
        };

        this.setPageItemHeight = function setPageItemHeightFn(pageItem, height) {
            pageItem.height = height;
        };

        this.resetPageItemHeight = function resetPageItemHeightFn(pageItem) {
            pageItem.height = 0;
        };

        this.resetFullPage = function resetFullPageFn(page) {
            var pageItems = null, pageItem = null, i = 0;

            pageItems = page.pageItems;
            for (; i < pageItems.length; i += 1) {
                pageItem = pageItems[i];
                self.resetPageItemHeight(pageItem);
            }
        };

        this.getRunningColumns = function getRunningColumnsFn(columnGroup) {
            var runningColumns = [], columns = null, column = null,
                i = 0, j = 0, pageItems = null, pageItem = null;

            columns = columnGroup.columns;
            for (; i < columns.length; i += 1) {
                column = columns[i];
                pageItems = column.pageItems;
                for (j = 0; j < pageItems.length; j += 1) {
                    pageItem = pageItems[j];
                    if (self.isRunningItem(pageItem) === true) {
                        runningColumns.push(column);
                        break;
                    }
                }
            }
            if (hasEqualColumnWidths(runningColumns) === true) {
                return runningColumns;
            }

            return [];
        };

        this.isTopMostPageItem = function isTopMostPageItemFn(pageItem) {
            var geoBound = null;

            geoBound = getGeomentricBound(pageItem.parent, pageItem);
            if (geoBound.top > 0) {
                return false;
            }

            return true;
        };

        this.isPageTopDynamicItem = function isPageTopDynamicItemFn(pageItem) {
            var geoBound = null;

            if (self.isDynamicItem(pageItem) === false) {
                return false;
            }
            geoBound = getGeomentricBound(pageItem.parent, pageItem);
            if (geoBound.top > 0 || geoBound.height > 0) {
                return false;
            }

            return true;
        };

        this.isPageBottomDynamicItem = function isPageBottomDynamicItemFn(pageItem) {
            var geoBound = null;

            if (self.isDynamicItem(pageItem) === false) {
                return false;
            }
            geoBound = getGeomentricBound(pageItem.parent, pageItem);
            if (
                geoBound.bottom > 0 || geoBound.height > 0 ||
                isBottomDynamicPageItem(pageItem.parent, pageItem) === false
            ) {
                return false;
            }

            return true;
        };

        init();
    };

    return TemplateHandler;
});
