var Configuration = {
    'Environment': 'dev',
    'Log': {
        'Level': 'info',
        'PersistOnServer': true
    },
    'Locale': 'en-US',
    'Version': 'kutung',
    'Plugins': {
        'Paginate': {
            'enable': true,
            'Routes': 'config/proof_routes.json',
            'Config': 'config/proof.json'
        }
    }
};
