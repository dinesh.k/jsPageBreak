/*global define*/
define([], function Config() {
    var rules = [
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .page:last-child'
                    ],
                    'selectAll': false
                }
            ],
            'action': [
                {
                    'id': 'pgnEndAct#1',
                    'name': 'applyStyle',
                    'style': {
                        'margin': '0px auto 0px auto'
                    }
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .journal-logo',
                        '.destination-container .customer-logo',
                        '.destination-container .cross-mark-logo'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnEndAct#2',
                    'name': 'applyStyle',
                    'style': {
                        'display': ''
                    }
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .page'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnEndAct#3',
                    'name': 'counter',
                    'selector': [
                        '.page-number'
                    ]
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .page .runningitem',
                        '.destination-container .page .textarea',
                        '.destination-container .page .textarea .column'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnEndAct#4',
                    'name': 'untilContent',
                    'className': 'top-most',
                    'fromTop': true
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .page .runningitem',
                        '.destination-container .page .textarea',
                        '.destination-container .page .textarea .column'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnEndAct#5',
                    'name': 'untilContent',
                    'className': 'bottom-most',
                    'fromTop': false
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .start-page-number'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnEndAct#6',
                    'name': 'templateReplace',
                    'template': [
                        ':startPageNumber'
                    ]
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .end-page-number'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnEndAct#7',
                    'name': 'templateReplace',
                    'template': [
                        ':endPageNumber'
                    ]
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .tb.bottom-most' +
                        ' + .te.bottom-most',
                        '.destination-container .hb.bottom-most' +
                        ' + .te.bottom-most'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'previousNode'
                }
            ],
            'action': [
                {
                    'id': 'pgnEndAct#8',
                    'name': 'removeNodeWrap'
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .tb.bottom-most'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnEndAct#9',
                    'name': 'regexReplace',
                    'find': /\s/g,
                    'replace': '\u200B'
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .tb.bottom-most',
                        '.destination-container .hb.bottom-most'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnEndAct#10',
                    'name': 'templateAfter',
                    'template': [
                        '<span class="broken-line-stretcher"></span>'
                    ]
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .hb.bottom-most'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnEndAct#11',
                    'name': 'addClass',
                    'classNames': ['inserted-hyphen']
                },
                {
                    'id': 'pgnEndAct#12',
                    'name': 'removeClass',
                    'classNames': ['hb', 'bottom-most']
                },
                {
                    'id': 'pgnEndAct#13',
                    'name': 'applyStyle',
                    'style': {
                        'display': 'inline-block',
                        'width': '3px',
                        'height': '3px',
                        'borderTop': '1px solid'
                    }
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .broken-line-stretcher'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnEndAct#14',
                    'name': 'applyStyle',
                    'style': {
                        'display': 'inline-block',
                        'width': '100%'
                    }
                },
                {
                    'id': 'pgnEndAct#15',
                    'name': 'removeHeight',
                    'blockTagNames': ['div']
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .page .page-margin'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnEndAct#16',
                    'name': 'fitHeight',
                    'selector': [
                        '.bottom-aligner'
                    ]
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .page .page-margin'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnEndAct#17',
                    'name': 'fitHeight',
                    'selector': [
                        '.textarea'
                    ]
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container *[title]'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnEndAct#18',
                    'name': 'removeAttribute',
                    'attributeName': 'title'
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container img'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnEndAct#19',
                    'name': 'freezeImages'
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .page'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnEndAct#20',
                    'name': 'pageCount',
                    'textareaSelector': '.textarea',
                    'onVariable': 'pageCount'
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .page'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnEndAct#21',
                    'name': 'maxWidth',
                    'onVariable': 'maxPageWidth'
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'scopeNode'
                }
            ],
            'action': [
                {
                    'id': 'pgnEndAct#22',
                    'name': 'applyStyle',
                    'style': {
                        'width': ':maxPageWidth'
                    }
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container *[id]'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnEndAct#23',
                    'name': 'regexAttributeReplace',
                    'attributeName': 'id',
                    'find': /^(.*)$/,
                    'replace': '$1-proof'
                }
            ]
        }
    ];

    return rules;
});
