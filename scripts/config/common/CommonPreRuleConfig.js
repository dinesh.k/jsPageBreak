/*global define*/
define([], function Config() {
    var rules = [
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.template-container .page .page-margin'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#1',
                    'name': 'applyStyle',
                    'style': {
                        'position': 'absolute'
                    }
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.template-container .page'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#2',
                    'name': 'applyStyle',
                    'style': {
                        'position': 'relative',
                        'padding': '0px',
                        'margin': '0px auto 20px auto',
                        'backgroundColor': 'white'
                    }
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.template-container .page .page-margin .columns'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#3',
                    'name': 'applyStyle',
                    'style': {
                        'display': 'flex',
                        'WebkitFlexDirection': 'row',
                        'MsFlexDirection': 'row',
                        'flexDirection': 'row'
                    }
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.template-container .page .page-margin',
                        '.template-container .page .page-margin .textarea',
                        '.template-container .page .page-margin .columns ' +
                        '.column'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#4',
                    'name': 'applyStyle',
                    'style': {
                        'display': 'flex',
                        'WebkitFlexDirection': 'column',
                        'MsFlexDirection': 'column',
                        'flexDirection': 'column'
                    }
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.template-container .landscapepage .page-margin ' +
                        '.textarea',
                        '.template-container .versolandscapepage .page-margin ' +
                        '.textarea',
                        '.template-container .rectolandscapepage .page-margin ' +
                        '.textarea'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#5',
                    'name': 'applyStyle',
                    'style': {
                        'WebkitJustifyContent': 'center',
                        'MsFlexPack': 'center',
                        'justifyContent': 'center'
                    }
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .cpedel',
                        '.source-container span[data-format="delete"][data-actor="ce"]'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'filterNodeExist',
                    'direction': false,
                    'selector': [
                        '.source-container .optreject .cpedel',
                        '.source-container span[data-format="reject"] span[data-format="delete"][data-actor="ce"]'
                    ]
                },
                {
                    'name': 'filterChildExist',
                    'selector': [
                        '.optreject',
                        'span[data-format="reject"]'
                    ],
                    'direction': false
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#6',
                    'name': 'removeNode'
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .optreject .cpeins',
                        '.source-container span[data-format="reject"] span[data-format="insert"][data-actor="ce"]',
                        '.source-container .optcomment',
                        '.source-container span[data-format="instruct"]',
                        '.source-container .optdel'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#7',
                    'name': 'removeNode'
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.layout-container .elements-template'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#7a',
                    'name': 'moveInto',
                    'selector': [
                        '.source-container'
                    ]
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.template-container .journal-logo',
                        '.source-container .journal-logo'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#8',
                    'name': 'applyStyle',
                    'style': {
                        'display': 'none'
                    }
                },
                {
                    'id': 'pgnBegAct#9',
                    'name': 'templateReplace',
                    'template': [
                        '<img src=":journalLogo">'
                    ]
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.template-container .customer-logo',
                        '.source-container .customer-logo'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#10',
                    'name': 'applyStyle',
                    'style': {
                        'display': 'none'
                    }
                },
                {
                    'id': 'pgnBegAct#11',
                    'name': 'templateReplace',
                    'template': [
                        '<img src=":customerLogo">'
                    ]
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.template-container .cross-mark-logo',
                        '.source-container .cross-mark-logo'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#12',
                    'name': 'applyStyle',
                    'style': {
                        'display': 'none'
                    }
                },
                {
                    'id': 'pgnBegAct#13',
                    'name': 'templateReplace',
                    'template': [
                        '<img src=":crossMarkLogo">'
                    ]
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.template-container .document-title',
                        '.source-container .document-title'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#14',
                    'name': 'templateReplace',
                    'template': [
                        ':documentTitle'
                    ]
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.template-container .abbreviated-title',
                        '.source-container .abbreviated-title'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#15',
                    'name': 'templateReplace',
                    'template': [
                        ':abbreviatedTitle'
                    ]
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.template-container .current-year',
                        '.source-container .current-year',
                        '.template-container .copyright-year',
                        '.source-container .copyright-year'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#16',
                    'name': 'templateReplace',
                    'template': [
                        ':currentYear'
                    ]
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.template-container .copyright-generated',
                        '.source-container .copyright-generated'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#17',
                    'name': 'templateReplace',
                    'template': [
                        ':copyright'
                    ]
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.template-container .cc-license-generated',
                        '.source-container .cc-license-generated'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#18',
                    'name': 'templateReplace',
                    'template': [
                        ':license'
                    ]
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container table td[rowspan]'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#19',
                    'name': 'handleTableRowSpan'
                },
                {
                    'id': 'pgnBegAct#20',
                    'name': 'addClass',
                    'classNames': ['td']
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .eqn-container'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#21',
                    'name': 'equationPathUpdation',
                    'equationSelector': 'img',
                    'equationPathAttribute': 'src',
                    'versionSelector': '.current_version',
                    'updatedPathSelector': '.equation_data_container > ' +
                    'span[version=":version"] .image-proof'
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .inline-formula img.svg'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#22',
                    'name': 'waitImageLoad'
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .inline-formula'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'filterChildExist',
                    'selector': ['img.svg'],
                    'direction': true
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#23',
                    'name': 'equationRendering',
                    'selector': ['img.svg'],
                    'fontSize': 13.33335
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container ul',
                        '.source-container ol'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#24',
                    'name': 'childCounter',
                    'attributeName': 'value',
                    'selector': ['li']
                }
            ]
        }
    ];

    return rules;
});
