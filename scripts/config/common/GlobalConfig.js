/*global define*/
define([], function Config() {
    var config = {
        'outputSelector': '.pager',
        'page': {
            'className': 'page',
            'columnGroupSelector': '.columns',
            'columnSelector': '.column',
            'textareaSelector': '.textarea'
        },
        'pageItem': {
            'className': 'page-item',
            'lineAttribute': 'data-line',
            'linkAttribute': 'data-link',
            'lineMinLimitAttribute': 'data-line-min-limit',
            'contentNeverStartAttribute': 'data-never-start'
        },
        'skeleton': {
            'layoutSelector': '.layout-container',
            'templateSelector': '.template-container',
            'sourceSelector': '.source-container',
            'destinationSelector': '.destination-container',
            'rootSelector': '.paginator',
            'template': [
                '<div class="pagination-container">',
                    '<div class="paginator" style="visibility: hidden;">',
                        '<div class="layout-container" style="display: none;">',
                        '</div>',
                        '<div class="template-container">',
                        '</div>',
                        '<div class="source-container" style="display: none;">',
                        '</div>',
                        '<div class="destination-container">',
                        '</div>',
                    '</div>',
                '</div>'
            ]
        }
    };

    return config;
});
