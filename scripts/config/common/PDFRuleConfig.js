/*global define*/
define([], function Config() {
    var rules = [
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .page .page-margin .columns'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pdfAct#1',
                    'name': 'applyStyle',
                    'style': {
                        'display': 'table',
                        'WebkitFlexDirection': null,
                        'MsFlexDirection': null,
                        'flexDirection': null
                    }
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .page .page-margin',
                        '.destination-container .page .page-margin .textarea',
                        '.destination-container .page .page-margin .columns ' +
                        '.column'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pdfAct#2',
                    'name': 'applyStyle',
                    'style': {
                        'display': 'table',
                        'WebkitFlexDirection': null,
                        'MsFlexDirection': null,
                        'flexDirection': null
                    }
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .landscapepage .page-margin ' +
                        '.textarea',
                        '.destination-container .versolandscapepage .page-margin ' +
                        '.textarea',
                        '.destination-container .rectolandscapepage .page-margin ' +
                        '.textarea'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pdfAct#3',
                    'name': 'applyStyle',
                    'style': {
                        'display': null,
                        'WebkitJustifyContent': null,
                        'MsFlexPack': null,
                        'justifyContent': null
                    }
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .landscapepage .page-margin ' +
                        '.textarea > *',
                        '.destination-container .versolandscapepage .page-margin ' +
                        '.textarea > *',
                        '.destination-container .rectolandscapepage .page-margin ' +
                        '.textarea > *'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pdfAct#4',
                    'name': 'verticalMiddle'
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .page .page-margin ' +
                        '.columns > *'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pdfAct#5',
                    'name': 'applyStyle',
                    'style': {
                        'display': 'table-cell',
                        'verticalAlign': 'top'
                    }
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pdfAct#6',
                    'name': 'applyStyle',
                    'style': {
                        'position': null
                    }
                }
            ]
        },
        {
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.layout-container',
                        '.template-container'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pdfAct#7',
                    'name': 'removeNode'
                }
            ]
        }
    ];

    return rules;
});
