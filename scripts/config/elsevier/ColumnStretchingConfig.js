/*global define*/
define([], function Config() {
    var config = [
        {
            'layouts': [
                'fiveG-double-column',
                'threeG-single-column'
            ],
            'stretchOn': [
                {
                    'selectors': [
                        '.dynamicitem.top-most.bottom-most ' +
                        '> div:not(:first-child)'
                    ],
                    'before': true,
                    'after': false,
                    'priority': 1,
                    'limit': 24
                },
                {
                    'selectors': [
                        '.dynamicitem.top-most:not(.bottom-most):not([data-footnote-region]) ' +
                        '> div'
                    ],
                    'before': false,
                    'after': true,
                    'priority': 1,
                    'limit': 24
                },
                {
                    'selectors': [
                        '.dynamicitem.bottom-most:not(.top-most):not([data-footnote-region]) ' +
                        '> div'
                    ],
                    'before': true,
                    'after': false,
                    'priority': 1,
                    'limit': 24
                },
                {
                    'selectors': [
                        '.dynamicitem.bottom-most[data-footnote-region]:not(.top-most) ' +
                        '> div:first-child'
                    ],
                    'before': true,
                    'after': false,
                    'priority': 1,
                    'limit': 24
                },
                {
                    'selectors': [
                        '.runningitem * + .ce_section.first_level',
                        '.runningitem * + .ce_acknowledgment',
                        '.runningitem * + .ce_appendices',
                        '.runningitem * + .ce_bibliography'
                    ],
                    'before': true,
                    'after': false,
                    'priority': 2,
                    'limit': 14
                },
                {
                    'selectors': [
                        '.runningitem *:not(.title_wrapper) + ' +
                        '.ce_section.second_level:not(.first_level)'
                    ],
                    'before': true,
                    'after': false,
                    'priority': 3,
                    'limit': 14
                },
                {
                    'selectors': [
                        '.runningitem *:not(.title_wrapper) + ' +
                        '.ce_section:not(.first_level):not(.second_level)'
                    ],
                    'before': true,
                    'after': false,
                    'priority': 4,
                    'limit': 14
                },
                {
                    'selectors': [
                        '.runningitem .ce_display'
                    ],
                    'before': true,
                    'after': true,
                    'priority': 5,
                    'limit': 10
                }
            ]
        }
    ];

    return config;
});
