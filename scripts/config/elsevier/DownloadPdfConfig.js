/* global define*/
define([], function DownloadPdfConfigLoader() {
    var downloadPdfConfig = [
        {
            'layouts': [
                'fiveG-double-column',
                'threeG-single-column'
            ],
            'journals': [],
            'page': {},
            'enablePdfRuler': true
        }
    ];

    return downloadPdfConfig;
});
