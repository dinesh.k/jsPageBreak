/*global define*/
define([], function Config() {
    var config = [
        {
            'layouts': [
                'fiveG-double-column',
                'threeG-single-column'
            ],
            'lang': 'en-us',
            'selectors': [
                '.ce_keywords .ce_text', '.ce_para', '.ce_simple-para',
                '.ce_abstract .newline_simplepara', '.ce_bib-reference',
                '.ce_biography'
            ],
            'excludeSelectors': [
                '.ce_sup', '.ce_inf', '.optsup', '.optsub', 'a',
                '.ce_inter-ref', '.ce_doi', '.doi', '.ce_e-address',
                '.eqn-container'
            ]
        }
    ];

    return config;
});
