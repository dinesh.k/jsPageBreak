/*global define*/
define([], function Config() {
    var date = new Date(),
        config = [
            {
                'name': 'fiveG-double-column',
                'keyterms': ['double'],
                'referenceStyling': true,
                'data': {
                    'copyright': '© 2017.',
                    'currentYear': date.getFullYear()
                }
            },
            {
                'name': 'threeG-single-column',
                'keyterms': ['single'],
                'default': true,
                'referenceStyling': true,
                'data': {
                    'copyright': '© 2017.',
                    'currentYear': date.getFullYear()
                }
            }
        ];

    return config;
});
