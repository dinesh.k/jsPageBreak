/*global define*/
define([], function Config() {
    var config = [
        {
            'layouts': [
                'fiveG-double-column',
                'threeG-single-column'
            ],
            'lineTypes': [
                {
                    'typeName': 'head',
                    'breakable': true,
                    'keepBefore': 0,
                    'keepAfter': 0,
                    'widows': 0,
                    'orphans': 0,
                    'selectors': [
                        '.elements-template .first-page-footnote',
                        '.article .body > .head'
                    ],
                    'continueAfter': []
                },
                {
                    'typeName': 'article-abstract-top',
                    'breakable': true,
                    'keepBefore': 0,
                    'keepAfter': 0,
                    'widows': 0,
                    'orphans': 0,
                    'selectors': [
                        '.elements-template .article-abstract-group-top-panel'
                    ],
                    'continueAfter': ['head']
                },
                {
                    'typeName': 'article-abstract-left',
                    'breakable': true,
                    'keepBefore': 0,
                    'keepAfter': 0,
                    'widows': 0,
                    'orphans': 0,
                    'selectors': [
                        '.elements-template .article-abstract-group-left-panel'
                    ],
                    'continueAfter': ['article-abstract-top']
                },
                {
                    'typeName': 'article-abstract-right',
                    'breakable': true,
                    'keepBefore': 0,
                    'keepAfter': 0,
                    'widows': 0,
                    'orphans': 0,
                    'selectors': [
                        '.elements-template .article-abstract-group-right-panel'
                    ],
                    'continueAfter': ['article-abstract-top']
                },
                {
                    'typeName': 'article-abstract-bottom',
                    'breakable': true,
                    'keepBefore': 0,
                    'keepAfter': 0,
                    'widows': 0,
                    'orphans': 0,
                    'selectors': [
                        '.elements-template .article-abstract-group-bottom-panel'
                    ],
                    'continueAfter': [
                        'article-abstract-left', 'article-abstract-right'
                    ]
                },
                {
                    'typeName': 'article-abstract',
                    'breakable': true,
                    'keepBefore': 0,
                    'keepAfter': 0,
                    'widows': 0,
                    'orphans': 0,
                    'selectors': [
                        '.elements-template .article-abstract'
                    ],
                    'continueAfter': ['head']
                },
                {
                    'typeName': 'body',
                    'breakable': true,
                    'keepBefore': 0,
                    'keepAfter': 0,
                    'widows': 0,
                    'orphans': 0,
                    'selectors': ['.article'],
                    'continueAfter': [
                        'article-abstract-bottom', 'article-abstract'
                    ]
                },
                {
                    'typeName': 'tail',
                    'breakable': true,
                    'keepBefore': 0,
                    'keepAfter': 0,
                    'widows': 0,
                    'orphans': 0,
                    'selectors': [
                        '.ce_bibliography',
                        '.ce_further-reading',
                        '.ce_biography',
                        '.ce_glossary'
                    ],
                    'continueAfter': ['body']
                },
                {
                    'typeName': 'heading',
                    'breakable': false,
                    'keepBefore': 0,
                    'keepAfter': 2,
                    'widows': 0,
                    'orphans': 0,
                    'selectors': [
                        '.article .body > .head > .ce_title',
                        '.ce_section > .sectionline_opt',
                        '.ce_section-title',
                        '.tablecaption',
                        'table thead',
                        '.ce_biography .container-image'
                    ],
                    'continueAfter': []
                },
                {
                    'typeName': 'nestedfigure',
                    'breakable': true,
                    'keepBefore': 1,
                    'keepAfter': 0,
                    'widows': 0,
                    'orphans': 0,
                    'selectors': [
                        '.nested-figure .figcaption'
                    ],
                    'continueAfter': []
                },
                {
                    'typeName': 'paragraph',
                    'breakable': true,
                    'keepBefore': 0,
                    'keepAfter': 0,
                    'widows': 2,
                    'orphans': 2,
                    'selectors': [
                        '.ce_para',
                        '.newline_simplepara',
                        '.newline_notepara',
                        '.ce_simple-para',
                        '.ce_bib-reference',
                        '.ce_keywords'
                    ],
                    'continueAfter': []
                }
            ]
        }
    ];

    return config;
});
