/*global define*/
define([], function Config() {
    var config = [
        {
            'layouts': [
                'fiveG-double-column',
                'threeG-single-column'
            ],
            'linkMappings': [
                {
                    'id': 'link#1',
                    'groupid': 'first-page-footnotes',
                    'footnoteType': true,
                    'completeBefore': null,
                    'template': {
                        'prepend': [
                            '<div class="short-ruler"></div>'
                        ],
                        'append': []
                    },
                    'source': {
                        'selector': 'span.first-page-footnote-anchor[refid]',
                        'attribute': 'refid'
                    },
                    'destination': {
                        'selector': 'div.first-page-footnote-dest[id]',
                        'attribute': 'id'
                    }
                },
                {
                    'id': 'link#2',
                    'groupid': 'footnotes',
                    'footnoteType': true,
                    'completeBefore': null,
                    'template': {
                        'prepend': [
                            '<div class="short-ruler"></div>'
                        ],
                        'append': []
                    },
                    'source': {
                        'selector': 'a.ce_cross-ref[title]',
                        'attribute': 'title'
                    },
                    'destination': {
                        'selector': [
                            'div.ce_footnote[id]:not(.author-footnote):not([data-footnote-parent])',
                            'div.ce_foonote[id]:not(.author-footnote):not([data-footnote-parent])'
                        ],
                        'attribute': 'id'
                    }
                },
                {
                    'id': 'link#3',
                    'groupid': 'floats',
                    'footnoteType': false,
                    'completeBefore': 'tail',
                    'template': {
                        'prepend': [],
                        'append': []
                    },
                    'source': {
                        'selector': 'span.ce_float-anchor[refid]',
                        'attribute': 'refid'
                    },
                    'destination': {
                        'selector': 'div.figure[id]:not(.display)',
                        'attribute': 'id'
                    }
                },
                {
                    'id': 'link#4',
                    'groupid': 'floats',
                    'footnoteType': false,
                    'completeBefore': 'tail',
                    'template': {
                        'prepend': [],
                        'append': []
                    },
                    'source': {
                        'selector': 'span.ce_float-anchor[refid]',
                        'attribute': 'refid'
                    },
                    'destination': {
                        'selector': 'div.table[id]:not(.display)',
                        'attribute': 'id'
                    }
                },
                {
                    'id': 'link#5',
                    'groupid': 'floats',
                    'footnoteType': false,
                    'completeBefore': 'tail',
                    'template': {
                        'prepend': [],
                        'append': []
                    },
                    'source': {
                        'selector': 'span.ce_float-anchor[refid]',
                        'attribute': 'refid'
                    },
                    'destination': {
                        'selector': 'div.ce_textbox[id]:not(.display)',
                        'attribute': 'id'
                    }
                }
            ]
        }
    ];

    return config;
});
