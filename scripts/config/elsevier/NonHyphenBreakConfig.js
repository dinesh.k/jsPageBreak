/*global define*/
define([], function Config() {
    var config = [
        {
            'layouts': [
                'fiveG-double-column',
                'threeG-single-column'
            ],
            'breakers': [
                {
                    'selectors': [
                        '.ce_doi', '.ce_inter-ref',
                        '.doi', '.ce_e-address'
                    ],
                    'before': [],
                    'after': [
                        '\\', '/', '.',
                        ',', ':', ';',
                        '_', '&', '@'
                    ]
                }
            ]
        }
    ];

    return config;
});
