/*global define*/
define([], function Config() {
    var rules = [
        {
            'ruleId': 'no-column-bottom-space',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.fiveG-double-column .destination-container ' +
                        '.bottom-most',
                        '.threeG-single-column .destination-container ' +
                        '.bottom-most',
                        '.heliyon-stubbed-single-column .destination-container ' +
                        '.bottom-most'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPostAct#1',
                    'name': 'applyStyle',
                    'style': {
                        'marginBottom': '0mm',
                        'paddingBottom': '0mm'
                    }
                }
            ]
        },
        {
            'ruleId': 'no-column-top-space',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.fiveG-double-column .destination-container ' +
                        '.top-most',
                        '.threeG-single-column .destination-container ' +
                        '.top-most',
                        '.heliyon-stubbed-single-column .destination-container ' +
                        '.top-most'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPostAct#2',
                    'name': 'applyStyle',
                    'style': {
                        'marginTop': '0mm',
                        'paddingTop': '0mm'
                    }
                }
            ]
        },
        {
            'ruleId': 'keep-in-center',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .figcaption'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'filterNodeExist',
                    'selector': [
                        '.destination-container .figcaption.continued'
                    ],
                    'direction': false
                },
                {
                    'name': 'filterSingleLine'
                }
            ],
            'action': [
                {
                    'id': 'pgnPostAct#3',
                    'name': 'addClass',
                    'classNames': ['keep-in-center']
                }
            ]
        },
        {
            'ruleId': 'no-inline-behavior',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .ce_display',
                        '.destination-container .table',
                        '.destination-container .displayfigtab',
                        '.destination-container .ce_list',
                        '.destination-container .ulist',
                        '.destination-container .article-abstract-group'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPostAct#4',
                    'name': 'templateAppend',
                    'template': [
                        '<div class="float-parent" style="clear: both;">',
                        '</div>'
                    ]
                }
            ]
        },
        {
            'ruleId': 'reorder-footnotes',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .page .page-item.dynamicitem'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPostAct#5',
                    'name': 'reorderFootnotes',
                    'selector': [
                        '.ce_footnote',
                        '.ce_foonote'
                    ]
                }
            ]
        },
        {
            'ruleId': 'require-figcap-arrow',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.cpc-single-column .destination-container div.figure' +
                        '[id]:not(.display).top-figure-caption .figcaption',
                        '.cpc-double-column .destination-container div.figure' +
                        '[id]:not(.display).top-figure-caption .figcaption'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPostAct#6',
                    'name': 'templateAfter',
                    'template': [
                        '<span class="figcaption-arrow">&#x25C0;</span>'
                    ]
                }
            ]
        },
        {
            'ruleId': 'no-bottom-border',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .table ' +
                        'tbody tr:not([data-attr-role]) td:only-child'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'parentNode'
                }
            ],
            'action': [
                {
                    'id': 'pgnPostAct#7',
                    'name': 'applyStyle',
                    'style': {
                        'borderBottom': '0px'
                    }
                }
            ]
        },
        {
            'ruleId': 'no-top-border',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .table ' +
                        'tbody tr:not([data-attr-role]):not(:first-child) ' +
                        'td:only-child'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'parentNode'
                },
                {
                    'name': 'previousNode'
                }
            ],
            'action': [
                {
                    'id': 'pgnPostAct#8',
                    'name': 'applyStyle',
                    'style': {
                        'borderBottom': '0px'
                    }
                }
            ]
        },
        {
            'ruleId': 'no-sibling-bottom-border',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.destination-container .table ' +
                        'tbody tr:not([data-attr-role]):first-child ' +
                        'td:only-child'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'parentNode'
                },
                {
                    'name': 'parentNode'
                },
                {
                    'name': 'previousNode'
                },
                {
                    'name': 'lastElementChildNode'
                }
            ],
            'action': [
                {
                    'id': 'pgnPostAct#9',
                    'name': 'applyStyle',
                    'style': {
                        'borderBottom': '0px'
                    }
                }
            ]
        }
    ];

    return rules;
});
