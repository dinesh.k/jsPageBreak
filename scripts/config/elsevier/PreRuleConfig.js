/*global define*/
define([], function Config() {
    var rules = [
        {
            'ruleId': 'stitle-cleanup',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .stitle .x'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#1',
                    'name': 'removeNode'
                }
            ]
        },
        {
            'ruleId': 'insert-elements-template',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .elements-template'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#2',
                    'name': 'moveBefore',
                    'selector': [
                        '.source-container .article .head'
                    ]
                }
            ]
        },
        {
            'ruleId': 'move-toc',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .article .toc-panel'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#3',
                    'name': 'moveInto',
                    'selector': [
                        '.source-container .toc-group',
                        '.template-container .toc-group'
                    ]
                }
            ]
        },
        {
            'ruleId': 'move-abstract',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .head .ce_abstract'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#4',
                    'name': 'moveInto',
                    'selector': [
                        '.source-container .abstract-group',
                        '.template-container .abstract-group'
                    ]
                }
            ]
        },
        {
            'ruleId': 'move-keywords',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .head .ce_keywords'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#5',
                    'name': 'moveInto',
                    'selector': [
                        '.source-container .keywords-group',
                        '.template-container .keywords-group'
                    ]
                }
            ]
        },
        {
            'ruleId': 'move-miscellaneous',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .head .ce_miscellaneous'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#6',
                    'name': 'moveInto',
                    'selector': [
                        '.source-container .miscellaneous-info',
                        '.template-container .miscellaneous-info'
                    ]
                }
            ]
        },
        {
            'ruleId': 'move-presented-info',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .head .ce_presented'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#7',
                    'name': 'moveInto',
                    'selector': [
                        '.source-container .presented-info',
                        '.template-container .presented-info'
                    ]
                }
            ]
        },
        {
            'ruleId': 'move-dedication-info',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .head .ce_dedication'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#8',
                    'name': 'moveInto',
                    'selector': [
                        '.source-container .dedication-info',
                        '.template-container .dedication-info'
                    ]
                }
            ]
        },
        {
            'ruleId': 'pii-cleanup',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .item-info .pii .x'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#9',
                    'name': 'removeNode'
                }
            ]
        },
        {
            'ruleId': 'copy-pii-to-attr',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .item-info .pii'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#10',
                    'name': 'innerTextToAttribute',
                    'attributeName': 'data-pii'
                },
                {
                    'id': 'pgnPreAct#11',
                    'name': 'copyNode',
                    'parallelCopy': false,
                    'copyChilds': true,
                    'replaceChilds': true,
                    'selector': [
                        '.template-container .issn-generated',
                        '.source-container .issn-generated'
                    ]
                }
            ]
        },
        {
            'ruleId': 'issn-cleanup',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.template-container .issn-generated',
                        '.source-container .issn-generated'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#12',
                    'name': 'regexReplace',
                    'find': /^([^0-9]*)([0-9-]*)(.*)$/g,
                    'replace': '$2'
                }
            ]
        },
        {
            'ruleId': 'doi-cleanup',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .item-info .doi .x'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#13',
                    'name': 'applyStyle',
                    'style': {
                        'display': 'none'
                    }
                }
            ]
        },
        {
            'ruleId': 'copy-doi-info',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .item-info .doi'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#14',
                    'name': 'copyNode',
                    'parallelCopy': false,
                    'copyChilds': true,
                    'replaceChilds': true,
                    'selector': [
                        '.template-container .doi-filled',
                        '.source-container .doi-filled'
                    ]
                }
            ]
        },
        {
            'ruleId': 'articlenum-cleanup',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .item-info .ce_article-number .x'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#15',
                    'name': 'removeNode'
                }
            ]
        },
        {
            'ruleId': 'copy-articlenum',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .item-info .ce_article-number'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#16',
                    'name': 'copyNode',
                    'parallelCopy': false,
                    'copyChilds': true,
                    'replaceChilds': true,
                    'selector': [
                        '.template-container .article-no',
                        '.source-container .article-no'
                    ]
                }
            ]
        },
        {
            'ruleId': 'associated-resource-cleanup',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .item-info .ce_associated-resource .x'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#16a',
                    'name': 'removeNode'
                }
            ]
        },
        {
            'ruleId': 'copy-associated-resource',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .item-info .ce_associated-resource'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#16b',
                    'name': 'copyNode',
                    'parallelCopy': false,
                    'copyChilds': true,
                    'replaceChilds': true,
                    'selector': [
                        '.template-container .associated-resource',
                        '.source-container .associated-resource'
                    ]
                }
            ]
        },
        {
            'ruleId': 'copy-received-date',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .item-info .ce_date-received > ' +
                        'div .x'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#17',
                    'name': 'copyNode',
                    'parallelCopy': false,
                    'copyChilds': true,
                    'replaceChilds': true,
                    'selector': [
                        '.template-container .ce_date-received .date-span',
                        '.source-container .ce_date-received .date-span'
                    ]
                }
            ]
        },
        {
            'ruleId': 'copy-revised-date',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .item-info .ce_date-revised > ' +
                        'div .x'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#18',
                    'name': 'copyNode',
                    'parallelCopy': false,
                    'copyChilds': true,
                    'replaceChilds': true,
                    'selector': [
                        '.template-container .ce_date-revised .date-span',
                        '.source-container .ce_date-revised .date-span'
                    ]
                }
            ]
        },
        {
            'ruleId': 'copy-accepted-date',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .item-info .ce_date-accepted > ' +
                        'div .x'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#19',
                    'name': 'copyNode',
                    'parallelCopy': false,
                    'copyChilds': true,
                    'replaceChilds': true,
                    'selector': [
                        '.template-container .ce_date-accepted .date-span',
                        '.source-container .ce_date-accepted .date-span'
                    ]
                }
            ]
        },
        {
            'ruleId': 'copy-to-RRH',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .body .stitle'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#20',
                    'name': 'copyNode',
                    'parallelCopy': false,
                    'copyChilds': true,
                    'replaceChilds': true,
                    'selector': [
                        '.template-container .left-header .document-title'
                    ]
                }
            ]
        },
        {
            'ruleId': 'copy-to-LRH',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .body .left-running-head'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#21',
                    'name': 'copyNode',
                    'parallelCopy': false,
                    'copyChilds': true,
                    'replaceChilds': true,
                    'selector': [
                        '.template-container .right-header .document-title'
                    ]
                }
            ]
        },
        {
            'ruleId': 'article-history-cleanup',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.template-container .ce_date-received .date-span',
                        '.template-container .ce_date-revised .date-span',
                        '.template-container .ce_date-accepted .date-span',
                        '.source-container .ce_date-received .date-span',
                        '.source-container .ce_date-revised .date-span',
                        '.source-container .ce_date-accepted .date-span'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#22',
                    'name': 'regexReplace',
                    'find': /^([^:]*):(.*)$/g,
                    'replace': '$2'
                },
                {
                    'id': 'pgnPreAct#23',
                    'name': 'regexReplace',
                    'find': /&nbsp;/g,
                    'replace': ' '
                }
            ]
        },
        {
            'ruleId': 'no-show-empty-parent',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.template-container .ce_date-received',
                        '.template-container .ce_date-revised',
                        '.template-container .ce_date-accepted',
                        '.source-container .ce_date-received',
                        '.source-container .ce_date-revised',
                        '.source-container .ce_date-accepted'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'filterChildExist',
                    'selector': [
                        '.date-span .no-data'
                    ],
                    'direction': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#24',
                    'name': 'applyStyle',
                    'style': {
                        'display': 'none'
                    }
                }
            ]
        },
        {
            'ruleId': 'copy-copyrightyear-to-attr',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.pagination-container'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#25',
                    'name': 'copyAttribute',
                    'fromSelector': ['.item-info .ce_copyright[year]'],
                    'fromAttribute': 'year',
                    'toSelector': ['.copyright-year'],
                    'toAttribute': 'data-copyright-year',
                    'append': false
                }
            ]
        },
        {
            'ruleId': 'copy-copyrightyear-to-innertext',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.template-container .copyright-year' +
                        '[data-copyright-year]',
                        '.source-container .copyright-year' +
                        '[data-copyright-year]'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#26',
                    'name': 'attributeToInnerText',
                    'attributeName': 'data-copyright-year'
                }
            ]
        },
        {
            'ruleId': 'naming-first-level-head',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_sections .ce_section[' +
                        'data-attr-role="sharp"]'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#27',
                    'name': 'addClass',
                    'classNames': ['first_level_head']
                }
            ]
        },
        {
            'ruleId': 'naming-first-level',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_sections .ce_section:not(' +
                        '[data-attr-role="sharp"])',
                        '.source-container .ce_sections .ce_section' +
                        '[data-attr-role="sharp"] .ce_section',
                        '.source-container .ce_appendices .ce_section'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#28',
                    'name': 'addClass',
                    'classNames': ['first_level']
                },
                {
                    'id': 'pgnPreAct#29',
                    'name': 'removeClass',
                    'classNames': ['first_level_head']
                }
            ]
        },
        {
            'ruleId': 'naming-second-level-head',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_sections .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section' +
                        '[data-attr-role="sharp"]',
                        '.source-container .ce_sections .ce_section' +
                        '[data-attr-role="sharp"] .ce_section .ce_section' +
                        '[data-attr-role="sharp"]'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#30',
                    'name': 'addClass',
                    'classNames': ['second_level_head']
                },
                {
                    'id': 'pgnPreAct#31',
                    'name': 'removeClass',
                    'classNames': ['first_level_head', 'first_level']
                }
            ]
        },
        {
            'ruleId': 'naming-second-level',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_sections .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section:not(' +
                        '[data-attr-role="sharp"])',
                        '.source-container .ce_sections .ce_section' +
                        '[data-attr-role="sharp"] .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section:not(' +
                        '[data-attr-role="sharp"])',
                        '.source-container .ce_sections .ce_section' +
                        '[data-attr-role="sharp"] .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section' +
                        '[data-attr-role="sharp"] .ce_section:not(' +
                        '[data-attr-role="sharp"])',
                        '.source-container .ce_sections .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section' +
                        '[data-attr-role="sharp"] .ce_section:not(' +
                        '[data-attr-role="sharp"])',
                        '.source-container .ce_appendices .ce_section ' +
                        '.ce_section'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#32',
                    'name': 'addClass',
                    'classNames': ['second_level']
                },
                {
                    'id': 'pgnPreAct#33',
                    'name': 'removeClass',
                    'classNames': ['first_level']
                }
            ]
        },
        {
            'ruleId': 'naming-third-level',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_sections .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section',
                        '.source-container .ce_sections .ce_section' +
                        '[data-attr-role="sharp"] .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section',
                        '.source-container .ce_sections .ce_section' +
                        '[data-attr-role="sharp"] .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section' +
                        '[data-attr-role="sharp"] .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section',
                        '.source-container .ce_sections .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section' +
                        '[data-attr-role="sharp"] .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section',
                        '.source-container .ce_appendices .ce_section ' +
                        '.ce_section .ce_section'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#34',
                    'name': 'addClass',
                    'classNames': ['third_level']
                },
                {
                    'id': 'pgnPreAct#35',
                    'name': 'removeClass',
                    'classNames': ['first_level', 'second_level']
                }
            ]
        },
        {
            'ruleId': 'naming-fourth-level',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_sections .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section .ce_section',
                        '.source-container .ce_sections .ce_section' +
                        '[data-attr-role="sharp"] .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section .ce_section',
                        '.source-container .ce_sections .ce_section' +
                        '[data-attr-role="sharp"] .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section' +
                        '[data-attr-role="sharp"] .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section .ce_section',
                        '.source-container .ce_sections .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section' +
                        '[data-attr-role="sharp"] .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section .ce_section',
                        '.source-container .ce_appendices .ce_section ' +
                        '.ce_section .ce_section .ce_section'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#36',
                    'name': 'addClass',
                    'classNames': ['fourth_level']
                },
                {
                    'id': 'pgnPreAct#37',
                    'name': 'removeClass',
                    'classNames': [
                        'first_level', 'second_level', 'third_level'
                    ]
                }
            ]
        },
        {
            'ruleId': 'naming-fifth-level',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_sections .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section .ce_section ' +
                        '.ce_section',
                        '.source-container .ce_sections .ce_section' +
                        '[data-attr-role="sharp"] .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section .ce_section ' +
                        '.ce_section',
                        '.source-container .ce_sections .ce_section' +
                        '[data-attr-role="sharp"] .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section' +
                        '[data-attr-role="sharp"] .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section .ce_section ' +
                        '.ce_section',
                        '.source-container .ce_sections .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section' +
                        '[data-attr-role="sharp"] .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section .ce_section ' +
                        '.ce_section',
                        '.source-container .ce_appendices .ce_section ' +
                        '.ce_section .ce_section .ce_section .ce_section'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#38',
                    'name': 'addClass',
                    'classNames': ['fifth_level']
                },
                {
                    'id': 'pgnPreAct#39',
                    'name': 'removeClass',
                    'classNames': [
                        'first_level', 'second_level', 'third_level',
                        'fourth_level'
                    ]
                }
            ]
        },
        {
            'ruleId': 'naming-sixth-level',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_sections .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section .ce_section ' +
                        '.ce_section .ce_section',
                        '.source-container .ce_sections .ce_section' +
                        '[data-attr-role="sharp"] .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section .ce_section ' +
                        '.ce_section .ce_section',
                        '.source-container .ce_sections .ce_section' +
                        '[data-attr-role="sharp"] .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section' +
                        '[data-attr-role="sharp"] .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section .ce_section ' +
                        '.ce_section .ce_section',
                        '.source-container .ce_sections .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section' +
                        '[data-attr-role="sharp"] .ce_section:not(' +
                        '[data-attr-role="sharp"]) .ce_section .ce_section ' +
                        '.ce_section .ce_section',
                        '.source-container .ce_appendices .ce_section ' +
                        '.ce_section .ce_section .ce_section ' +
                        '.ce_section .ce_section'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#40',
                    'name': 'addClass',
                    'classNames': ['sixth_level']
                },
                {
                    'id': 'pgnPreAct#41',
                    'name': 'removeClass',
                    'classNames': [
                        'first_level', 'second_level', 'third_level',
                        'fourth_level', 'fifth_level'
                    ]
                }
            ]
        },
        {
            'ruleId': 'naming-box-first-level',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_textbox ' +
                        '.ce_sections .ce_section',
                        '.source-container .ce_dtextbox ' +
                        '.ce_sections .ce_section'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#42',
                    'name': 'addClass',
                    'classNames': ['box_first_level']
                },
                {
                    'id': 'pgnPreAct#43',
                    'name': 'removeClass',
                    'classNames': [
                        'first_level', 'second_level', 'third_level',
                        'fourth_level', 'fifth_level', 'sixth_level'
                    ]
                }
            ]
        },
        {
            'ruleId': 'naming-box-second-level',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_textbox ' +
                        '.ce_sections .ce_section .ce_section',
                        '.source-container .ce_dtextbox ' +
                        '.ce_sections .ce_section .ce_section'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#44',
                    'name': 'addClass',
                    'classNames': ['box_second_level']
                },
                {
                    'id': 'pgnPreAct#45',
                    'name': 'removeClass',
                    'classNames': [
                        'first_level', 'second_level', 'third_level',
                        'fourth_level', 'fifth_level', 'sixth_level',
                        'box_first_level'
                    ]
                }
            ]
        },
        {
            'ruleId': 'naming-box-third-level',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_textbox ' +
                        '.ce_sections .ce_section .ce_section .ce_section',
                        '.source-container .ce_dtextbox ' +
                        '.ce_sections .ce_section .ce_section .ce_section'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#46',
                    'name': 'addClass',
                    'classNames': ['box_third_level']
                },
                {
                    'id': 'pgnPreAct#47',
                    'name': 'removeClass',
                    'classNames': [
                        'first_level', 'second_level', 'third_level',
                        'fourth_level', 'fifth_level', 'sixth_level',
                        'box_first_level', 'box_second_level'
                    ]
                }
            ]
        },
        {
            'ruleId': 'move-abbreviation-info',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_keywords .ce_section-title'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'filterRegexMatch',
                    'direction': true,
                    'regex': /Abbreviation/ig
                },
                {
                    'name': 'parentNode'
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#48',
                    'name': 'moveInto',
                    'selector': [
                        '.template-container .abbrevation-filled',
                        '.source-container .abbrevation-filled'
                    ]
                }
            ]
        },
        {
            'ruleId': 'move-article-footnote-info',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_article-footnote'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#49',
                    'name': 'moveInto',
                    'selector': [
                        '.template-container .article-footnote-filled',
                        '.source-container .article-footnote-filled'
                    ]
                }
            ]
        },
        {
            'ruleId': 'copy-copyright-info',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_copyright'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'filterRegexMatch',
                    'direction': false,
                    'regex': /unknown/ig
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#50',
                    'name': 'copyNode',
                    'parallelCopy': false,
                    'copyChilds': true,
                    'replaceChilds': true,
                    'selector': [
                        '.source-container .copyright-filled',
                        '.template-container .copyright-filled'
                    ]
                }
            ]
        },
        {
            'ruleId': 'copyright-cleanup',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .copyright-filled .x',
                        '.template-container .copyright-filled .x'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#51',
                    'name': 'applyStyle',
                    'style': {
                        'display': 'none'
                    }
                }
            ]
        },
        {
            'ruleId': 'move-correspondence-info',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_correspondence'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#52',
                    'name': 'moveInto',
                    'selector': [
                        '.template-container .correspondence-filled',
                        '.source-container .correspondence-filled'
                    ]
                }
            ]
        },
        {
            'ruleId': 'move-author-footnote',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_author-group .ce_footnote',
                        '.source-container .ce_author-group .ce_foonote'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#53',
                    'name': 'moveInto',
                    'selector': [
                        '.template-container .author-group-footnote-filled',
                        '.source-container .author-group-footnote-filled'
                    ]
                },
                {
                    'id': 'pgnPreAct#54',
                    'name': 'addClass',
                    'classNames': ['author-footnote']
                }
            ]
        },
        {
            'ruleId': 'format-web-address',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_author-group .ce_author'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'filterNodeExist',
                    'direction': false,
                    'selector': [
                        '.source-container .ce_author-group ' +
                        '.ce_collaboration .ce_author'
                    ]
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#55',
                    'name': 'webAddressFormatting',
                    'emailSelector': [
                        '.ce_e-address[data-type=\'email\']',
                        '.ce_e-address a[data-type=\'email\']'
                    ],
                    'urlSelector': [
                        '.ce_e-address[type=\'url\']',
                        '.ce_e-address a[data-type=\'url\']'
                    ],
                    'socialMediaSelector': '.ce_e-address a[data-type=\'social-media\']',
                    'authorGivenNameSelector': '.ce_given-name',
                    'authorSurnameSelector': '.ce_surname',
                    'moveIntoSelector': [
                        '.template-container .author-web-generated',
                        '.source-container .author-web-generated'
                    ]
                }
            ]
        },
        {
            'ruleId': 'no-eaddress-empty-child',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_author-group .ce_author .ce_e-address'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#55a',
                    'name': 'removeNode'
                }
            ]
        },
        {
            'ruleId': 'generate-author-name',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_author-group .ce_author'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#56',
                    'name': 'generateAuthorName',
                    'allowableAuthors': 2,
                    'authorGivenNameSelector': '.ce_given-name',
                    'authorSurnameSelector': '.ce_surname',
                    'moveIntoSelector': [
                        '.template-container .page .page-header .author-name',
                        '.source-container .page .page-header .author-name'
                    ]
                }
            ]
        },
        {
            'ruleId': 'naming-without-degree',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .head .ce_author-group .ce_author'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'filterChildExist',
                    'direction': false,
                    'selector': [
                        '.ce_degrees'
                    ]
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#57',
                    'name': 'addClass',
                    'classNames': ['without-degree']
                }
            ]
        },
        {
            'ruleId': 'naming-with-degree',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .head .ce_author-group .ce_author'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'filterChildExist',
                    'direction': true,
                    'selector': [
                        '.ce_degrees'
                    ]
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#58',
                    'name': 'addClass',
                    'classNames': ['with-degree']
                }
            ]
        },
        {
            'ruleId': 'wrap-author-group-childs',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .head .ce_author-group'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#59',
                    'name': 'wrapChilds',
                    'direction': true,
                    'tagName': 'div',
                    'wrapLimit': null,
                    'directChildSeletor': [
                        '.ce_author', '.ce_text', '.ce_collaboration'
                    ],
                    'classNames': ['author-group_body']
                }
            ]
        },
        {
            'ruleId': 'author-before-space',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .head .ce_author-group .ce_author ' +
                        '.ce_degrees + .ce_given-name',
                        '.source-container .head .ce_author-group .ce_author ' +
                        '+ .ce_text'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#60',
                    'name': 'templateBefore',
                    'template': [
                        '<span class="x"> </span>'
                    ]
                }
            ]
        },
        {
            'ruleId': 'naming-last-author',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .head .ce_author-group .ce_author'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'lastNode'
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#61',
                    'name': 'addClass',
                    'classNames': ['last-author']
                },
                {
                    'id': 'pgnPreAct#62',
                    'name': 'removeClass',
                    'classNames': ['without-degree', 'with-degree']
                }
            ]
        },
        {
            'ruleId': 'naming-parent-single-affiliation',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .head .ce_author-group'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'filterChildCount',
                    'count': 1,
                    'greater': false,
                    'lesser': false,
                    'selector': [
                        '.ce_affiliation'
                    ]
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#63',
                    'name': 'addClass',
                    'classNames': ['single-affiliation']
                }
            ]
        },
        {
            'ruleId': 'naming-single-affiliation',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .single-affiliation .last-author'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#64',
                    'name': 'addClass',
                    'classNames': ['single-affiliation']
                }
            ]
        },
        {
            'ruleId': 'naming-reference-labeled',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_bib-reference > .ce_label'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'parentNode'
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#65',
                    'name': 'addClass',
                    'classNames': ['labeled']
                }
            ]
        },
        {
            'ruleId': 'no-LQ-empty-child',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_bib-reference span[name^="LQ"]:empty',
                        '.source-container .ce_list span[name^="LQ"]:empty',
                        '.source-container .ulist span[name^="LQ"]:empty'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#66',
                    'name': 'removeNode'
                }
            ]
        },
        {
            'ruleId': 'naming-reference-unlabeled',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_bib-reference > .ce_label1'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'parentNode'
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#67',
                    'name': 'addClass',
                    'classNames': ['unlabeled']
                }
            ]
        },
        {
            'ruleId': 'wrap-reference-childs',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_bib-reference'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#68',
                    'name': 'wrapChilds',
                    'direction': false,
                    'tagName': 'div',
                    'wrapLimit': null,
                    'directChildSeletor': ['.ce_label', '.ce_label1', '.x'],
                    'classNames': ['reference_wrapper']
                }
            ]
        },
        {
            'ruleId': 'wrap-reference-label-childs',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_bib-reference'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#69',
                    'name': 'wrapChilds',
                    'direction': false,
                    'tagName': 'span',
                    'wrapLimit': null,
                    'directChildSeletor': ['.reference_wrapper'],
                    'classNames': ['reference_label_wrapper']
                }
            ]
        },
        {
            'ruleId': 'balance-width-reference',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_bibliography-sec',
                        '.source-container .ce_further-reading-sec'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#70',
                    'name': 'balanceWidth',
                    'selector': ['.ce_bib-reference:not(.unlabeled)'],
                    'onLeft': [
                        '.reference_label_wrapper'
                    ],
                    'onRight': [
                        '.reference_wrapper'
                    ]
                }
            ]
        },
        {
            'ruleId': 'no-text-indent',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_para'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'filterChildExist',
                    'selector': [
                        '.ce_def-list'
                    ],
                    'direction': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#71',
                    'name': 'addClass',
                    'classNames': ['no-text-indent']
                }
            ]
        },
        {
            'ruleId': 'no-start-symbol',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .sectionline_opt > span.x',
                        '.source-container .ce_affiliation span.x',
                        '.source-container .ce_correspondence span.x',
                        '.source-container .author-web-generated span.x',
                        '.source-container .ce_footnote span.x',
                        '.source-container .ce_miscellaneous span.x',
                        '.source-container .ce_article-footnote span.x',
                        '.source-container .firstFootnote span.x',
                        '.source-container .notFirstFootnote span.x',
                        '.source-container .cpeins span.x',
                        '.source-container span[data-format="insert"][data-actor="ce"] span.x',
                        '.source-container .ce_glossary-sec span.x',
                        '.source-container .with-degree span.x',
                        '.source-container ' +
                        '.without-degree:not(.single-affiliation) span.x',
                        '.source-container .ce_foonote .ce_label span.x'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#72',
                    'name': 'regexReplace',
                    'find': /^\.|^:|^\u2002|^,/g,
                    'replace': ''
                }
            ]
        },
        {
            'ruleId': 'space-after-title-label',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .sectionline_opt > span.x',
                        '.source-container .figcaption > span.x',
                        '.source-container .ce_appendices ' +
                        '.sectionline_opt  > span.x'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#73',
                    'name': 'regexReplace',
                    'find': /^&nbsp;|^\s|^\u00A0|&nbsp;$|\s$|\u00A0$/g,
                    'replace': '\u2002'
                }
            ]
        },
        {
            'ruleId': 'period-after-label',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .sectionline_opt > .ce_label',
                        '.source-container .figcaption > .ce_label',
                        '.source-container .ce_textbox > .ce_label',
                        '.source-container .ce_dtextbox > .ce_label',
                        '.source-container .ce_section > .cpeins > .ce_label',
                        '.source-container .ce_section > span[data-format="insert"][data-actor="ce"] > .ce_label',
                        '.source-container .toc-label',
                        '.A02-single-column .source-container .ce_foonote > .ce_label',
                        '.A02-single-column .source-container .ce_footnote > .ce_label'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'filterRegexMatch',
                    'direction': false,
                    'regex': /\.$/ig
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#74',
                    'name': 'regexReplace',
                    'find': /(.)$/ig,
                    'replace': '$1.'
                }
            ]
        },
        {
            'ruleId': 'no-extra-period',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_sections ' +
                        '.ce_section .ce_section .sectionline_opt > .ce_label',
                        '.source-container .figcaption > .ce_label',
                        '.source-container .ce_textbox > .ce_label',
                        '.source-container .ce_dtextbox > .ce_label'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#75',
                    'name': 'regexReplace',
                    'find': /(.)$/ig,
                    'replace': ''
                }
            ]
        },
        {
            'ruleId': 'space-after-label',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_affiliation .ce_label',
                        '.source-container .ce_correspondence .ce_label',
                        '.source-container .abbrevation-filled .ce_label',
                        '.source-container .author-web-generated .ce_label',
                        '.source-container .ce_footnote .ce_label',
                        '.source-container .ce_miscellaneous .ce_label',
                        '.source-container .ce_article-footnote .ce_label',
                        '.source-container .firstFootnote .ce_label',
                        '.source-container .notFirstFootnote .ce_label',
                        '.source-container .cpeins .ce_label',
                        '.source-container span[data-format="insert"][data-actor="ce"] .ce_label',
                        '.source-container .ce_foonote .ce_label'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'filterRegexMatch',
                    'direction': false,
                    'regex': /\u2002$/g
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#76',
                    'name': 'regexReplace',
                    'find': /$/,
                    'replace': '\u2002'
                }
            ]
        },
        {
            'ruleId': 'no-space-after-label',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_affiliation .ce_label',
                        '.source-container .ce_correspondence .ce_label'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#77',
                    'name': 'regexReplace',
                    'find': /\u2002$/g,
                    'replace': ''
                }
            ]
        },
        {
            'ruleId': 'no-space-after-footnote-label',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_correspondence .ce_label',
                        '.source-container .ce_footnote .ce_label',
                        '.source-container .ce_article-footnote .ce_label',
                        '.source-container .firstFootnote .ce_label',
                        '.source-container .notFirstFootnote .ce_label',
                        '.source-container .ce_foonote .ce_label'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#77a',
                    'name': 'regexReplace',
                    'find': /\u2002$/g,
                    'replace': ''
                }
            ]
        },
        {
            'ruleId': 'semicolon-after-head',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_glossary-sec ' +
                        '.ce_glossary-heading'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'filterRegexMatch',
                    'direction': false,
                    'regex': /:$/g
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#78',
                    'name': 'regexReplace',
                    'find': /$/ig,
                    'replace': ':'
                }
            ]
        },
        {
            'ruleId': 'space-after-runon-head',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .fourth_level .sectionline_opt ' +
                        '> .ce_section-title',
                        '.source-container .fifth_level .sectionline_opt ' +
                        '> .ce_section-title',
                        '.source-container .ce_glossary .ce_glossary-entry ' +
                        '.ce_glossary-heading'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#79',
                    'name': 'regexReplace',
                    'find': /$/g,
                    'replace': '\u2002'
                }
            ]
        },
        {
            'ruleId': 'append-author-end',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .with-degree',
                        '.source-container ' +
                        '.without-degree:not(.single-affiliation)'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#80',
                    'name': 'templateAppend',
                    'template': ['<span class="author-end">, </span>']
                }
            ]
        },
        {
            'ruleId': 'space-after-affiliation',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_affiliation:not(:last-child)'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#81',
                    'name': 'templateAfter',
                    'template': [
                        '<span class="ce_affiliation-after" ' +
                        'style="padding-right: 5px;">,</span>'
                    ]
                }
            ]
        },
        {
            'ruleId': 'space-before-au-ref',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_author-group .ce_author ' +
                        '*:not(.x) + .ce_cross-ref'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#82',
                    'name': 'templateBefore',
                    'template': [
                        '<span class="x" style="padding-left: 2px;"></span>'
                    ]
                }
            ]
        },
        {
            'ruleId': 'move-to-parent-last-child',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .table',
                        '.source-container .displayfigtab'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#83',
                    'name': 'moveAsLastChildOfParent',
                    'selector': [
                        '.ce_source'
                    ]
                }
            ]
        },
        {
            'ruleId': 'clear-inline-behavior',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_display',
                        '.source-container .table',
                        '.source-container .displayfigtab',
                        '.source-container .ce_list',
                        '.source-container .article-abstract-group'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#84',
                    'name': 'templateAppend',
                    'template': [
                        '<div class="float-parent" style="clear: both;">',
                        '</div>'
                    ]
                }
            ]
        },
        {
            'ruleId': 'wrap-title-childs',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_section'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#85',
                    'name': 'wrapChilds',
                    'direction': true,
                    'tagName': 'span',
                    'wrapLimit': null,
                    'directChildSeletor': [
                        '.cpeins', '.sectionline_opt',
                        '[data-actor="ce"][data-format="insert"]'
                    ],
                    'classNames': ['title_wrapper']
                }
            ]
        },
        {
            'ruleId': 'no-extra-footnote-head',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container span#fnhead'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#86',
                    'name': 'removeNode'
                }
            ]
        },
        {
            'ruleId': 'table-character-align',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .table table',
                        '.source-container .displayfigtab table'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#87',
                    'name': 'tableCharacterAlign'
                }
            ]
        },
        {
            'ruleId': 'naming-nested-figure',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container div.figure'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'filterChildCount',
                    'count': 1,
                    'greater': true,
                    'lesser': false,
                    'selector': [
                        '.wrapper + .figcaption'
                    ]
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#88',
                    'name': 'addClass',
                    'classNames': ['nested-figure']
                }
            ]
        },
        {
            'ruleId': 'call-break-figures',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container div.figure[id]:not(.display)'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#89',
                    'name': 'figureBreaker',
                    'linkid': 'link#3',
                    'selector': '.destination-container',
                    'options': {
                        'caption': '.figcaption',
                        'image': 'wrapper',
                        'idAttribute': 'id',
                        'label': '.ce_label'
                    }
                }
            ]
        },
        {
            'ruleId': 'naming-top-figure-caption',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.cpc-single-column .source-container div.figure' +
                        '[id]:not(.display)[data-position="bottom"]',
                        '.cpc-double-column .source-container div.figure' +
                        '[id]:not(.display)[data-position="bottom"]'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#90',
                    'name': 'removeAttribute',
                    'attributeName': 'data-position'
                },
                {
                    'id': 'pgnPreAct#91',
                    'name': 'addClass',
                    'classNames': ['top-figure-caption']
                },
                {
                    'id': 'pgnPreAct#92',
                    'name': 'applyStyle',
                    'style': {
                        'position': 'relative'
                    }
                }
            ]
        },
        {
            'ruleId': 'call-break-tables',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container div.table[id]:not(.display)'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#93',
                    'name': 'tableBreaker',
                    'linkid': 'link#4',
                    'selector': '.destination-container',
                    'options': {
                        'selectors': {
                            'caption': '.ce_caption',
                            'footer': '.ce_legend, .ce_table-footnote'
                        }
                    }
                }
            ]
        },
        {
            'ruleId': 'call-box-process',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_textbox',
                        '.source-container .ce_dtextbox'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#94',
                    'name': 'boxProcessor',
                    'selectors': {
                        'fAnchor': 'a.ce_cross-ref[title^="fn"]',
                        'attributeName': 'title',
                        'foonote': 'div.ce_foonote',
                        'footnote': 'div.ce_footnote',
                        'foonoteClassname': 'footnote-container',
                        'auSrc': '.ce_author-group',
                        'auDest': '.ce_textbox-body',
                        'credit': '.ce_source'
                    }
                }
            ]
        },
        {
            'ruleId': 'caption-before-paren',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .table[data-continued="true"] ' +
                        '.tablecaption .ce_caption',
                        '.source-container ' +
                        '.figure[data-is-part-figure="true"] .figcaption i'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#95',
                    'name': 'templateBefore',
                    'template': [
                        '<span class="ce_caption-before">(</span>'
                    ]
                }
            ]
        },
        {
            'ruleId': 'caption-after-paren',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .table[data-continued="true"] ' +
                        '.tablecaption .ce_caption',
                        '.source-container ' +
                        '.figure[data-is-part-figure="true"] .figcaption i'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#96',
                    'name': 'templateAfter',
                    'template': [
                        '<span class="ce_caption-after">)</span>'
                    ]
                }
            ]
        },
        {
            'ruleId': 'wrap-list-childs',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_list-item'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#97',
                    'name': 'wrapChilds',
                    'direction': false,
                    'tagName': 'div',
                    'wrapLimit': null,
                    'directChildSeletor': [
                        '.ce_label', '.cpeins',
                        '[data-actor="ce"][data-format="insert"]'
                    ],
                    'classNames': ['list-item_wrapper']
                }
            ]
        },
        {
            'ruleId': 'balance-width-list',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_list'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#98',
                    'name': 'balanceWidth',
                    'selector': ['.ce_list-item'],
                    'onLeft': [
                        '.ce_label', '.cpeins > .ce_label',
                        'span[data-format="insert"][data-actor="ce"] > .ce_label'
                    ],
                    'onRight': [
                        '.ce_label + .list-item_wrapper',
                        '.cpeins + .list-item_wrapper',
                        'span[data-format="insert"][data-actor="ce"] + .list-item_wrapper'
                    ]
                }
            ]
        },
        {
            'ruleId': 'copy-title-to-attr',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_cross-ref[title]',
                        '.source-container .ce_cross-refs[title]'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#99',
                    'name': 'copyAttribute',
                    'fromSelector': [],
                    'fromAttribute': 'title',
                    'toSelector': [],
                    'toAttribute': 'data-href',
                    'append': false
                },
                {
                    'id': 'pgnPreAct#100',
                    'name': 'regexAttributeReplace',
                    'attributeName': 'data-href',
                    'find': /^(.*)$/,
                    'replace': '#$1-proof'
                }
            ]
        },
        {
            'ruleId': 'naming-external-link',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_inter-ref[title]',
                        '.source-container .ce_inter-ref + .x a[title]'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#101',
                    'name': 'copyAttribute',
                    'fromSelector': [],
                    'fromAttribute': 'title',
                    'toSelector': [],
                    'toAttribute': 'data-href',
                    'append': false
                },
                {
                    'id': 'pgnPreAct#102',
                    'name': 'addAttribute',
                    'attributeName': 'target',
                    'attributeValue': '_blank'
                },
                {
                    'id': 'pgnPreAct#103',
                    'name': 'addClass',
                    'classNames': ['external_link_wrapper']
                }
            ]
        },
        {
            'ruleId': 'wrap-external-link-childs',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_doi',
                        '.source-container .doi',
                        '.source-container .ce_e-address'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#104',
                    'name': 'wrapChilds',
                    'direction': false,
                    'tagName': 'a',
                    'wrapLimit': null,
                    'directChildSeletor': [],
                    'classNames': ['external_link_wrapper']
                },
                {
                    'id': 'pgnPreAct#105',
                    'name': 'addAttribute',
                    'attributeName': 'target',
                    'attributeValue': '_blank'
                },
                {
                    'id': 'pgnPreAct#106',
                    'name': 'innerTextToAttribute',
                    'attributeName': 'data-href'
                },
                {
                    'id': 'pgnPreAct#107',
                    'name': 'regexAttributeReplace',
                    'attributeName': 'data-href',
                    'find': /\u200B/g,
                    'replace': ''
                }
            ]
        },
        {
            'ruleId': 'naming-ref-with-external-link',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_bib-reference'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'filterChildExist',
                    'selector': [
                        '.external_link_wrapper'
                    ],
                    'direction': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#108',
                    'name': 'addClass',
                    'classNames': ['with_external_link']
                }
            ]
        },
        {
            'ruleId': 'naming-ref-without-external-link',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_bib-reference'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'filterChildExist',
                    'selector': [
                        '.external_link_wrapper'
                    ],
                    'direction': false
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#109',
                    'name': 'addClass',
                    'classNames': ['without_external_link']
                }
            ]
        },
        {
            'ruleId': 'wrap-reference-link-childs',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_bib-reference.labeled' +
                        '.without_external_link .reference_wrapper',
                        '.source-container .ce_bib-reference.unlabeled' +
                        '.without_external_link .reference_wrapper'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#110',
                    'name': 'wrapChilds',
                    'direction': false,
                    'tagName': 'a',
                    'wrapLimit': null,
                    'directChildSeletor': ['.sb_authors', '.sb_date'],
                    'classNames': ['external_link_wrapper', 'reference_link']
                },
                {
                    'id': 'pgnPreAct#111',
                    'name': 'addAttribute',
                    'attributeName': 'target',
                    'attributeValue': '_blank'
                }
            ]
        },
        {
            'ruleId': 'copy-pii-to-ref-link-attr',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#112',
                    'name': 'copyAttribute',
                    'fromSelector': ['.item-info .pii'],
                    'fromAttribute': 'data-pii',
                    'toSelector': ['.ce_bib-reference .reference_link'],
                    'toAttribute': 'data-href',
                    'append': false
                },
                {
                    'id': 'pgnPreAct#113',
                    'name': 'regexAttributeReplace',
                    'attributeName': 'data-href',
                    'find': /^(.*)$/,
                    'replace': '//refhub.elsevier.com/$1/'
                }
            ]
        },
        {
            'ruleId': 'copy-ref-link-to-attr',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_bib-reference'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#114',
                    'name': 'copyAttribute',
                    'fromSelector': [],
                    'fromAttribute': 'id',
                    'toSelector': ['.reference_link'],
                    'toAttribute': 'data-href',
                    'append': true
                }
            ]
        },
        {
            'ruleId': 'wrap-def-list-childs',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_def-list'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#115',
                    'name': 'wrapChilds',
                    'direction': true,
                    'tagName': 'div',
                    'wrapLimit': null,
                    'directChildSeletor': ['.ce_def-term', '.ce_def-description'],
                    'classNames': ['def-list_body']
                }
            ]
        },
        {
            'ruleId': 'generate-dummy-def-desc',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .def-list_body' +
                        ' .ce_def-term + .ce_def-term'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'previousNode'
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#116',
                    'name': 'templateAfter',
                    'template': [
                        '<div class="ce_def-description"></div>'
                    ]
                }
            ]
        },
        {
            'ruleId': 'generate-dummy-last-child-def-desc',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .def-list_body' +
                        ' .ce_def-term:last-child'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#117',
                    'name': 'templateAfter',
                    'template': [
                        '<div class="ce_def-description"></div>'
                    ]
                }
            ]
        },
        {
            'ruleId': 'wrap-def-term-childs',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .def-list_body'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#118',
                    'name': 'wrapChilds',
                    'direction': true,
                    'tagName': 'div',
                    'wrapLimit': 2,
                    'directChildSeletor': ['.ce_def-term', '.ce_def-description'],
                    'classNames': ['def-term_wrapper']
                }
            ]
        },
        {
            'ruleId': 'balance-width-def-list',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .def-list_body'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'filterNodeExist',
                    'direction': false,
                    'selector': [
                        '.source-container .ce_glossary .def-list_body'
                    ]
                }

            ],
            'action': [
                {
                    'id': 'pgnPreAct#119',
                    'name': 'balanceWidth',
                    'selector': ['.def-term_wrapper'],
                    'onLeft': [
                        '.ce_def-term'
                    ],
                    'onRight': [
                        '.ce_def-description'
                    ]
                }
            ]
        },
        {
            'ruleId': 'naming-empty-content',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .title_wrapper',
                        '.source-container .sectionline_opt'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#120',
                    'name': 'emptyContent',
                    'className': 'empty-content'
                }
            ]
        },
        {
            'ruleId': 'require-title-arrow',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_sections > .ce_section' +
                        '[data-attr-role=sharp] > .title_wrapper .sectionline_opt',
                        '.source-container .ce_sections > .ce_section' +
                        ':not([data-attr-role=sharp]) > .title_wrapper .sectionline_opt',
                        '.source-container .ce_sections > .ce_section' +
                        '[data-attr-role=sharp] > .ce_section > .title_wrapper ' +
                        '.sectionline_opt'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#115a',
                    'name': 'templateAppend',
                    'template': [
                        '<span class="arrow-icon"></span>'
                    ]
                }
            ]
        },
        {
            'ruleId': 'require-abstract-bottom-rule',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_abstract ' +
                        '.ce_abstract-sec'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#121',
                    'name': 'templateAfter',
                    'template': [
                        '<div class="abstract-bottom-ruler"></div>'
                    ]
                }
            ]
        },
        {
            'ruleId': 'require-dtextbox-top-rule',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_dtextbox'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#122',
                    'name': 'templateBefore',
                    'template': [
                        '<div class="dtextbox-top-ruler"></div>'
                    ]
                }
            ]
        },
        {
            'ruleId': 'require-dtextbox-bottom-rule',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_dtextbox'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#123',
                    'name': 'templateAfter',
                    'template': [
                        '<div class="dtextbox-bottom-ruler"></div>'
                    ]
                }
            ]
        },
        {
            'ruleId': 'move-keywords-after-abstract-sec',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_keywords'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#124',
                    'name': 'moveAfter',
                    'selector': [
                        '.source-container .ce_abstract .ce_abstract-sec'
                    ]
                }
            ]
        },
        {
            'ruleId': 'number-of-tables',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .table'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'filterChildCount',
                    'count': 1,
                    'greater': true,
                    'lesser': false,
                    'selector': [
                        'table'
                    ]
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#125',
                    'name': 'addClass',
                    'classNames': ['sibling_table']
                }
            ]
        },
        {
            'ruleId': 'convert-number-to-word',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .body > .head > .ce_label'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#126',
                    'name': 'convertNumberToWord',
                    'find': /([0-9]+)/g
                },
                {
                    'id': 'pgnPreAct#127',
                    'name': 'regexReplace',
                    'find': /-| and/g,
                    'replace': ' '
                }
            ]
        },
        {
            'ruleId': 'naming-etoc-only-title',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .sectionline_opt'
                    ],
                    'selectAll': true
                },
                {
                    'name': 'filterChildExist',
                    'selector': [
                        '.ce_section-title[data-role^="etoc-only"]'
                    ],
                    'direction': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#128',
                    'name': 'addClass',
                    'classNames': ['etoc-only-title']
                }
            ]
        },
        {
            'ruleId': 'naming-etoc-only-title',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.source-container .ce_section-title[data-role^="etoc-only"]'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#129',
                    'name': 'addClass',
                    'classNames': ['etoc-only-title']
                }
            ]
        },
        {
            'ruleId': 'copy-xlink-to-attr',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.template-container .associated-resource '+
                        '.ce_inter-ref[data-xlink_href]',
                        '.source-container .associated-resource '+
                        '.ce_inter-ref[data-xlink_href]'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnPreAct#130',
                    'name': 'copyAttribute',
                    'fromSelector': [],
                    'fromAttribute': 'data-xlink_href',
                    'toSelector': [],
                    'toAttribute': 'data-href',
                    'append': false
                },
                {
                    'id': 'pgnPreAct#131',
                    'name': 'regexAttributeReplace',
                    'attributeName': 'data-href',
                    'find': /^doi:(\s*)http/ig,
                    'replace': 'http'
                },
                {
                    'id': 'pgnPreAct#132',
                    'name': 'regexAttributeReplace',
                    'attributeName': 'data-href',
                    'find': /^doi:/ig,
                    'replace': 'https://dx.doi.org/'
                },
                {
                    'id': 'pgnPreAct#133',
                    'name': 'addAttribute',
                    'attributeName': 'target',
                    'attributeValue': '_blank'
                }
            ]
        },
        {
            'ruleId': 'article-info-width-balancer',
            'condition': [
                {
                    'name': 'nodeExist',
                    'selector': [
                        '.cpc-single-column .article-abstract-column-one',
                        '.cpc-double-column .article-abstract-column-one',
                        '.threeG-single-column .article-abstract-column-one',
                        '.fiveG-double-column .article-abstract-column-one',
                        '.cpc-single-column .article-abstract-column-two',
                        '.cpc-double-column .article-abstract-column-two',
                        '.threeG-single-column .article-abstract-column-two',
                        '.fiveG-double-column .article-abstract-column-two',
                        '.cpc-single-column .article-abstract-column-three',
                        '.cpc-double-column .article-abstract-column-three',
                        '.threeG-single-column .article-abstract-column-three',
                        '.fiveG-double-column .article-abstract-column-three'
                    ],
                    'selectAll': true
                }
            ],
            'action': [
                {
                    'id': 'pgnBegAct#134',
                    'name': 'balanceArticleInfoWidth',
                    'widthIndicator': '.ce_keywords',
                    'classNames': ['no_keywords_exist']
                }
            ]
        }
    ];

    return rules;
});
