/*global define*/
define([], function RuleManagerConfig() {
    var rules = [
        {
            'layouts': [
                'fiveG-double-column',
                'threeG-single-column'
            ],
            'requiredRules': [
                'stitle-cleanup', 'insert-elements-template', 'move-toc',
                'move-abstract', 'move-keywords', 'move-miscellaneous',
                'move-presented-info', 'move-dedication-info',
                'pii-cleanup', 'copy-pii-to-attr', 'issn-cleanup',
                'doi-cleanup', 'copy-doi-info', 'copy-associated-resource',
                'articlenum-cleanup', 'copy-articlenum', 'associated-resource-cleanup',
                'copy-received-date', 'copy-revised-date', 'copy-accepted-date',
                'article-history-cleanup', 'no-show-empty-parent',
                'copy-copyrightyear-to-attr', 'copy-copyrightyear-to-innertext',
                'naming-first-level-head', 'naming-first-level',
                'naming-second-level-head', 'naming-second-level',
                'naming-third-level', 'naming-fourth-level',
                'naming-fifth-level', 'naming-sixth-level',
                'move-abbreviation-info', 'move-article-footnote-info',
                'copy-copyright-info', 'copyright-cleanup',
                'move-correspondence-info', 'move-author-footnote',
                'format-web-address', 'generate-author-name',
                'naming-without-degree', 'naming-with-degree',
                'wrap-author-group-childs', 'author-before-space',
                'naming-last-author', 'maming-parent-single-affiliation',
                'naming-single-affiliation', 'naming-reference-labeled',
                'no-LQ-empty-child', 'naming-reference-unlabeled',
                'wrap-reference-childs', 'wrap-reference-label-childs',
                'balance-width-reference', 'no-text-indent',
                'no-start-symbol', 'period-after-label', 'space-after-label',
                'semicolon-after-head', 'space-after-runon-head',
                'append-author-end', 'space-before-au-ref',
                'clear-inline-behavior', 'wrap-title-childs',
                'no-extra-footnote-head', 'table-character-align',
                'naming-nested-figure', 'call-break-figures',
                'call-break-tables', 'caption-before-paren',
                'caption-after-paren', 'wrap-list-childs',
                'balance-width-list', 'copy-title-to-attr',
                'naming-external-link', 'wrap-external-link-childs',
                'naming-ref-with-external-link',
                'naming-ref-without-external-link',
                'wrap-reference-link-childs',
                'copy-pii-to-ref-link-attr', 'copy-ref-link-to-attr',
                'wrap-def-list-childs', 'generate-dummy-def-desc',
                'generate-dummy-last-child-def-desc',
                'wrap-def-term-childs', 'balance-width-def-list',
                'naming-empty-content', 'no-column-bottom-space',
                'no-column-top-space', 'keep-in-center', 'no-inline-behavior',
                'reorder-footnotes', 'naming-etoc-only-title',
                'copy-xlink-to-attr', 'article-info-width-balancer',
                'no-eaddress-empty-child'
            ]
        }
    ];

    return rules;
});
