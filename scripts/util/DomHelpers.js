/*global define, document, console, window*/
define([
    'phoenix/Helper', 'phoenix/Logger'
], function DomHelpersLoader(Helper, Logger) {
    var domHelpers = null, proto = null;

    domHelpers = function domHelper() {};

    proto = domHelpers.prototype;

    proto.find = function find(toBeSelected, parent) {
        return parent.querySelector(toBeSelected);
    };

    proto.findAll = function findAll(toBeSelected, parent) {
        return parent.querySelectorAll(toBeSelected);
    };

    proto.append = function append(child, parent) {
        parent.appendChild(child);
    };

    proto.addAfter = function addAfter(toBeInserted, target, parent) {
        if (parent.lastChild === target) {
            parent.appendChild(toBeInserted);
        }

        if (toBeInserted === target.nextSibling) {
            return;
        }
        parent.insertBefore(toBeInserted, target.nextSibling);
    };

    proto.remove = function remove(toBeRemoved, parent) {
        if (toBeRemoved !== null && parent !== null) {
            parent.removeChild(toBeRemoved);
        }
    };

    proto.addOrModifyAttribute = function addOrModifyAttribute(
        attribute, value, element
    ) {
        if (element !== null) {
            element.setAttribute(attribute, value);
        }
    };

    proto.removeAttribute = function removeAttribute(attribute, element) {
        if (element !== null && element.hasAttribute(attribute)) {
            element.removeAttribute(attribute);
        }
    };

    proto.create = function create(toBeCreated) {
        return document.createElement(toBeCreated);
    };

    proto.createText = function createText(toBeCreated) {
        return document.createTextNode(toBeCreated);
    };

    proto.createFragment = function createFragment() {
        return document.createDocumentFragment();
    };

    proto.addBefore = function addBefore(toBeInserted, target, parent) {
        parent.insertBefore(toBeInserted, target);
    };

    proto.replaceElement = function replaceElement(toBeReplaced, target, parent) {
        if (target !== null && parent !== null) {
            parent.replaceChild(toBeReplaced, target);
        }
    };

    proto.clone = function clone(targetElement, flag) {
        return targetElement.cloneNode(flag);
    };

    proto.getParent = function getParent(child) {
        var parent = child.parentNode;

        if (parent !== null) {
            return child.parentNode;
        }

        return null;
    };

    proto.getNexts = function getNexts(element) {
        var result = [], next = null;

        next = element.nextElementSibling || element.nextSibling;

        while (next && next.nodeType === 1) {
            result.push(next);
            next = next.nextElementSibling || next.nextSibling;
        }

        return result;
    };

    proto.getContents = function getContents(source) {
        return source.innerHTML;
    };

    function isAllowableParent(self, parent, selector) {
        if (Helper.isObject(parent) === true) {
            return false;
        }

        if (self.containClass(parent, 'paginator') === true) {
            return false;
        }

        if (self.containClass(parent, selector) === true) {
            return false;
        }

        if (parent.nodeName.toLowerCase() === selector.toLowerCase()) {
            return false;
        }

        return true;
    }

    proto.getParents = function getParents(element, parentSelector) {
        var parents = [], parent = null;

        parent = element.parentNode;

        if (Helper.isObject(parent) === true) {
            while (isAllowableParent(this, parent, parentSelector) === true) {
                parents.push(parent);
                parent = parent.parentNode;
            }
        }

        return parents;
    };

    proto.addIntoClassList = function addIntoClassList(element, classname) {
        if (Helper.isUndefined(element) === true) {
            return false;
        }

        if (Helper.isObject(element) === false) {
            return false;
        }

        if (Helper.isObject(element.classList) === false) {
            return false;
        }

        element.classList.add(classname);

        return true;
    };

    proto.containClass = function containClass(element, toBeChecked) {
        var classList = [];

        if (Helper.isObject(element) === false) {
            return false;
        }

        classList = element.classList;

        if (Helper.isObject(classList) === false ||
            Helper.isUndefined(classList) === true
        ) {
            Logger.debug('Classlist is empty or undefined');

            return false;
        }

        if (classList.length === 0) {
            return false;
        }

        return element.classList.contains(toBeChecked);
    };

    proto.removeFromClassList = function removeFromClassList(element, classname) {
        if (Helper.isUndefined(element) === true) {
            return false;
        }

        if (Helper.isObject(element) === false) {
            return false;
        }

        if (Helper.isObject(element.classList) === false) {
            return false;
        }

        if (this.containClass(element, classname) === true) {
            element.classList.remove(classname);

            return true;
        }

        return false;
    };

    proto.removeClasses = function removeClasses(element, classes) {
        var i = 0, length = classes.length, classname = '';

        if (length > 0) {
            for (i = 0; i < length; i += 1) {
                classname = classes[i];
                this.removeFromClassList(element, classname);
            }
        }
    };

    proto.getOffsetWidth = function getOffsetWidth(element) {
        return element.offsetWidth;
    };

    proto.getOffsetHeight = function getOffsetHeight(element) {
        return element.offsetHeight;
    };

    proto.getOffsetTop = function getOffsetTop(element) {
        return element.offsetTop;
    };

    proto.getOffsetLeft = function getOffsetLeft(element) {
        return element.offsetLeft;
    };

    proto.getComputedStyles = function getComputedStyles(element, prop) {
        return window.getComputedStyle(element)[prop];
    };

    proto.getOuterHeight = function getOuterHeight(element) {
        var height = 0, i = 0, value = '', appliedStyles = [],
            props = [
                'height', 'border-top-width', 'border-bottom-width',
                'padding-top', 'padding-bottom', 'margin-top', 'margin-bottom'
            ], length = props.length, prop = '', numeric = 0;

        appliedStyles = window.getComputedStyle(element);

        for (i = 0; i < length; i += 1) {
            prop = props[i];
            value = appliedStyles[prop];
            value = value.replace('px', '');
            numeric = parseInt(value, 10);
            if (Helper.isNumber(numeric) === true) {
                height += numeric;
            }
        }

        return height;
    };

    proto.getOuterWidth = function getOuterWidth(element) {
        var width = 0, i = 0, appliedStyles = [], value = '', prop = '',
            props = [
                'width', 'border-top-width', 'border-bottom-width', 'padding-top',
                'padding-bottom', 'margin-top', 'margin-bottom'
            ], length = props.length, numeric = 0;

        appliedStyles = window.getComputedStyle(element);

        for (i = 0; i < length; i += 1) {
            prop = props[i];
            value = appliedStyles[prop];
            value = value.replace('px', '');
            numeric = parseInt(value, 10);
            if (Helper.isNumber(numeric) === true) {
                width += numeric;
            }
        }

        return width;
    };

    proto.getPrevious = function getPrevious(element) {
        var prev = element.previousSibling;

        if (prev !== null) {
            return prev;
        }

        return null;
    };

    proto.getPreviousElement = function getPreviousElement(element) {
        var prev = element.previousElementSibling;

        if (prev !== null) {
            return prev;
        }

        return null;
    };

    proto.isHiddenItem = function isHiddenItem(element) {
        var height = 0, width = 0;

        if (Helper.isObject(element) === false) {
            return true;
        }

        height = element.offsetHeight;
        width = element.offsetWidth;

        if (height === 0 && width === 0) {
            return true;
        }

        return false;
    };

    proto.empty = function empty(item) {
        while (Helper.isObject(item.firstChild) === true) {
            this.remove(item.firstChild, item);
        }
    };

    return domHelpers;
});
